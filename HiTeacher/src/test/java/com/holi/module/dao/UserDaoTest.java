package com.holi.module.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.holi.module.entity.User;
import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-mybatis.xml"})
public class UserDaoTest { 
	 private Logger logger = Logger.getLogger(this.getClass());
	@Resource
	private UserDao userDao;
	  
	@org.junit.Test
	public void testUserExist() throws JsonProcessingException {
		User user = userDao.get(1);
//		String phone=user.getLoginPhone();
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(user);
		logger.info(json);
		
		//Java集合转JSON
        //输出结果：[{"name":"小民","age":20,"birthday":844099200000,"email":"xiaomin@sina.com"}]
        List<User> users = new ArrayList<User>();
        users.add(user);
       // String jsonlist = mapper.writeValueAsString(users);
		 
		/**
		 * ObjectMapper支持从byte[]、File、InputStream、字符串等数据的JSON反序列化。
		 * ObjectMapper mapper = new ObjectMapper();
		 User user = mapper.readValue(json, User.class);
		 */

	}
	/**
	 * 签到测试
	 */
	@org.junit.Test
	public void testJpushId() {
		userDao.updateJpushId("01",null);
		/*//String jpushId = userDao.getjpushIdByStuId(2);
		System.out.println(jpushId);*/
	}
} 
