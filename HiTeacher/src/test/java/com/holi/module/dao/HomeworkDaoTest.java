package com.holi.module.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.holi.common.resultBean.CourseInfoBean;
import com.holi.common.resultBean.AllHomeworkInfoBean;
import com.holi.common.resultBean.HomeworkInfoBean;
import com.holi.module.entity.Homework;
import com.holi.module.entity.HomeworkRecord;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-mybatis.xml")
public class HomeworkDaoTest {

	@Resource
	private HomeworkDao homeworkDao;

	private Logger logger = Logger.getLogger(this.getClass());

	private static Date date = new Date();

	@Test
	public void testInsertHomeworkRecord(){
		//		Map<String, Object> map = new LinkedHashMap<>();
		//		map.put("courseId", "1115");
		//		map.put("courseName", "英语写作");
		//		map.put("classnumber", "01");
		//		map.put("teacherId", 102L);
		//		map.put("assignTime", date);
		HomeworkRecord hoRecord = new HomeworkRecord("1115", "商务英语", "02", "202", date);
		homeworkDao.insertHomeworkRecord(hoRecord);
		int lastId = hoRecord.getId();
		logger.info("上次插入的id:" + lastId);
	}

	@Test
	public void testBatchInsert() {
		List<Homework> list = null;
//		list.add(new Homework("1115", "02", "202", 101L, "英语作文两篇,1000字", date, 10));
//		list.add(new Homework("1115", "02", "202", 102L, "英语作文两篇,1000字", date, 10));
//		list.add(new Homework("1115", "02", "202", 103L, "英语作文两篇,1000字", date, 10));
//		list.add(new Homework("1115", "02", "202", 104L, "英语作文两篇,1000字", date, 10));
//		list.add(new Homework("1115", "02", "202", 105L, "英语作文两篇,1000字", date, 10));
//		list.add(new Homework("1115", "02", "202", 106L, "英语作文两篇,1000字", date, 10));
//		list.add(new Homework("1115", "02", "202", 107L, "英语作文两篇,1000字", date, 10));
//		list.add(new Homework("1115", "02", "202", 108L, "英语作文两篇,1000字", date, 10));
//		list.add(new Homework("1115", "02", "202", 109L, "英语作文两篇,1000字", date, 10));
//		list.add(new Homework("1115", "02", "202", 110L, "英语作文两篇,1000字", date, 10));

		Map<String, Object> map = new LinkedHashMap<>();
		map.put("homework", list);
		int affectRows = homeworkDao.insertByBatch(map);
		System.out.println("批量插入了"+affectRows+"条记录");
	}

//	@Test
//	public void testUncommitedStudentInfo() {
//
//		List<AllHomeworkInfoBean> list = null;
//
//		int homeworkRecordId = 1;
//		list = homeworkDao.listUncommitedStudentInfo(homeworkRecordId);
//		logger.info(list);
//
//		for(AllHomeworkInfoBean uBean : list){
//			logger.info("学号：" + uBean.getStuId());
//			logger.info("姓名：" + uBean.getStuName());
//			logger.info("学院：" + uBean.getSchoolId());
//		}
//
//	}

	@Test
	public void testListTeacherCourses(){

		List<CourseInfoBean> list = null;
		String teacherId = "109";

		list = homeworkDao.listTeacherCourseInfo(teacherId);

		logger.info(list.size());

		for(CourseInfoBean courseInfoBean : list){

			logger.info("课程编号：" + courseInfoBean.getCourseId());
			logger.info("课程名称：" + courseInfoBean.getCourseName());

		}
	}

	@Test
	public void testGetClassnumber(){

		List<String> list = null;
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("courseId", "1112");
		map.put("teacherId", "201");

		list = homeworkDao.getClassnumbersByTeacherAndCourse(map);

		for(String s : list){
			logger.info("班号：" + s);
		}
	}

	@Test
	public void pathTest(){

		String picUrl = "C:\\Users\\IVAN\\Desktop\\aa";
		ArrayList<String> urlList = new ArrayList<>();

		File file = new File(picUrl);
		if(file.exists() && file.isDirectory()){
			File[] files = file.listFiles();
			for(File tempFile : files){
//				String filename = tempFile.getName();
				String filename = tempFile.getAbsolutePath();
				System.out.println("filename:" + filename);
				urlList.add(picUrl + "\\" + filename);
			}
		}
	}
	
	@Test
	public void obtainHomeworkNumberTest(){
		
		Map<String, Object> map = new HashMap<>();
		
		map.put("teacherId", "201");
		map.put("courseId", "1112");
		map.put("classnumber", "03");
		
		List<HomeworkInfoBean> list = homeworkDao.getHomeworkNumberByCourseAndClassnumber(map);
		
		for(HomeworkInfoBean hoBean : list){
			logger.info("recordId:" + hoBean.getId() + "\t assignTime:" + hoBean.getAssignTime());
		}
		
	}
}
