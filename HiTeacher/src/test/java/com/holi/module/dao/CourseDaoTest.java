package com.holi.module.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.holi.module.entity.Course;

/**
 * 滚动课表Dao测试
 * @author IVAN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-mybatis.xml")
public class CourseDaoTest {

	@Resource
	private CourseDao courseDao;
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Test
	public void listCoursesTest() {
		List<Course> list = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("stu_id", 1);
		map.put("weekday", "3");
		map.put("term", 1);
		
		list = courseDao.listCourses(map);
		logger.info("查询得到的课程list对象："+list);
		
		Course course = list.get(0);
		logger.info("上课周次："+course.getCourseWeek());
		logger.info("上课节次："+course.getCourseTime());
		logger.info("课程名称："+course.getCourseName());
		logger.info("上课老师："+course.getTeacherName());
		logger.info("开课校区："+course.getCampus());
		logger.info("分班号："+course.getClassnumber());
		logger.info("上课教师："+course.getClassroom());
		logger.info("课程学分："+course.getCredit());
		logger.info("是否学位课："+course.getDegree());
	}

}
