package com.holi.module.dao;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.holi.module.entity.ClassGroup;
import com.holi.module.entity.Course;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-mybatis.xml"})
public class ClassGroupDaoTest {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Resource
	private ClassGroupDao classGroupDao;
	
	@Resource
	private CourseDao courseDao;
	  
	@org.junit.Test
	public void testUserExist() {
		/*//判断该课程是否已有班级群
		ClassGroup classGroup = classGroupDao.getClassGroupByCourseId("1");
		logger.info(classGroup.getTname());*/
		List<Course> course = courseDao.findCourseByTeacherId("10");
		
		logger.info(course.get(0).getStuId());
		logger.info(course.get(0).getCourseName());
	}
}
