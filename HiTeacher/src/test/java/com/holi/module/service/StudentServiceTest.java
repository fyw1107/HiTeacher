package com.holi.module.service;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.holi.module.dao.StudentDao;
import com.holi.module.entity.Student;

/**
 * 
 * @author lz
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-service.xml","classpath:spring-mybatis.xml"})
public class StudentServiceTest {
	
	private Logger logger = Logger.getLogger(this.getClass()); 
	
	@Autowired
	private StudentDao studentDao;
	/**
	 * 开启签到业务逻辑
	 * @param teacherId
	 */
	@org.junit.Test
	public void startSignIn() {
		//TODO 课程表抓取
		//TODO 极光推送
		long nowTime = new Date().getTime();
		List<Student> studentList = studentDao.getStuByTcherId("123", nowTime);
		for (Student student : studentList) {
			logger.info(student.getUserId());
		}
		logger.info(studentList);  
		
	}
}
