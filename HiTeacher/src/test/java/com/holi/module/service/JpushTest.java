package com.holi.module.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.holi.common.jpush.Jpush;
import com.holi.common.util.DateUtil;
import com.holi.common.yunxin.security.CheckSumBuilder;

import cn.jiguang.common.ClientConfig;
import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class JpushTest {

	@org.junit.Test
	public void JpushSend() {

		Logger LOG = Logger.getLogger(this.getClass());

		// List<Student> studentList = studentDao.getStuByTcherId(teacherId,
		// nowTime);
		/*JPushClient jPushClient = new JPushClient("ab3c06c1404e182cf68135fc", "288ad690020a0de09a268601", null,
				ClientConfig.getInstance());*/
		JPushClient jPushClient = Jpush.getJPushClient();
		// PushPayload payload = PushPayload.alertAll("startSignIn");
		try {
			Map<String, String> extras = new HashMap<String, String>();
			extras.put("type", "1001");
			PushResult result = jPushClient.sendAndroidNotificationWithAlias("SignIn", "statrtSignIn", extras,
					"101");
		} catch (APIConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (APIRequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@org.junit.Test
	public void post() throws IOException {

		Logger LOG = Logger.getLogger(this.getClass());

		OkHttpClient client = new OkHttpClient();

		String curTime = String.valueOf(DateUtil.getSystemTime().getTime());
		RequestBody formBody = new FormBody.Builder().add("accid", "1324784").build();

		Request request = new Request.Builder().url("https://api.netease.im/nimserver/user/create.action")
				.addHeader("AppKey", "afa6029173a8749cf06335d14fbaf265").addHeader("Nonce", "121424")
				.addHeader("CurTime", curTime)
				.addHeader("CheckSum", CheckSumBuilder.getCheckSum("9f25fb3a576e", "121424", curTime))
				.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8").post(formBody).build();
		/*
		 * // 设置请求的参数 List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		 * nvps.add(new BasicNameValuePair("accid", "helloworld"));
		 * httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));
		 */

		/*
		 * Response response = client.newCall(request).execute();
		 * System.out.println(response.body().string()); LOG.info(response);
		 */
		Call call = client.newCall(request);
		call.enqueue(new Callback() {

			@Override
			public void onResponse(Call arg0, Response arg1) throws IOException {
				// TODO Auto-generated method stub
				LOG.info(arg1.body().string());
			}

			@Override
			public void onFailure(Call arg0, IOException arg1) {
				LOG.info("dcd");
			}
		});

	}

	@org.junit.Test
	public void Tesyt() {

		/*
		 * System.out.println("start");
		 * 
		 * // String[] name = new String[]{"python //
		 * D:\\j2p\\studentdb.py","1601120338",password};
		 * 
		 * // 将参数传递给Python并执行爬取学生信息的py文件 // Process pr =
		 * Runtime.getRuntime().exec("python // D:\\j2p\\studentdb.py 1601120338
		 * 113411");//可行代码 // Process pr = Runtime.getRuntime().exec("python //
		 * D:\\j2p\\studentdb.py --id value --password value"); // Process pr =
		 * Runtime.getRuntime().exec(name); String id = "1501120400"; String
		 * password = "285099"; // 定义参数 String[] args2 = { id, password};
		 * Properties props = new Properties();
		 * props.put("python.console.encoding", "UTF-8");
		 * props.put("python.security.respectJavaAccessibility", "false");
		 * props.put("python.import.site", "false"); Properties preprops =
		 * System.getProperties(); // 设置参数
		 * PythonInterpreter.initialize(System.getProperties(), props, args2);
		 * PythonInterpreter interpreter = new PythonInterpreter(); // 执行
		 * interpreter.execfile("C:\\Users\\lz\\Desktop\\kk\\j2p\\studentdb.py")
		 * ; System.out.println("----------run over!----------");
		 */
	}
	
	public static void main(String[] args) {
		JPushClient jPushClient = Jpush.getJPushClient();
		
		try {
			Map<String, String> extras = new HashMap<String, String>();
			extras.put("type", "1001");
			PushResult result = jPushClient.sendAndroidNotificationWithAlias("SignIn", "statrtSignIn", extras,
					"100");
		} catch (APIConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (APIRequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
