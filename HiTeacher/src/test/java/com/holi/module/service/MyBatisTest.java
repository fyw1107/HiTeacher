package com.holi.module.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.holi.module.dao.SignCacheDao;
import com.holi.module.entity.SignCache;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring-service.xml", "classpath:spring-mybatis.xml",
		"classpath:spring-userBean.xml" })
public class MyBatisTest {

	private Logger LOG = Logger.getLogger(this.getClass());

	@Autowired
	private SignCacheDao signCacheDao;

	@org.junit.Test
	public void myBatisTest() {
		//SignCache signCache = signCacheDao.get(1);
		List<SignCache> signCacheList = signCacheDao.findAllCreateWifi("1121", 1);
		
		for (SignCache signCache : signCacheList) {
			LOG.info(signCache.getStudent().getStuId());
		}
	}
}
