package com.holi.module.service;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.holi.common.help.YunXinHelp;
import com.holi.common.util.DateUtil;
import com.holi.common.yunxin.request.ClassGroupRequest;
import com.holi.common.yunxin.security.CheckSumBuilder;
import com.holi.common.yunxin.security.RandomNum;
import com.holi.module.entity.ClassGroup;

import io.netty.handler.codec.http2.Http2HeadersEncoder.SensitivityDetector;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ClassGroupServiceTest {

	private static Logger LOG = Logger.getLogger(ClassGroupServiceTest.class);

	private static final String createClassGroupURL = "https://api.netease.im/nimserver/team/create.action";

	static OkHttpClient client = new OkHttpClient();

	public void classGroupServiceTest() {
		ClassGroup classGroup = new ClassGroup();

		// coourseName = classGroup.setTname("毛泽东思想");
		classGroup.setOwner("03");
		classGroup.setMembers("[\"" + "03" + "\"]");
		classGroup.setMsg("毛泽东思想");
		classGroup.setMagree(0);
		classGroup.setJoinmode(2);
		classGroup.setBeinvitemode(1);

		try {
			Response response = createClassGroup(classGroup);
			String s = response.body().string();
			LOG.info(s);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		try {
			Response response = ClassGroupRequest.getInstance().queryMsg("[\"" + "49298642" + "\"]", "1");

			String str = response.body().string();
			System.out.println(str);
			JSONObject jsonObject = new JSONObject(str);
			JSONArray jsonArray = jsonObject.getJSONArray("tinfos");
			int tid = jsonArray.getJSONObject(0).getInt("tid");
			System.out.println(tid);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static String sensitiveWord(String courseName) {

		// 正则表达式规则
		String regEx = "毛泽东";
		// 编译正则表达式
		Pattern pattern = Pattern.compile(regEx);

		// 忽略大小写的写法
		Matcher matcher = pattern.matcher(courseName);

		// 查找字符串中是否有匹配正则表达式的字符/字符串
		if (matcher.find()) {
			courseName = "毛概";
		}

		return courseName;
	}

	/**
	 * 创建班级群
	 * 
	 * @param classGroup
	 * @return
	 * @throws IOException
	 */
	public static Response createClassGroup(ClassGroup classGroup) throws IOException {

		String curTime = String.valueOf(DateUtil.getSystemTime().getTime());
		String Nonce = RandomNum.getRandomNum();
		String CheckSum = CheckSumBuilder.getCheckSum(YunXinHelp.appSecret, Nonce, curTime);

		RequestBody formBody = new FormBody.Builder().add("tname", classGroup.getTname())
				.add("owner", classGroup.getOwner()).add("members", classGroup.getMembers())
				/* .add("intro", classGroup.getIntro()) */.add("msg", classGroup.getMsg())
				.add("magree", String.valueOf(classGroup.getMagree()))
				.add("joinmode",
						String.valueOf(classGroup
								.getJoinmode()))/*
												 * .add("custom",
												 * classGroup.getCustom())
												 */
				.add("beinvitemode", String.valueOf(classGroup.getBeinvitemode()))
				/*
				 * .add("invitemode",
				 * String.valueOf(classGroup.getInvitemode()))
				 * .add("uptinfomode",
				 * String.valueOf(classGroup.getUptinfomode()))
				 * .add("upcustommode",
				 * String.valueOf(classGroup.getUptinfomode()))
				 */.build();

		Request request = new Request.Builder().url(createClassGroupURL).addHeader("AppKey", YunXinHelp.appKey)
				.addHeader("Nonce", Nonce).addHeader("CurTime", curTime).addHeader("CheckSum", CheckSum)
				.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8").post(formBody).build();

		Response response = client.newCall(request).execute();

		return response;
	}
}
