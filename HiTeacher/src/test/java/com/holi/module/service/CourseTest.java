package com.holi.module.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.holi.module.dao.CourseDao;
import com.holi.module.dao.SignCacheDao;
import com.holi.module.entity.Course;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring-service.xml", "classpath:spring-mybatis.xml",
		"classpath:spring-userBean.xml" })
public class CourseTest {

	Logger LOG = Logger.getLogger(this.getClass());

	@Autowired
	private CourseDao courseDao;

	@Autowired
	private SignCacheDao signCacheDao;

	@org.junit.Test
	public void TestGetAllCourse() {
		List<Course> courseList = courseDao.getAllCourse();
		for (Course course : courseList) {
			System.out.println(course.getCourseName() + "(" + course.getTeacherName() + ")");
		}

	}

	@org.junit.Test
	public void getCourseIdByCouresName() {
		System.out.println(signCacheDao.deleteByTeacherId("109"));
	}
}
