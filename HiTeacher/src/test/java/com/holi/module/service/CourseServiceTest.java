package com.holi.module.service;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.holi.common.resultBean.CourseBean;

/**
 * 滚动课表Service测试
 * @author IVAN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring-service.xml", "classpath:spring-mybatis.xml",
"classpath:spring-userBean.xml" })
public class CourseServiceTest {
	
	@Resource
	private CourseService courseService;
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Test
	public void courseServiceTest() {
		 
		List<CourseBean> list = courseService.listCourseByStuAndWeek(1);
		
		logger.info(list);
		logger.info("课程名称："+list.get(0).getCourseName());
		logger.info("上课时间："+list.get(0).getDuration());
		logger.info("上课地点："+list.get(0).getCoursePlace());
		logger.info("--------------------------");
		logger.info("课程名称："+list.get(1).getCourseName());
		logger.info("上课时间："+list.get(1).getDuration());
		logger.info("上课地点："+list.get(1).getCoursePlace());
		
	}

}
