package com.holi.module.service;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.holi.common.resultBean.CourseScheduleBean;
import com.holi.module.dao.ClassGroupDao;
import com.holi.module.entity.CourseSchedule;

/**
 * 查询我的课表逻辑测试
 * 
 * @author IVAN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring-service.xml", "classpath:spring-mybatis.xml",
		"classpath:spring-userBean.xml" })
public class CourseScheduleServiceTest {

	@Autowired
	private CourseScheduleService courseScheduleService;

	@Autowired
	private ClassGroupDao classGroupDao;

	private Logger logger = Logger.getLogger(this.getClass());

	@Test
	public void testCourseScheduleService() {
		classGroupDao.updataOwner("48950717", "123456");
	}

}
