package com.holi.module.service;

import java.net.ServerSocket;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.holi.common.help.XDxuyuanHelp;
import com.holi.module.entity.SignCache;
import com.holi.module.entity.Student;

public class MapTest {
	private Logger LOG = Logger.getLogger(this.getClass());

	@org.junit.Test
	public void mapTest() {
		Map<Integer, String> map = new TreeMap<>(new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o2 - o1;
			}
		});
		map.put(1, "1");
		map.put(5, "2");
		map.put(6, "2");
		map.put(82, "2");
		map.put(76, "2");
		Set<Integer> s = map.keySet();
		for (Integer integer : s) {
			LOG.info(integer);
		}
		LOG.info(map.get(1));
		/*
		 * Iterator<?> iter = map.entrySet().iterator(); while (iter.hasNext())
		 * { Map.Entry entry1 = (Map.Entry) iter.next(); String val2 = (String)
		 * ((Map.Entry) iter.next()).getValue(); Object val1 =
		 * entry1.getValue(); // Object val2 = entry2.getValue();
		 * LOG.info(val1); LOG.info(val2); }
		 */
	}

	@org.junit.Test
	public void setTest() {
		String s ="123";
		s.length();
		String[] s1 = new String[2];
		String [] s2 = s.split("");
		int result = Integer.parseInt("00110101", 2);
        //System.out.println(result);
		LOG.info(result);
	
		/*Set<SignCache> set = new HashSet<SignCache>();

		SignCache cache1 = new SignCache();
		Student student1 = new Student();
		student1.setStuId("1234");
		student1.setStuName("小帅");
		cache1.setStudent(student1);
		set.add(cache1);

		SignCache cache2 = new SignCache();
		Student student2 = new Student();
		student2.setStuId("1234");
		student2.setStuName("小帅");
		cache2.setStudent(student2);
		// set.add(cache2);

		if (set.contains(cache2))
			LOG.info("set test over");*/
	}
	
	@org.junit.Test
	public void mapTest2() {
		String s = "15010130042";
		s = s.substring(s.length() - 10, s.length());
		System.out.println(s);
	}
}
