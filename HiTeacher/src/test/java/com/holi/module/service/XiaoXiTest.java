package com.holi.module.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.holi.common.resultBean.XiaoxiNewsResultBean;
import com.holi.module.dao.XiaoXiNewsDao;
import com.holi.module.entity.XiaoXiNews;

/**
 * 校息测试
 * 
 * @author lz
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring-service.xml", "classpath:spring-mybatis.xml",
		"classpath:spring-userBean.xml" })
public class XiaoXiTest {

	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private XiaoXiService xiaoXiService;
	@Autowired
	private XiaoXiNewsDao xiaoXiNewsDao;

	/**
	 * 下拉刷新测试
	 */
	@org.junit.Test
	public void pullDown() {
		/*List<XiaoxiNewsResultBean> list = xiaoXiNewsDao.pullDown(4);
		for (XiaoxiNewsResultBean xiaoXi : list) {
			logger.info(xiaoXi.getId());
		}*/
	}

	/**
	 *上拉加载测试
	 */
	@org.junit.Test
	public void pullUp() {
		/*List<XiaoXiNews> list = xiaoXiService.pullUp(6);
		for (XiaoXiNews xiaoXi : list) {
			logger.info(xiaoXi.getId());
		}*/
	}
}
