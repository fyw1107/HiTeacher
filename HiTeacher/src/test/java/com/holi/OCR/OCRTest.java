package com.holi.OCR;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.holi.common.jsonBean.OCR;
import com.holi.common.jsonBean.Words;
import com.holi.common.yunxin.request.ClassGroupRequest;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import sun.misc.BASE64Encoder;

public class OCRTest {
	Logger LOG = Logger.getLogger(this.getClass());

	private OkHttpClient client;
	private static OCRTest baiduOCR;

	private static final String grant_type = "client_credentials";
	private static final String client_id = "gttA8PG9NBFoN53MAns6qPjz";
	private static final String client_secret = "iLcheOPjwvGROVr8KzZjTrTQ9UCQ4Qa1";
	private static String access_token = "24.1372a88d54f4f37b6f3b9b0fabaceb0f.2592000.1499137039.282335-9723725";

	private static final String generalOcrURL = "https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic?access_token="
			+ access_token;
	private static final String getAccessTokenURL = "https://aip.baidubce.com/oauth/2.0/token?grant_type=" + grant_type
			+ "&client_id=" + client_id + "&client_secret=" + client_secret;

	private OCRTest() {
		client = new OkHttpClient();
	}

	public static OCRTest getInstance() {

		if (baiduOCR == null) {
			synchronized (ClassGroupRequest.class) {
				if (baiduOCR == null) {
					baiduOCR = new OCRTest();
				}
			}
		}
		return baiduOCR;
	}

	/**
	 * 获取accessToken
	 * 
	 * @return
	 * @throws IOException
	 */
	public String getAccessToken() throws IOException {

		Request request = new Request.Builder().url(getAccessTokenURL).build();

		Response response = client.newCall(request).execute();

		String result = response.body().string();
		JSONObject jsonObject = new JSONObject(result);
		String accessToken = jsonObject.getString("access_token");

		return accessToken;
	}

	/**
	 * 通用文字识别
	 * 
	 * @param image
	 * @return
	 * @throws IOException
	 */
	public Response generalOCR(String image) throws IOException {

		RequestBody formBody = new FormBody.Builder().add("image", image).add("detect_direction", "true").build();

		Request request = new Request.Builder().url(generalOcrURL)
				.addHeader("Content-Type", "application/x-www-form-urlencoded").post(formBody).build();

		Response response = client.newCall(request).execute();

		return response;
	}

	@SuppressWarnings("restriction")
	public static String GetBase64ImageStr(String imgFilePath) {
		// 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
		byte[] data = null;

		// 读取图片字节数组
		try {
			InputStream in = new FileInputStream(imgFilePath);
			data = new byte[in.available()];
			in.read(data);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// 对字节数组Base64编码
		BASE64Encoder encoder = new BASE64Encoder();
		return encoder.encode(data).replace("\r\n", "");// 返回Base64编码过的字节数组字符串
	}

	public static void main(String[] args) {
		try {
			String base64ImageStr = GetBase64ImageStr("C:\\Users\\lz\\Desktop\\16.jpg");
			Response response = OCRTest.getInstance().generalOCR(base64ImageStr);
			String OCRStr = response.body().string();

			System.out.println(OCRStr);

			Gson gson = new Gson();
			OCR ocr = gson.fromJson(OCRStr, OCR.class);

			System.out.println(ocr.getWords_result_num());

			List<Words> wordLsit = ocr.getWords_result();

			for (Words words : wordLsit) {
				String reg = "^.*\\d{9}.*$";
				String word = words.getWords();
				if (word.matches(reg)) {
					String regEx = "[^0-9]";
					Pattern p = Pattern.compile(regEx);
					Matcher m = p.matcher(word);
					System.out.println(m.replaceAll("").trim());
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
