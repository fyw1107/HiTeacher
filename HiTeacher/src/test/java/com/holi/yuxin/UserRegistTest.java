package com.holi.yuxin;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.holi.common.util.DateUtil;
import com.holi.common.yunxin.request.UserRequest;
import com.holi.common.yunxin.security.CheckSumBuilder;
import com.holi.common.yunxin.security.RandomNum;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UserRegistTest {

	Logger LOG = Logger.getLogger(this.getClass());

	private static UserRegistTest user = null;
	private OkHttpClient client;

	private static final String userRegisterURL = "https://api.netease.im/nimserver/user/create.action";

	private UserRegistTest() {
		client = new OkHttpClient();
	};

	public static UserRegistTest getInstance() {
		// 先判断是否被实例化过，避免了每次加锁同步的性能消耗
		if (user == null) {
			synchronized (UserRequest.class) {
				if (user == null) {
					user = new UserRegistTest();
				}
			}
		}
		return user;
	}

	public Response userRegister(String accid, String name, String token) throws IOException {

		String curTime = String.valueOf(DateUtil.getSystemTime().getTime());
		String Nonce = RandomNum.getRandomNum();
		String CheckSum = CheckSumBuilder.getCheckSum("9f25fb3a576e", Nonce, curTime);

		RequestBody formBody = new FormBody.Builder().add("accid", accid).add("name", name)
				.add("token", token).build();

		Request request = new Request.Builder().url(userRegisterURL)
				.addHeader("AppKey", "afa6029173a8749cf06335d14fbaf265").addHeader("Nonce", Nonce)
				.addHeader("CurTime", curTime).addHeader("CheckSum", CheckSum)
				.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8").post(formBody).build();

		Response response = client.newCall(request).execute();

		return response;
	}
	
	public static void main(String[] args) {
		try {
			Response response = UserRegistTest.getInstance().userRegister("10", "hi", "21");
			String str = response.body().string();
			System.out.println(str);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
