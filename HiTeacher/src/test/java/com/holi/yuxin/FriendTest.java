package com.holi.yuxin;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.holi.common.util.DateUtil;
import com.holi.common.yunxin.request.FriendsRequest;
import com.holi.common.yunxin.security.CheckSumBuilder;
import com.holi.common.yunxin.security.RandomNum;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FriendTest {

	private Logger LOG = Logger.getLogger(this.getClass());

	private OkHttpClient client;
	private static FriendTest friends;

	private static final String addFriendURL = "https://api.netease.im/nimserver/friend/add.action";
	private static final String updateFriendURL = "https://api.netease.im/nimserver/friend/update.action";

	private FriendTest() {
		client = new OkHttpClient();
	}

	public static FriendTest getInstance() {

		if (friends == null) {
			synchronized (FriendsRequest.class) {
				if (friends == null) {
					friends = new FriendTest();
				}
			}
		}
		return friends;
	}

	public Response addFriend(String accid, String faccid, String type, String msg) throws IOException {

		String curTime = String.valueOf(DateUtil.getSystemTime().getTime());
		String Nonce = RandomNum.getRandomNum();
		String CheckSum = CheckSumBuilder.getCheckSum("9f25fb3a576e", Nonce, curTime);

		RequestBody formBody = new FormBody.Builder().add("accid", accid).add("faccid", faccid).add("msg", msg)
				.add("type", type).build();

		Request request = new Request.Builder().url(addFriendURL)
				.addHeader("AppKey", "afa6029173a8749cf06335d14fbaf265").addHeader("Nonce", Nonce)
				.addHeader("CurTime", curTime).addHeader("CheckSum", CheckSum)
				.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8").post(formBody).build();

		Response response = client.newCall(request).execute();

		return response;
	}

	public static void main(String[] args) {
		FriendTest friendTest = FriendTest.getInstance();
		Response response;
		try {
			response = friendTest.addFriend("15291578736", "15202965053", "2", "hi");

			String str = response.body().string();
			System.out.println(str);
			JSONObject jsonObject = new JSONObject(str);
			int code = jsonObject.getInt("code");
			System.out.println(code);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
