package com.holi.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.apache.ibatis.reflection.SystemMetaObject;
import org.apache.log4j.Logger;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ServiceTest {

	static Logger LOG = Logger.getLogger(ServiceTest.class);

	@org.junit.Test
	public static Response userRegister(String accid, String name, String token) throws IOException {

		OkHttpClient client = new OkHttpClient();

		RequestBody formBody = new FormBody.Builder().add("accid", accid).add("name", name).add("token", token).build();

		Request request = new Request.Builder().url("http://114.115.130.61:8080/HiTeacher/user/bind")
				.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8").post(formBody).build();

		Response response = client.newCall(request).execute();

		return response;
	}

	public static int i;

	public static void main(String[] args) throws IOException {
		/*
		 * Response response = userRegister("12", "陈老师", "01"); //01 —— 09 九位同学
		 * //10，11两位老师 //10 高数课老师 11 大学英语课老师 12 微观经济学 //01 —— 09 高数课 和 英语课 //05
		 * —— 09 微观经济学 LOG.info(response.body().string());
		 */
		/*
		 * String [] str1 = {"123"}; String str2 = "123"; char c[] =
		 * {'1','2','3'}; String str3 = c.toString(); if(str2 == str3) {
		 * System.out.println("t"); } else {
		 */
		/*
		 * float m = (float) 2.0; float n = (float) 1.2; m = m - n;
		 * System.out.println(m); //}
		 */

		Scanner sc = new Scanner(System.in);

		String str1 = sc.next();
		String str2 = sc.next();

		// String str2 = "1*trade*done";
		String[] subStr2 = str2.split("");
		String s = "";
		for (String string : subStr2) {
			if (string.equals("*")) {
				s += "." + string;
			} else if (string.equals("?")) {
				s += ".";
			} else {
				s += string;
			}
			// System.out.println(string);
		}
		System.out.println(s);

		boolean isMatch = Pattern.matches("1.*trade*.done", "100-trade-done");
		System.out.println(isMatch);
	}
}
