package com.holi.service;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

public class Main {

	public static void main(String[] args) {

		ArrayList<Integer> inputs = new ArrayList<Integer>();
		Scanner in = new Scanner(System.in);
		String line = in.nextLine();
		if (line != null && !line.isEmpty()) {
			int res = resolve(line.trim());
			System.out.println(String.valueOf(res));
		}
	}

	// write your code here
	public static int resolve(String expr) {
		Stack<String> stack = new Stack<String>();
		String[] exprs = expr.split("\\s");
		for (int i = 0; i < exprs.length; i++) {
			String theIString = exprs[i];
			try {
				Integer.parseInt(theIString);
				stack.push(theIString);
			} catch (Exception e) {
				// not Integer
				switch (theIString) {
				case "+":
					if (stack.size() < 2) {
						return -1;
					}
					stack.push(String.valueOf((Integer.parseInt(stack.pop()) + Integer.parseInt(stack.pop()))));
					break;
				case "^":
					if (stack.size() < 1) {
						return -1;
					}
					stack.push(String.valueOf(Integer.parseInt(stack.pop()) + 1));
					break;
				case "*":
					if (stack.size() < 2) {
						return -1;
					}
					stack.push(String.valueOf((Integer.parseInt(stack.pop()) * Integer.parseInt(stack.pop()))));
					break;
				}
			}
			if (stack.size() > 16) {
				return -2;
			}
		}
		return Integer.parseInt(stack.pop());
	}
}
