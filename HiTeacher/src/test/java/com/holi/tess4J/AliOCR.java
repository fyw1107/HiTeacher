package com.holi.tess4J;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.ibatis.reflection.SystemMetaObject;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import sun.misc.BASE64Encoder;

public class AliOCR {
	static OkHttpClient client = new OkHttpClient();

	public static void main(String[] args) {

		// final MediaType JSON = MediaType.parse("application/json;
		// charset=utf-8");
		// final String appcode = "6d2e474cacdf4a32a41a94d31374cb75";
		//
		// String Base64ImageStr =
		// GetBase64ImageStr("C:\\Users\\lz\\Documents\\tessJ4\\3.jpg");
		//
		// String bodys =
		// "{\"inputs\":[{\"image\":{\"dataType\":50,\"dataValue\":"
		// + Base64ImageStr + "}}]}";
		//
		// // 创建一个RequestBody(参数1：数据类型 参数2传递的json串)
		// RequestBody requestBody = RequestBody.create(JSON, bodys);
		//
		// Request request = new Request.Builder()
		// .url("http://dm-57.data.aliyun.com/rest/160601/ocr/ocr_business_card.json")
		// .addHeader("Content-Type", "application/json; charset=UTF-8")
		// .addHeader("Authorization", "APPCODE " +
		// appcode).post(requestBody).build();
		//
		// try {
		// Response response = client.newCall(request).execute();
		// System.out.println(response.body().string());
		// } catch (IOException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// }

		final String appcode = "6d2e474cacdf4a32a41a94d31374cb75";
		String host = "https://dm-57.data.aliyun.com";
		String path = "/rest/160601/ocr/ocr_business_card.json";
		String method = "POST";

		String Base64ImageStr = GetBase64ImageStr("C:\\Users\\lz\\Documents\\tessJ4\\1.jpg");
//		String bodys1 = "{\"inputs\":[{\"image\":{\"dataType\":50,\"dataValue\":" +"/"" + "" + "}}]}";
		String d = "}}]}";
//		System.out.println(bodys1);
		String bodys = "{\"inputs\":[{\"image\":{\"dataType\":50,\"dataValue\":\"" + Base64ImageStr + "\"}}]}";
		System.out.println(bodys);
		JSONObject jsonObject = new JSONObject();
		
		Map<String, String> headers = new HashMap<String, String>();
		// 最后在header中的格式(中间是英文空格)为Authorization:APPCODE
		// 83359fd73fe94948385f570e3c139105
		headers.put("Authorization", "APPCODE " + appcode);
		// 根据API的要求，定义相对应的Content-Type
		headers.put("Content-Type", "application/json; charset=UTF-8");
		Map<String, String> querys = new HashMap<String, String>();

		try {
			/**
			 * 重要提示如下: HttpUtils请从
			 * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
			 * 下载
			 *
			 * 相应的依赖请参照
			 * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
			 */
			HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
			System.out.println(response.toString());
			// 获取response的body
			System.out.println(EntityUtils.toString(response.getEntity()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("restriction")
	public static String GetBase64ImageStr(String imgFilePath) {
		// 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
		byte[] data = null;

		// 读取图片字节数组
		try {
			InputStream in = new FileInputStream(imgFilePath);
			data = new byte[in.available()];
			in.read(data);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// 对字节数组Base64编码
		BASE64Encoder encoder = new BASE64Encoder();
		return encoder.encode(data);// 返回Base64编码过的字节数组字符串
	}
}
