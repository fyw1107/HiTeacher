package com.holi.tess4J;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.util.ImageHelper;

public class tess4JTest {

	public static void main(String[] args) {
		try {
			File imageFile = new File("C:\\Users\\lz\\Documents\\tessJ4\\1.jpg");
			System.out.println(imageFile.exists());
			System.out.println(imageFile.canRead());

			BufferedImage grayImage = ImageHelper.convertImageToBinary(ImageIO.read(imageFile));
			ImageIO.write(grayImage, "jpg", new File("C:\\Users\\lz\\Documents\\tessJ4\\", "test2.jpg"));
			File imageFile2 = new File("C:\\Users\\lz\\Documents\\tessJ4\\test2.jpg");

			ITesseract instance = new Tesseract();
			instance.setLanguage("eng");
		    instance.setLanguage("chi_sim");//中文库识别中文

			String result = instance.doOCR(imageFile);
			System.out.println(result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
