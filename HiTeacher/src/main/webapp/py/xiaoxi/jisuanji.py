# -*- coding: utf-8 -*-
import requests
import pymysql
from bs4 import BeautifulSoup
import jingguanyuan

#获取主页全部代码
jy_html = requests.get('http://cs.xidian.edu.cn/')
jy_html.encoding = 'gbk'
#对网页进行soup
jy_hostpage_soup = BeautifulSoup(jy_html.text,'html.parser')
#获取新闻和通知整体,
selected_news_html = jy_hostpage_soup.find_all(class_=['list-news'])
selected_inf_html = jy_hostpage_soup.find_all(class_=['list-notice'])
#将获取的新闻和通知整体进行soup
selected_news_soup = BeautifulSoup(str(selected_news_html),'html.parser')
selected_inf_soup = BeautifulSoup(str(selected_inf_html),'html.parser')
#提取每一条新闻和通知 提取出来形式为list
news_list = selected_news_soup.select('li')
inf_list = selected_inf_soup.select('li')
#获取新闻和会议的个数
news_num = len(news_list)
inf_num = len(inf_list)
#startid = 41


#获取单个新闻连接
def getnew_link(i):
    eachnew_soup = BeautifulSoup(str(news_list[i]),'html.parser')
    href =  eachnew_soup.select('a')[0]['href']
    return href

#获取单个新闻标题
def getnew_name(everynew_soup):
    name = everynew_soup.select('.title')[0].text
    return name

#获取单个新闻时间
def getnew_date(everynew_soup):
    date_list = everynew_soup.select('.time')
    date1 = date_list[0].text[12:22]
    date = date1.replace('-','.')
    return date

#获取单个新闻内容
def getnew_content(everynew_soup):
    content = everynew_soup.select('.content')[0].text
    return content

#获取单个新闻图片
def getnew_pic(everynew_soup):
    pic = everynew_soup.select('.content')
    pic_soup = BeautifulSoup(str(pic),'html.parser')
    pic_list = pic_soup.select('img')
    picsrc1 = []
    for i in range(len(pic_list)):
        src = 'http://cs.xidian.edu.cn'+pic_list[i]['src']
        picsrc1.append(src)
    picsrc = str(picsrc1)
    return picsrc

#获取单个新闻的id, publisher, is_hot, cateory_id, name, link, date, content, picsrc,并添加到数据库xidian
def everynew(i):
    #id = startid + i
    number = i
    xueyuan = '3'
    category_id= '1'
    link = getnew_link(i)
    everynew_html = requests.get(link)
    everynew_html.encoding = 'gbk'
    everynew_soup = BeautifulSoup(everynew_html.text, 'html.parser')
    name = getnew_name(everynew_soup)
    date = getnew_date(everynew_soup)
    content = getnew_content(everynew_soup)
    picsrc = getnew_pic(everynew_soup)

    # 连接数据库
    connection = pymysql.connect(host='127.0.0.1', user='root', password='mysqladmin', db='hiteacher', charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)
    # 执行sql语句
    try:
        with connection.cursor() as cursor:
            sql = "insert into `xiaoxi`(`xueyuan`,`category_id`,`name`,`link`,`date`,`content`,`picsrc`)values(%s,%s,%s,%s,%s,%s,%s)"
            # 使用 execute()  方法执行 SQL 查询
            cursor.execute(sql, (xueyuan, category_id, name, link, date, content, picsrc))
            connection.commit()
    finally:
        connection.close()

#获取单个通知连接
def getinf_link(i):
    eachinf_soup = BeautifulSoup(str(inf_list[i]), 'html.parser')
    href = eachinf_soup.select('a')[0]['href']
    return href

#获取单个通知标题
def getinf_name(everyinf_soup):
    name = everyinf_soup.select('title')[0].text
    return name

#获取单个通知时间
def getinf_date(everyinf_soup):
    date_list = everyinf_soup.select('.time')
    date1 = date_list[0].text[12:22]
    date = date1.replace('-','.')
    return date

#获取单个通知内容
def getinf_content(everyinf_soup):
    content = everyinf_soup.select('.content')[0].text
    return content

#获取单个新闻的id, publisher, is_hot, cateory_id, name, link, date, content, picsrc,添加到数据库
def everyinf(i):
    #id = startid + news_num +i
    number = i
    xueyuan = '3'
    category_id = '2'
    link = getinf_link(i)
    everyinf_html = requests.get(link)
    everyinf_html.encoding = 'gbk'
    everyinf_soup = BeautifulSoup(everyinf_html.text, 'html.parser')
    name = getinf_name(everyinf_soup)
    date = getinf_date(everyinf_soup)
    content = getinf_content(everyinf_soup)
    picsrc = getnew_pic(everyinf_soup)

    # 连接数据库
    connection = pymysql.connect(host='127.0.0.1', user='root', password='mysqladmin', db='hiteacher', charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)
    # 执行sql语句
    try:
        with connection.cursor() as cursor:
            sql = "insert into `xiaoxi`(`xueyuan`,`category_id`,`name`,`link`,`date`,`content`)values(%s,%s,%s,%s,%s,%s)"
            # 使用 execute()  方法执行 SQL 查询
            cursor.execute(sql, (xueyuan, category_id, name, link, date, content))
            connection.commit()
    finally:
        connection.close()

def sel():

    for i in range(news_num):
        try:
            everynew(i)
        except:
            i+1

    for i in range(inf_num):
        try:
            everyinf(i)
        except:
            i+1


