# -*- coding: utf-8 -*-
import requests
import pymysql
import re
from bs4 import BeautifulSoup
import ruanyuan

# 获取主页全部代码
y_html = requests.get('http://math.xidian.edu.cn/')
y_html.encoding = 'gbk'
# 对网页进行soup
y_hostpage_soup = BeautifulSoup(y_html.text,'html.parser')
# 获取新闻和通知整体
selected_news_html = y_hostpage_soup.select('table')[18]
selected_inf_html = y_hostpage_soup.select('table')[15]
# 将获取的新闻和通知整体进行soup
selected_news_soup = BeautifulSoup(str(selected_news_html),'html.parser')
selected_inf_soup = BeautifulSoup(str(selected_inf_html),'html.parser')
# 提取每一条新闻和通知 提取出来形式为list
news_list = selected_news_soup.select('tr')[1:]
inf_list = selected_inf_soup.select('tr')[1:]
# 获取新闻和会议的个数
news_num = len(news_list)
inf_num = len(inf_list)
#startid = 81



# 获取单个新闻连接
def getnew_link(i):
    eachnew_soup = BeautifulSoup(str(news_list[i]),'html.parser')
    href =  'http://math.xidian.edu.cn/'+ eachnew_soup.select('a')[0]['href']
    return href

# 获取单个通知连接
def getinf_link(i):
    eachinf_soup = BeautifulSoup(str(inf_list[i]), 'html.parser')
    href = 'http://math.xidian.edu.cn/' + eachinf_soup.select('a')[0]['href']
    return href

# 获取单个标题
def get_name(every_soup):
    nameall = every_soup.select('table')[22]
    name1 = BeautifulSoup(str(nameall),'html.parser')
    name = name1.select('td')[1].text
    return name

# 获取单个时间
def get_date(every_soup):
    dateall = every_soup.select('table')[22]
    date1 = BeautifulSoup(str(dateall),'html.parser')
    date2 = date1.select('td')[3].text
    p1 = r'201\d.\d\d.\d\d'
    pattern1 = re.compile(p1)
    date3 = re.search(pattern1,date2).group()
    date = date3.replace('-','.')
    return date

# 获取单个内容
def get_content(every_soup):
    contentall = every_soup.select('table')[22]
    content1 = BeautifulSoup(str(contentall),'html.parser')
    content = content1.select('td')[4].text
    return content

# 获取单个图片
def get_pic(every_soup):
    picall = every_soup.select('table')[22]
    pic1 = BeautifulSoup(str(picall),'html.parser')
    pic = pic1.select('p')
    pic_soup = BeautifulSoup(str(pic),'html.parser')
    pic_list = pic_soup.select('img')
    picsrc1 = []
    for i in range(len(pic_list)):
        src = 'http://math.xidian.edu.cn'+pic_list[i]['src']
        picsrc1.append(src)
    picsrc = str(picsrc1)
    return picsrc

# 获取单个新闻的id, xueyuan, is_hot, cateory_id, name, link, date, content, picsrc,并添加到数据库xidian
def everynew(i):
    #id = startid + i
    number = i
    xueyuan = '7'
    category_id = '1'
    link = getnew_link(i)
    every_html = requests.get(link)
    every_html.encoding = 'gbk'
    every_soup = BeautifulSoup(every_html.text, 'html.parser')
    name = get_name(every_soup)
    date = get_date(every_soup)
    content = get_content(every_soup)
    picsrc = get_pic(every_soup)
    # 连接数据库
    connection = pymysql.connect(host='127.0.0.1', user='root', password='mysqladmin', db='hiteacher', charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)
    # 执行sql语句
    try:
        with connection.cursor() as cursor:
            sql = "insert into `xiaoxi`(`xueyuan`,`category_id`,`name`,`link`,`date`,`content`,`picsrc`)values(%s,%s,%s,%s,%s,%s,%s)"
            # 使用 execute()  方法执行 SQL 查询
            cursor.execute(sql, (xueyuan, category_id, name, link, date, content, picsrc))
            connection.commit()
    finally:
        connection.close()

# 获取单个通知的id, xueyuan, is_hot, cateory_id, name, link, date, content, picsrc,并添加到数据库xidian
def everyinf(i):
    #id = startid + news_num + i
    number = i
    xueyuan = '7'
    category_id = '2'
    link = getinf_link(i)
    every_html = requests.get(link)
    every_html.encoding = 'gbk'
    every_soup = BeautifulSoup(every_html.text, 'html.parser')
    name = get_name(every_soup)
    date = get_date(every_soup)
    content = get_content(every_soup)
    picsrc = get_pic(every_soup)

    # 连接数据库
    connection = pymysql.connect(host='127.0.0.1', user='root', password='mysqladmin', db='hiteacher', charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)
    # 执行sql语句
    try:
        with connection.cursor() as cursor:
            sql = "insert into `xiaoxi`(`xueyuan`,`category_id`,`name`,`link`,`date`,`content`)values(%s,%s,%s,%s,%s,%s)"
            # 使用 execute()  方法执行 SQL 查询
            cursor.execute(sql, (xueyuan,category_id, name, link, date, content))
            connection.commit()
    finally:
        connection.close()

def sel():


    for i in range(news_num):
        try:
            everynew(i)
        except:
            i = i+1

    for i in range(inf_num):
        try:
            everyinf(i)
        except:
            i+1
