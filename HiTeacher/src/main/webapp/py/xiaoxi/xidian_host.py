# -*- coding: utf-8 -*-
import requests
import pymysql
from bs4 import BeautifulSoup
#获取主网页全部代码
xidian_res = requests.get("http://news.xidian.edu.cn/")
xidian_res.encoding = 'utf-8'
#对全部网页代码进行soup化，并提取有用内容，内容格式为list,但其实内容只有一个class
news_hosepage_html_soup = BeautifulSoup(xidian_res.text,'html.parser')
selected_news_html = news_hosepage_html_soup.select('.content1_left_bottom_right')[0]
#将content1_left_bottom_right内容soup化
new_list_soup = BeautifulSoup(str(selected_news_html),'html.parser')
#提取每一条新闻
new_list = new_list_soup.select('li')
#获取会议网页
#startid = 177



#获取单个新闻的时间函数
def getnew_date(clink):
    every_new_html_res = requests.get(clink)
    every_new_html_res.encoding = 'utf-8'
    every_new_html_soup = BeautifulSoup(every_new_html_res.text, 'html.parser')
    date1 = every_new_html_soup.select('#date')[0].text
    date2 = date1[5:15]
    date = date2.replace('-','.')
    return date


#获取单个新闻图片函数
def getnew_pict(clink):
    every_new_html_res = requests.get(clink)
    every_new_html_res.encoding = 'utf-8'
    every_new_html_soup = BeautifulSoup(every_new_html_res.text, 'html.parser')
    selected_text_list = every_new_html_soup.select('#wz_zw')[0]
    picsoup = BeautifulSoup(str(selected_text_list), 'html.parser')
    piclist = picsoup.select('img')
    lenpic = len(piclist)
    srcfirst = 'http://news.xidian.edu.cn/'
    picsrc1 = []
    for i in range(lenpic):
        srclast1 = piclist[i]['src'][7:]
        srclast2 = '_' + srclast1
        src = str(srcfirst) + str(srclast2)
        picsrc1.append(src)
    picsrc = str(picsrc1)
    return picsrc

#获取单个新闻内容的函数
def getnew_content(clink):
    every_new_html_res = requests.get(clink)
    every_new_html_res.encoding = 'utf-8'
    every_new_html_soup = BeautifulSoup(every_new_html_res.text, 'html.parser')
    selected_text_list = every_new_html_soup.select('#wz_zw')[0]  # 一个列表
    content= selected_text_list.text
    return content


#针对每一条新闻id开始获取name、content、publisher、date、is_hot、category_id,picsrc
def everynew(i):
    onenew_soup = BeautifulSoup(str(new_list[i]),'html.parser')
    href = onenew_soup.select('a')[1]['href']
    if len(href)<21:
        link = 'http://news.xidian.edu.cn/' + href
    else:
        link = href

    #id = startid+i
    number = i
    xueyuan = '0'
    category_id = 1
    name = onenew_soup.select('a')[1].text
    link = link
    date = getnew_date(link)
    content = getnew_content(link)
    picsrc = getnew_pict(link)


    # 连接数据库
    connection = pymysql.connect(host='127.0.0.1', user='root', password='mysqladmin', db='hiteacher', charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)
    # 执行sql语句
    try:
        with connection.cursor() as cursor:
            sql = "insert into `xiaoxi`(`xueyuan`,`category_id`,`name`,`link`,`date`,`content`,`picsrc`)values(%s,%s,%s,%s,%s,%s,%s)"
            # 使用 execute()  方法执行 SQL 查询
            cursor.execute(sql, (xueyuan , category_id, name, link, date, content, picsrc))
            connection.commit()
    finally:
        connection.close()

def sel():

    for i in range(len(new_list)):
        try:
            everynew(i)
        except:
            i+1

