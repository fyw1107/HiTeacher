# -*- coding: utf-8 -*-
import requests
import pymysql
from bs4 import BeautifulSoup
import weiyuan

#获取主页全部代码
y_html = requests.get('http://stp.xidian.edu.cn/')
y_html.encoding = 'utf-8'
#对网页进行soup
y_hostpage_soup = BeautifulSoup(y_html.text,'html.parser')
#获取新闻和通知整体
selected_news_html = y_hostpage_soup.select('.content_list')[0]
selected_inf_html = y_hostpage_soup.select('.content_list')[2]
#将获取的新闻和通知整体进行soup
selected_news_soup = BeautifulSoup(str(selected_news_html),'html.parser')
selected_inf_soup = BeautifulSoup(str(selected_inf_html),'html.parser')
#提取每一条新闻和通知 提取出来形式为list
news_list = selected_news_soup.select('li')
inf_list = selected_inf_soup.select('li')
#获取新闻和会议的个数
news_num = len(news_list)
inf_num = len(inf_list)
#startid = 149


#获取单个新闻连接
def getnew_link(i):
    eachnew_soup = BeautifulSoup(str(news_list[i]),'html.parser')
    href =  'http://stp.xidian.edu.cn'+ eachnew_soup.select('a')[0]['href']
    return href

#获取单个通知连接
def getinf_link(i):
    eachinf_soup = BeautifulSoup(str(inf_list[i]), 'html.parser')
    href = 'http://stp.xidian.edu.cn' + eachinf_soup.select('a')[0]['href']
    return href

#获取单个标题
def get_name(every_soup):
    name = every_soup.select('#sub_title')[0].text
    return name

#获取单个时间
def get_date(every_soup):
    date_list = every_soup.select('center')
    date1 = date_list[2].text[5:15]
    date = date1.replace('-','.')
    return date

#获取单个内容(list=0,8,9content部分有点问题)
def get_content(every_soup):
    content_list = every_soup.select('#sub_content_detail')
    content = content_list[0].text
    #content = ''
    #for i in range(len(content_list)):
        #content = content + content_list[i].text
    return content

#获取单个图片
def get_pic(every_soup):
    pic = every_soup.select('#sub_content_detail')
    pic_soup = BeautifulSoup(str(pic),'html.parser')
    pic_list = pic_soup.select('img')
    picsrc1 = []
    for i in range(len(pic_list)):
        src = 'http://stp.xidian.edu.cn'+pic_list[i]['src']
        picsrc1.append(src)
    picsrc = str(picsrc1)
    return picsrc

# 获取单个新闻的id, xueyuan, is_hot, cateory_id, name, link, date, content, picsrc,并添加到数据库xidian
def everynew(i):
    #id = startid + i
    number = i
    xueyuan = '5'
    category_id = '1'
    link = getnew_link(i)
    every_html = requests.get(link)
    every_html.encoding = 'utf-8'
    every_soup = BeautifulSoup(every_html.text, 'html.parser')
    name = get_name(every_soup)
    date = get_date(every_soup)
    content = get_content(every_soup)
    picsrc = get_pic(every_soup)
    # 连接数据库
    connection = pymysql.connect(host='127.0.0.1', user='root', password='mysqladmin', db='hiteacher', charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)
    # 执行sql语句
    try:
        with connection.cursor() as cursor:
            sql = "insert into `xiaoxi`(`xueyuan`,`category_id`,`name`,`link`,`date`,`content`,`picsrc`)values(%s,%s,%s,%s,%s,%s,%s)"
            # 使用 execute()  方法执行 SQL 查询
            cursor.execute(sql, (xueyuan, category_id, name, link, date, content, picsrc))
            connection.commit()
    finally:
        connection.close()


# 获取单个通知的id, xueyuan, is_hot, cateory_id, name, link, date, content, picsrc,并添加到数据库xidian
def everyinf(i):
    #id = startid + news_num + i
    number = i
    xueyuan = '5'
    category_id = '2'
    link = getinf_link(i)
    every_html = requests.get(link)
    every_html.encoding = 'utf-8'
    every_soup = BeautifulSoup(every_html.text, 'html.parser')
    name = get_name(every_soup)
    date = get_date(every_soup)
    content = get_content(every_soup)
    picsrc = get_pic(every_soup)

    # 连接数据库
    connection = pymysql.connect(host='127.0.0.1', user='root', password='mysqladmin', db='hiteacher', charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)
    # 执行sql语句
    try:
        with connection.cursor() as cursor:
            sql = "insert into `xiaoxi`(`xueyuan`,`category_id`,`name`,`link`,`date`,`content`)values(%s,%s,%s,%s,%s,%s)"
            # 使用 execute()  方法执行 SQL 查询
            cursor.execute(sql, (xueyuan, category_id, name, link, date, content))
            connection.commit()
    finally:
        connection.close()

def sel():

    for i in range(news_num):
        try:
            everynew(i)
        except:
            i+1

    for i in range(inf_num):
        try:
            everyinf(i)
        except:
            i+1



#print(getnew_link(0))
#print(getinf_link(0))
# everyinf_html = requests.get(getnew_link(1))
# everyinf_html.encoding = 'utf-8'
# everyinf_soup = BeautifulSoup(everyinf_html.text, 'html.parser')
# print(get_name(everyinf_soup))
# print(get_date(everyinf_soup))
# print(get_content(everyinf_soup))
# print(get_pic(everyinf_soup))