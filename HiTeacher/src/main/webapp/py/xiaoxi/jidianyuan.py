# -*- coding: utf-8 -*-
import requests
import pymysql
import re
from bs4 import BeautifulSoup


#获取主页全部代码
jdy_html = requests.get('http://eme.xidian.edu.cn/index.html')
jdy_html.encoding = 'utf-8'
#对网页进行soup
jdy_hostpage_soup = BeautifulSoup(jdy_html.text,'html.parser')
#获取新闻和通知整体
selected_news_html = jdy_hostpage_soup.select('.news')
selected_inf_html = jdy_hostpage_soup.select('.notice')
#将获取的新闻和通知整体进行soup
selected_news_soup = BeautifulSoup(str(selected_news_html),'html.parser')
selected_inf_soup = BeautifulSoup(str(selected_inf_html),'html.parser')
#提取每一条新闻和通知 提取出来形式为list
news_list = selected_news_soup.select('li')
inf_list = selected_inf_soup.select('li')
#获取新闻和会议的个数
news_num = len(news_list)
inf_num = len(inf_list)
#startid = 8

#获取单个新闻连接
def getnew_link(i):
    eachnew_soup = BeautifulSoup(str(news_list[i]),'html.parser')
    href =  'http://eme.xidian.edu.cn'+ eachnew_soup.select('a')[0]['href']
    return href

#获取单个通知连接
def getinf_link(i):
    eachinf_soup = BeautifulSoup(str(inf_list[i]), 'html.parser')
    href = 'http://eme.xidian.edu.cn' + eachinf_soup.select('a')[0]['href']
    return href

#获取单个标题
def get_name(every_soup):
    name = every_soup.select('h6')[0].text
    return name

#获取单个时间
def get_date(every_soup):
    date_list = every_soup.select('.arc_info')
    date1 = date_list[0].text[10:25]
    p1 = r'201\d.\d\d.\d\d'
    pattern1 = re.compile(p1)
    date3 = re.search(pattern1,date1).group()#!!!加个group（）
    date = date3.replace('-','.')
    return date

#获取单个内容
def get_content(every_soup):
    content = every_soup.select('.arc_con')[0].text
    return content

#获取单个图片
def get_pic(every_soup):
    pic = every_soup.select('.arc_con')[0]
    pic_soup = BeautifulSoup(str(pic),'html.parser')
    pic_list = pic_soup.select('img')
    picsrc1 = []
    for i in range(len(pic_list)):
        src = 'http://eme.xidian.edu.cn'+pic_list[i]['src']
        picsrc1.append(src)
    picsrc = str(picsrc1)
    return picsrc

#获取单个新闻的id, publisher, is_hot, cateory_id, name, link, date, content, picsrc,并添加到数据库xidian
def everynew(i):
    #id = startid+i
    number = i
    xueyuan = '4'
    category_id= '1'
    link = getnew_link(i)
    everynew_html = requests.get(link)
    everynew_html.encoding = 'utf-8'
    everynew_soup = BeautifulSoup(everynew_html.text, 'html.parser')
    name = get_name(everynew_soup)
    date = get_date(everynew_soup)
    content = get_content(everynew_soup)
    picsrc = get_pic(everynew_soup)

    # 连接数据库
    connection = pymysql.connect(host='127.0.0.1', user='root', password='mysqladmin', db='hiteacher', charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)
    # 执行sql语句
    try:
        with connection.cursor() as cursor:
            sql = "insert into `xiaoxi`(`xueyuan`,`category_id`,`name`,`link`,`date`,`content`,`picsrc`)values(%s,%s,%s,%s,%s,%s,%s)"
            # 使用 execute()  方法执行 SQL 查询
            cursor.execute(sql, (xueyuan,category_id,name,link,date,content,picsrc))
            connection.commit()
    finally:
        connection.close()

#获取单个通知的id, publisher, is_hot, cateory_id, name, link, date, content, picsrc,并添加到数据库xidian
def everyinf(i):
    #id = startid+news_num+i
    number = i
    xueyuan = '4'
    category_id= '2'
    link = getinf_link(i)
    everyinf_html = requests.get(link)
    everyinf_html.encoding = 'utf-8'
    everyinf_soup = BeautifulSoup(everyinf_html.text, 'html.parser')
    name = get_name(everyinf_soup)
    date = get_date(everyinf_soup)
    content = get_content(everyinf_soup)
    picsrc = get_pic(everyinf_soup)

    # 连接数据库
    connection = pymysql.connect(host='127.0.0.1', user='root', password='mysqladmin', db='hiteacher', charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)
    # 执行sql语句
    try:
        with connection.cursor() as cursor:
            sql = "insert into `xiaoxi`(`xueyuan`,`category_id`,`name`,`link`,`date`,`content`)values(%s,%s,%s,%s,%s,%s)"
            # 使用 execute()  方法执行 SQL 查询
            cursor.execute(sql, (xueyuan, category_id, name, link, date, content))
            connection.commit()
    finally:
        connection.close()

def sel():

    for i in range(news_num):
        try:
            everynew(i)
        except:
            i+1

    for i in range(inf_num):
        try:
            everyinf(i)
        except:
            i+1







