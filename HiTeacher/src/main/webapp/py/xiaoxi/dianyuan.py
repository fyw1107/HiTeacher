# -*- coding: utf-8 -*-
import requests
import pymysql
from bs4 import BeautifulSoup

# 获取主页全部代码
y_html = requests.get('http://see.xidian.edu.cn/')
y_html.encoding = 'utf-8'
# 对网页进行soup
y_hostpage_soup = BeautifulSoup(y_html.text,'html.parser')
# 获取新闻整体
selected_news_html = y_hostpage_soup.select('.news_list')[1]
# 将获取的新闻整体进行soup
selected_news_soup = BeautifulSoup(str(selected_news_html),'html.parser')
# 提取每一条新闻和通知 提取出来形式为list
news_list = selected_news_soup.select('li')
# 获取新闻和会议的个数
news_num = len(news_list)
#startid = 0
#endid = startid + news_num

# 获取单个新闻连接
def getnew_link(i):
    eachnew_soup = BeautifulSoup(str(news_list[i]),'html.parser')
    href =  eachnew_soup.select('a')[0]['href']
    return href

# 获取单个标题
def get_name(every_soup):
    name = every_soup.select('#article_title')[0].text
    return name

# 获取单个时间
def get_date(every_soup):
    date_list = every_soup.select('#article_detail')[0]
    date1 = date_list.text[0:10]
    date = date1.replace('-','.')
    return date

# 获取单个内容
def get_content(every_soup):
    content_list = every_soup.select('#article_content')
    content = content_list[0].text
    return content

# 获取单个图片
def get_pic(every_soup):
    pic = every_soup.select('#article_content')
    pic_soup = BeautifulSoup(str(pic),'html.parser')
    pic_list = pic_soup.select('img')
    picsrc1 = []
    for i in range(len(pic_list)):
        src = 'http://see.xidian.edu.cn/'+pic_list[i]['src']
        picsrc1.append(src)
    picsrc = str(picsrc1)
    return picsrc

# 获取单个新闻的id, publisher, is_hot, cateory_id, name, link, date, content, picsrc,并添加到数据库xidian
def everynew(i):
    #id = 0+i
    number = i
    xueyuan = '2'
    category_id = '1'
    link = getnew_link(i)
    every_html = requests.get(link)
    every_html.encoding = 'utf-8'
    every_soup = BeautifulSoup(every_html.text, 'html.parser')
    name = get_name(every_soup)
    date = get_date(every_soup)
    content = get_content(every_soup)
    picsrc = get_pic(every_soup)
    # 连接数据库
    connection = pymysql.connect(host='127.0.0.1', user='root', password='mysqladmin', db='hiteacher', charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)
    # 执行sql语句
    try:
        with connection.cursor() as cursor:
            sql = "insert into `xiaoxi`(`xueyuan`,`category_id`,`name`,`link`,`date`,`content`,`picsrc`)values(%s,%s,%s,%s,%s,%s,%s)"
            # 使用 execute()  方法执行 SQL 查询
            cursor.execute(sql, (xueyuan , category_id, name, link, date, content, picsrc))
            connection.commit()
    finally:
        connection.close()

def sel():
    for i in range(news_num):
        try:
            everynew(i)
        except:
            i = i+1



