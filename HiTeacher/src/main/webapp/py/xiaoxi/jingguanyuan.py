# -*- coding: utf-8 -*-
import requests
import pymysql
import re
from bs4 import BeautifulSoup


# 获取主页全部代码
y_html = requests.get('http://em.xidian.edu.cn/')
y_html.encoding = 'gbk'
# 对网页进行soup
y_hostpage_soup = BeautifulSoup(y_html.text,'html.parser')
# 获取新闻和通知整体
selected_news_html = y_hostpage_soup.select('table')[17:23]
selected_inf_html = y_hostpage_soup.select('table')[8:15]
# 将获取的新闻和通知整体进行soup
selected_news_soup = BeautifulSoup(str(selected_news_html),'html.parser')
selected_inf_soup = BeautifulSoup(str(selected_inf_html),'html.parser')
# 提取每一条新闻和通知 提取出来形式为list
news_list = selected_news_soup.select('table')[0:]
inf_list = selected_inf_soup.select('table')[0:]
# 获取新闻和会议的个数
news_num = len(news_list)
inf_num = len(inf_list)
#startid = 28


# 获取单个新闻连接
def getnew_link(i):
    eachnew_soup = BeautifulSoup(str(news_list[i]),'html.parser')
    href =  'http://em.xidian.edu.cn/'+ eachnew_soup.select('a')[0]['href']
    return href

# 获取单个通知连接
def getinf_link(i):
    eachinf_soup = BeautifulSoup(str(inf_list[i]), 'html.parser')
    href = 'http://em.xidian.edu.cn/' + eachinf_soup.select('a')[0]['href']
    return href

# 获取单个标题
def getnew_name(i):
    eachnew_soup = BeautifulSoup(str(news_list[i]), 'html.parser')
    name =  eachnew_soup.select('a')[0].text
    return name

# 获取单个时间
def getnew_date(i):
    eachnew_soup = BeautifulSoup(str(news_list[i]), 'html.parser')
    date1 = eachnew_soup.select('td')[2].text
    date2 = date1.split('/')
    datey = date2[0]
    datem = date2[1].zfill(2)
    dated = date2[2].zfill(3)
    date = datey[1:5] + '.'+ datem + '.'+dated[0:2]
    return date

# 获取单个标题
def getinf_name(i):
    eachinf_soup = BeautifulSoup(str(inf_list[i]), 'html.parser')
    name =  eachinf_soup.select('a')[0].text
    return name

# 获取单个时间
def getinf_date(i):
    eachinf_soup = BeautifulSoup(str(inf_list[i]), 'html.parser')
    date1 = eachinf_soup.select('td')[2].text
    date2 = date1.split('/')
    datey = date2[0]
    datem = date2[1].zfill(2)
    dated = date2[2].zfill(3)
    date = datey[1:5] + '.' + datem + '.' + dated[0:2]
    return date

# 获取单个内容
def get_content(every_soup):
    contentall = every_soup.select('table')[16]
    content1 = BeautifulSoup(str(contentall),'html.parser')
    content = content1.select('tr')[0].text
    return content

# 获取单个图片
def get_pic(every_soup):
    picall = every_soup.select('table')[16]
    pic1 = BeautifulSoup(str(picall),'html.parser')
    pic = pic1.select('tr')
    pic_soup = BeautifulSoup(str(pic),'html.parser')
    pic_list = pic_soup.select('img')
    picsrc1 = []
    for i in range(len(pic_list)):
        p1 = r'http://em.xidian.edu.cn'
        pattern1 = re.compile(p1)
        src1 = str(re.search(pattern1,str(pic_list[i])))
        if src1 == 'None':
            src = 'http://em.xidian.edu.cn'+pic_list[i]['src']
        else:
            src = str(pic_list[i]['src'])
        picsrc1.append(src)
    picsrc = str(picsrc1)
    return picsrc

# 获取单个新闻的id, publisher, is_hot, cateory_id, name, link, date, content, picsrc,并添加到数据库xidian
def everynew(i):
    #id = startid + i
    number = i
    xueyuan = '6'
    category_id = '1'
    link = getnew_link(i)
    every_html = requests.get(link)
    every_html.encoding = 'gbk'
    every_soup = BeautifulSoup(every_html.text, 'html.parser')
    name = getnew_name(i)
    date = getnew_date(i)
    content = get_content(every_soup)
    picsrc = get_pic(every_soup)
    # 连接数据库
    connection = pymysql.connect(host='127.0.0.1', user='root', password='mysqladmin', db='hiteacher', charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)
    # 执行sql语句
    try:
        with connection.cursor() as cursor:
            sql = "insert into `xiaoxi`(`xueyuan`,`category_id`,`name`,`link`,`date`,`content`,`picsrc`)values(%s,%s,%s,%s,%s,%s,%s)"
            # 使用 execute()  方法执行 SQL 查询
            cursor.execute(sql, (xueyuan, category_id, name, link, date, content, picsrc))
            connection.commit()
    finally:
        connection.close()

# 获取单个通知的id, publisher, is_hot, cateory_id, name, link, date, content, picsrc,并添加到数据库xidian
def everyinf(i):
    #id = startid + news_num +i
    number = i
    xueyuan = '6'
    category_id = '2'
    link = getinf_link(i)
    every_html = requests.get(link)
    every_html.encoding = 'gbk'
    every_soup = BeautifulSoup(every_html.text, 'html.parser')
    name = getinf_name(i)
    date = getinf_date(i)
    content = get_content(every_soup)
    picsrc = get_pic(every_soup)

    # 连接数据库
    connection = pymysql.connect(host='127.0.0.1', user='root', password='mysqladmin', db='hiteacher', charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)
    # 执行sql语句
    try:
        with connection.cursor() as cursor:
            sql = "insert into `xiaoxi`(`xueyuan`,`category_id`,`name`,`link`,`date`,`content`)values(%s,%s,%s,%s,%s,%s)"
            # 使用 execute()  方法执行 SQL 查询
            cursor.execute(sql, (xueyuan, category_id, name, link, date, content))
            connection.commit()
    finally:
        connection.close()

def sel():

    for i in range(news_num):
        try:
            everynew(i)
        except:
            i+1
    for i in range(inf_num):
        try:
            everyinf(i)
        except:
            i+1


