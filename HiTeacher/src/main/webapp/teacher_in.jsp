<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<link rel="shortcut icon" href="images/logo.png" type="image/x-icon" />
	<script type="text/javascript" src="./js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="./js/bootstrap.min.js"></script>
	<script type="text/javascript" src="./js/npm.js"></script>
	<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="./css/bootstrap-theme.min.css">
</head>
<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<nav class="navbar navbar-default navbar-static-top" role="navigation">
				<div class="navbar-header">
					  <img src="images/logo_min.png" style="margin-top: 6px">
					  <a class="navbar-brand" href="#">快铃</a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" ><font id="courseName" ></font><font id="courseId" style="display: none;"></font>&nbsp;<strong class="caret"></strong></a>
							 <ul class="dropdown-menu" id="courseList">
							</ul> 
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<!-- <li>
							 <a href="#">Link</a>
						</li> -->
						<li class="dropdown" style="margin-right: 20px ">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown" ><font id="name"></font>&nbsp;<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									 <a id="modal-998709" href="#modal-container-998709"  data-toggle="modal">个人信息</a>
								</li>
								<li>
									 <!-- <a href="#">修改密码</a> -->
								</li>
								<li>
									 <!-- <a href="#">使用帮助</a> -->
								</li>
								<li class="divider">
								</li>
								<li>
									 <a href="#" id="quit">退出</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>

			<div class="modal fade" id="modal-container-998709" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title" id="myModalLabel">
								个人信息
							</h4>
						</div>
						<div class="modal-body">
							姓名：<font id="Inname"></font><br>
							手机号：<font><%=session.getAttribute("phone")%></font><br>
							<!-- 课程:<font>工程优化</font> -->
						</div>
						<div class="modal-footer">
							 <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button> <button type="button" class="btn btn-primary">保存</button>
						</div>
					</div>
				</div>
			</div>

			<div class="row clearfix">
				<div class="col-md-9 column">
					<div class="tabbable" id="tabs-705818">
						<ul class="nav nav-tabs">
							<li class="active">
								 <a href="#panel-282045" data-toggle="tab">作业</a>
							</li>
							<!-- <li>
								 <a href="#panel-464779" data-toggle="tab">签到</a>
							</li>
							<li>
								 <a href="#panel-464780" data-toggle="tab">课件</a>
							</li> -->
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="panel-282045"><br/>
							
									<div class="row clearfix">
										<div class="col-md-12 column">
											 班级：
											<div class="btn-group">
												 <button class="btn btn-default" id="class">班级</button> 
												 <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
												 <b class="caret"></b>
												 </button>
												 <ul class="dropdown-menu"  role="menu" aria-labelledby="myTabDrop1" id="classList">
												</ul>
											</div>
											作业：
											<div class="btn-group">
												<button class="btn btn-default" id="homework">作业</button>
												<button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu" id="homeworkList">
													<li>请先选择班级</li>
												</ul>
											</div>
											<div id="homeworkMess" style="color: red;"></div><br>
											<table class="table  table-hover">
												<thead>
													<tr>
														<th>编号</th>
														<th>学生姓名</th>
														<th>学号</th>
														<th>状态</th>
														<th>分数</th>
													</tr>
												</thead>
												<tbody id="stuList">
													
												</tbody>
											</table>
										</div>
									</div>
								
							
							</div>
							<!-- <div class="tab-pane" id="panel-464779">
								<p>
									签到
								</p>
							</div>
							<div class="tab-pane" id="panel-464780">
								<p>
									课件
								</p>
							</div> -->
						</div>
					</div>
				</div>
				 <div class="col-md-3 column">
					<h2>
						课堂信息
					</h2>
					<p id="className">名称：工程优化</p>
					<p>时间：2017春</p>
				</div> 
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
$(function(){
	//获取老师姓名和电话
	var phone = "<%=session.getAttribute("phone")%>"; 
    if (phone == "null"){
    	alert("请先登录！");
    	window.location.href = "index.jsp";
    } 
	function getQueryString(key){
	    var reg = new RegExp("(^|&)"+key+"=([^&]*)(&|$)");
	    var result = window.location.search.substr(1).match(reg);
	    return result?decodeURIComponent(result[2]):null;
	}
	var name = getQueryString('name');
	var id = getQueryString('id');
	var courseId;
	var classnumber;
	var homeworkId;
	var courseName;
	$('#name').html(name);
	$('#Inname').html(name);
	
	getCourse();
	getClass();
	getHomework();
	
	courseChange();
	classChange();
	homeworkChange();
	
	
	
	
	//获取课程列表
	function getCourse(){
		$.ajax({
			url:'homework/listTC',
			type:'GET',
			data:{'teacherId':id},
			contentType:'application/x-www-form-urlencoded',
			dataType:'json',
			async:false,
			success:function(data){
				//alert(data.status);
				courseName = data.data[0].courseName;
				$('#courseName').html(courseName);
				courseId = data.data[0].courseId;
				$('#courseId').html(courseId);
				$('#className').html("名称： " + courseName);
				$.each(data.data, function(idx, obj) {  
					var message = "<li ><a href='#' id="+ obj.courseId +" class='courseLi'>"+obj.courseName+"</a></li>";
					$('#courseList').append(message);
				}); 
			}
		});
	}
	
	//默认显示第一节课的所有班
	function getClass(){
		 $.ajax({
				url:'homework/listClassnumber',
				type:'GET',
				data:{'teacherId':id,'courseId':courseId},
				contentType:'application/x-www-form-urlencoded',
				dataType:'json',
				async:false,
				success:function(data){
					//classnumber = data.data[0];
					//$('#class').html(classnumber);
					$.each(data.data, function(idx, obj) {  
						var message = "<li><a class='classNumb' href='#'>"+ obj +"</a></li>";
						$('#classList').append(message);
					});
				}
			});
	}
	 
	//默认显示第一节课第一个班的所有作业次数
	function getHomework(){
		$.ajax({
			url:'homework/listHomeworkNumber',
			type:'GET',
			data:{'teacherId':id,'courseId':courseId,'classnumber':classnumber},
			contentType:'application/x-www-form-urlencoded',
			dataType:'json',
			async:false,
			success:function(data){
				//homeworkId = data.data[0].id;
				//$('#homework').html(homeworkId);
				$.each(data.data, function(idx, obj) { 
					var date = new Date(obj.assignTime);
					var message = "<li><a id="+obj.id+" class='homeworkNum' href='#'>"+date.toLocaleDateString()+"</a></li>";
					$('#homeworkList').append(message);
				});
			}
		}); 
	}
	 
	
	
	//更换课程
	function courseChange(){
		$('.courseLi').click(function(){
			var nn = this;
			courseName = nn.innerText;
			$('#courseName').html(courseName);
			$('#courseId').html(nn.id);
			courseId = $('#courseId').text();
			$('#class').html("班级");
			$('#homework').html("作业");
			$('#className').html("名称： " + courseName);
			
			$('#classList').html('');
			$('#homeworkList').html('请先选择班级');
			$('#homeworkMess').html('');
			$('#stuList').html('');

			//获取班级列表
			getClass();
			classChange();
		});
	}
	
	
	//班级选择
	function classChange(){
		$('.classNumb').click(function(){
			var nn = this;
			classnumber = nn.innerText;
			$('#class').html(classnumber);
			courseId = $('#courseId').text();
			classnumber = $('#class').text();
			$('#homework').html("作业");
			$('#homeworkList').html('');
			$('#homeworkMess').html('');
			$('#stuList').html('');
			//获取作业列表
			getHomework();
			homeworkChange();
		}); 
	}
	//作业选择
	function homeworkChange(){
		$('.homeworkNum').click(function(){
			var nn = this;
			$('#homework').html(nn.innerText);
			courseId = $('#courseId').text();
			classnumber = $('#class').text();
			homeworkId = nn.id;
			$('#stuList').html("");
			$('#homeworkMess').html('');
			$('#stuList').html('');
			//获取所有作业
			$.ajax({
				url:'homework/listAll',
				type:'GET',
				data:{'homeworkRecordId':homeworkId},
				contentType:'application/x-www-form-urlencoded',
				dataType:'json',
				async:false,
				success:function(data){
					$('#homeworkMess').html("已提交:" + data.data.commitedListSize + " 未提交：" + data.data.uncommitedListSize + "  已批阅：" + data.data.checkedListSize+" 未批阅：" + data.data.uncheckedListSize);
					var index = 0;
					$.each(data.data.uncheckedList, function(idx, obj) {  
						   if(obj.hwScore == null){
							   obj.hwScore = "";
						   }
						   var msg = "<tr><td>"+(++idx)+"</td><td>"+obj.stuName+"</td><td>"+obj.stuId+"</td><td><a href='handleImg.jsp?homeworkId="+obj.id+"&picUrl="+obj.hwPicture+"'>未批阅</a></td><td>"+obj.hwScore+"</td></tr>";
						   $('#stuList').append(msg);
						   index = idx;
						});  
					$.each(data.data.checkedList, function(idx, obj) {  
						   var msg = "<tr><td>"+(++index)+"</td><td>"+obj.stuName+"</td><td>"+obj.stuId+"</td><td>已批阅</td><td>"+obj.hwScore+"</td></tr>";
						   $('#stuList').append(msg);
						});
					$.each(data.data.uncommitedList, function(idx, obj) {  
						   var msg = "<tr><td>"+(++index)+"</td><td>"+obj.stuName+"</td><td>"+obj.stuId+"</td><td>未提交</td><td></td></tr>";
						   $('#stuList').append(msg);
						}); 
				}
			});
		}); 
	}
	 
	//用户退出
	$('#quit').click(function(){
		window.location.href = "index.jsp";
	});
});
</script>
</body>
</html>