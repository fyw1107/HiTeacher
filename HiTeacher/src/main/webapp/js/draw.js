/** Created by ty 2017/3/13.
 */
$(document).ready(function(){
	
	//获取图片url
    function getQueryString(key){
        var reg = new RegExp("(^|&)"+key+"=([^&]*)(&|$)");
        var result = window.location.search.substr(1).match(reg);
        return result?decodeURIComponent(result[2]):null;
    }
    var homeworkId = getQueryString('homeworkId');
    var picUrl = getQueryString('picUrl');
    var picList = picUrl.split(',');
    for(var i=0;i<picList.length;i++){
    	var mess = "<img class='imgGet' src="+ picList[i] +">";
    	$('#hideImg').append(mess);
    }
    var canvas, board, img, imgList;
    
    //获取canvas
    canvas = document.getElementById('myCanvas');
    //获取绘图上下文，将来就是用这个上下文在画板上绘制图形
    board = canvas.getContext('2d');
    
    
    //imgGet赋值
    //$('.imgGet').attr('src',picUrl+'/1.PNG');
    //获取class="imgGet"的所有标签
    imgList = document.getElementsByClassName("imgGet");

    alert(imgList[0]);
    //得到每张图片
    for(var i = 0; i<imgList.length; i++){
        //为每个从服务器得到的图分配id
        imgList[i].id = "imgGet"+i;
    }

    
 
    // canvas.width = $("#imgGet0").width();
    // canvas.height = $("#imgGet0").height();


    //插入图片背景
    var yImg = new Image();

    yImg.src = $("#imgGet0").attr("src");
   
    yImg.onload = function(){
        //设置画布大小，适配图片(不能设置style.width,否则更改的是画板大小，会产生拉伸变形)
        canvas.width = yImg.width;
        canvas.height = yImg.height;
        board.drawImage(yImg,0,0);
        //设置画笔宽度和颜色
        board.lineWidth = 5;
        board.strokeStyle = "red";
    };

  
    

    //统计按“下一张”按钮的次数
    var clickTime=0;

    //按下“下一张”触发事件
    $('#next').click(function(){
        //保存修改后的图片
        //var upImg = new Image();
        var dataUrl = canvas.toDataURL("image/png");
        // dataUrl = dataUrl.replace(/\&/g,"%26");
        // dataUrl = dataUrl.replace(/\+/g,"%2B");
       // $("#imgShow").attr("src",dataUrl) ;

        //向后台传输处理后的图片
        $.ajax({
            type: "POST",
            url: "homework/checkPicture",
            data: {url:picList[clickTime],imgBase64:dataUrl}
        });

        //判断是否读取完图片
        if(clickTime>=imgList.length-1){
            alert("已经是最后一页了！");
            return;
        }
        ++clickTime;
        //清理上一张图片
        board.clearRect(0, 0, canvas.width, canvas.height);
        //画下一张图片
        canvas.width = $("#imgGet"+clickTime).width();
        canvas.height = $("#imgGet"+clickTime).height();
        yImg.src = $("#imgGet"+clickTime).attr("src");
        board.drawImage(yImg,0,0);
        //设置画笔宽度和颜色
        board.lineWidth = 5;
        board.strokeStyle = "red";
    });
    //撤销标记
    $("#clean").click(function(){
        board.clearRect(0, 0, canvas.width, canvas.height);
        board.drawImage(yImg,0,0);
    });
    //提交分数
    $('#sub').click(function(){
    	swal({
    		title: "确认提交?",
    		text: "给出的最终成绩为："+$('#score').val(),
    		type: "warning",
    		showCancelButton: true,
    		confirmButtonColor: "#DD6B55",
    		confirmButtonText: "Yes！",
    		closeOnConfirm: false
    		},
    		function(){
    			var score = $('#score').val();
    			$.ajax({
    				url: 'homework/checkScore',
    				data: {homeworkId:homeworkId,score:score},
    				success: function(data){
    					if (data == 'success'){
    						swal("提交!", "分数上传成功！", "success");
    						function go(){
    							window.history.go(-1)
    						}
    						setTimeout(go,1500);
    					}
    					else 
    					swal(data,"","fail");
    				}
    			});
    		});
    });

//声明变量，表示鼠标的按下状态,false表示未按下，true表示按下
    var mousePress = false;
    var last = null;

    function beginDraw(event) {
        mousePress = true;
    }

    function drawing(event) {
        event.preventDefault();
        if (!mousePress) return;
        var xy = pos(event);
        if (last != null) {
            board.beginPath();
            board.moveTo(last.x, last.y);
            board.lineTo(xy.x, xy.y);
            board.stroke();
        }
        last = xy;
    }

    function endDraw(event) {
        mousePress = false;
        event.preventDefault();
        last = null;
    }

    function pos(event) {
        var x, y;
        if (isTouch(event)) {
            x = event.touches[0].pageX;
            y = event.touches[0].pageY;
        } else {
            //x = event.offsetX + event.target.offsetLeft;
            x = event.offsetX;
            y = event.offsetY;
        }
        return {
            x: x,
            y: y
        };
    }

    function log(msg) {
        var log = document.getElementById('log');
        var val = log.value;
        log.value = msg + 'n' + val;
    }

    function isTouch(event) {
        var type = event.type;
        //alert(type);
        if (type.indexOf('touch') >= 0) {
            return true;
        } else {
            return false;
        }
    }



//为鼠标按下事件指定要执行的函数
    canvas.onmousedown = beginDraw;
//为鼠标移动事件指定要执行的函数
    canvas.onmousemove = drawing;
//为鼠标松开事件指定要执行的函数
    canvas.onmouseup = endDraw;
//为canvas添加touchstart事件，当手指在触摸屏设备(如手机)中按下时触发
    canvas.addEventListener('touchstart', beginDraw, false);
//为canvas添加touchmove事件，当手指在触摸屏设备(如手机)中按下并移动时触发
    canvas.addEventListener('touchmove', drawing, false);
//为canvas添加touchend事件，当手指在触摸屏设备(如手机)中离开时触发
    canvas.addEventListener('touchend', endDraw, false);
});
