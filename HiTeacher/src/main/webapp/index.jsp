<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>登录</title>
    <link rel="shortcut icon" href="images/logo.png" type="image/x-icon" />
    <script src='./js/jquery-1.9.0.min.js' type='text/javascript' charset='utf-8'></script>
    <script src='./js/jquery_utils.js' type='text/javascript' charset='utf-8'></script>
    <script src='./js/$17.min.js' type='text/javascript' charset='utf-8'></script>
    <link href='./css/main2.css' rel='stylesheet' type='text/css' charset='utf-8' />
    <link href='./css/main1.css' rel='stylesheet' type='text/css' charset='utf-8' />
    <script src='./js/template.min.js' type='text/javascript' charset='utf-8'></script>
    <script src='./js/flexslider.js' type='text/javascript' charset='utf-8'></script>
    <script src='./js/jquery.realperson.min.js' type='text/javascript' charset='utf-8'></script>
    <script src='./js/impromptu.js' type='text/javascript' charset='utf-8'></script> <!--[if IE 6]>
    <script src='./js/DD.js' type='text/javascript' charset='utf-8'></script>
    <script type="text/javascript">
        $(function(){ if(typeof DD_belatedPNG === "object"){DD_belatedPNG.fix('*');} });
    </script>
    <![endif]--><script type="text/javascript">

</script>
</head>
<body class="">
<div class="zy-header JS-indexPageBox">
    <div class="zy-inner">
        <div class="zy-nav">
            <a href="#" class="logo"></a>
            <div class="navList">
                <!--  <a href="#" class="menu active" title="首页">首页</a>
                <a href="#" class="menu" title="产品概念">产品概念</a>
                <a href="#" class="menu" title="各界声音">各界声音</a>
                <a href="#" class="menu" title="新闻中心">新闻中心</a>
                <a href="#" class="menu" title="关于我们">关于我们</a>
                <a href="#" class="menu" title="加入我们">加入我们</a>-->
            </div>
        </div>
        <!-- <div class="rightIn">
            <a href="#" target="_blank" class="load"><i class="phone-icon"></i>APP下载</a>
        </div> -->
    </div>
</div>

<div class="zy-homeContainer JS-indexPageBox" style="height: 100%;">
    <div class="JS-indexSwitch-main">
        <ul class="zy-homeBox slides">
            <li class="homeItem homeItem02" style="width: 180%">
                <div class="innerBox">
                    <div class="info">
                        <div class="title01" style="font-size: 54px">让作业批改变得简单</div>
                        <div class="loginBtn clearfix">
                            <div class="zy-header" style="position: static; display: inline-block; width: auto; float: right; padding-left: 20px;">
                                <!-- <div class="rightIn" style="display: inline-block; float: none; margin: 0;padding: 0;">
                                    <a href="#" target="_blank" class="load" style="padding: 0; border: none; float: none; width: auto; font-size: 16px; color: #000;"><i class="phone-icon"></i>APP下载</a>
                                </div> -->
                            </div>
                            <!--  <a href="javascript:;" class="JS-register-main">注册</a>  -->
                            <a href="javascript:;" class="active JS-login-main">登录</a>
                        </div>
                    </div>
                </div>
            </li>


            <li class="homeItem homeItem01" style="width: 180%">
                <div class="innerBox">
                    <div class="info">
                        <div class="title01" style="font-size: 54px">让作业批改变得简单</div>
                        <div class="loginBtn clearfix">
                            <div class="zy-header" style="position: static; display: inline-block; width: auto; float: right; padding-left: 20px;">
                                <!-- <div class="rightIn" style="display: inline-block; float: none; margin: 0;padding: 0;">
                                    <a href="#" target="_blank" class="load" style="padding: 0; border: none; float: none; width: auto; font-size: 16px; color: #000;"><i class="phone-icon"></i>APP下载</a>
                                </div> -->
                            </div>
                            <!--  <a href="javascript:;" class="JS-register-main">注册</a>-->
                            <a href="javascript:;" class="active JS-login-main">登录</a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <ul class="zy-scrollNav JS-indexSwitch-mode">
        <li style="left: -16px;"><a>1</a></li>
        <li style="right: -16px;"><a>2</a></li>
    </ul>
</div>

<div id="RenderMain" class="JS-indexPageBox"></div>


<script type="text/html" id="T:LoginTemplateMain">
    <div class="loginPop-box loginBg">
        <div class="loginPop-close JS-clear-btn" id="close"></div>
        <div class="lop-inner">
            <div class="logo"></div>
            <h1>登录</h1>
            <form id="login" class="JS-formSubmit" enctype="multipart/form-data" method="POST" >
                <div class="lop-content">
                    <div class="c-text">
                        <span class="login-icon icon-5"></span>
                        <input type="text" value="" name="phone" tabIndex="1"
                               class="JS-inputEvent" id="index_login_username" placeholder="手机号"/>
                        <div class="errorTips" id="phoneError"></div>
                    </div>
                    <div class="c-text">
                        <span class="login-icon icon-4"></span>
                        <input type="password" id="index_login_password" name="password" class="JS-inputEvent"
                               tabIndex="2" placeholder="输入密码"/>
                        <div class="errorTips" id="passwordError"></div>
                    </div>
                    <div class="c-text">
                        <a href="javascript:void(0);" class="info active JS-rememberMe-btn">
                            <input name="_spring_security_remember_me" value="on" type="checkbox" checked="checked"
                                   style="display: none;"/>
                            <span class="login-icon icon-6"></span>
                        </a>
                        <!-- <a href="#" target="_blank" class="info">忘记密码</a> -->
                    </div>
                </div>
                <div class="loginPop-footer">
                    <button value="登录" class="login-btn" id="_a_loginForm" name="_a_loginForm" tabIndex="3" onclick="login();return false;">登录</button>
                </div>
            </form>
        </div>
    </div>
</script>
<script type="text/javascript">
function login(){
	$.ajax({			
		url:'user/login',
		type:'POST',
		data:$('#login').serialize(),
		dataType:'json',
		success:function(data){
			if(data.status == 200){
				alert("欢迎回来："+data.data.teacher.teacherName);/* resultBean.resultMap.teacher.teacherName */
				//if(data.data.user_type == 1){
					/* 表示重新定向到新页面，同时刷新打开的这个页面 */
					window.location.href='teacher_in.jsp?name='+data.data.teacher.teacherName+"&id="+data.data.teacher.teacherId+"&courseName="+data.data.courseList[0].courseName+"&courseId="+data.data.courseList[0].courseId;
				//}else{
					//indow.location.href='textUp.html?name='+data.data.nickName+"&phone="+data.data.loginPhone;
				//}
			}else if(data.status == 105){
				$('#passwordError').show();
				$('#passwordError').html("密码错误");
			}else if(data.status == 107){
				alert("请用老师账号登陆！");	
			}else{
				$('#phoneError').show();
				$('#phoneError').html("用户不存在");
			}
		},
		error:function(data){
			alert("登陆失败！");
		}
	});
};
</script>

<script src='./js/loginv5.js' type='text/javascript' charset='utf-8'></script>
<script type="text/javascript">
    /*ga统计*/
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    /*根据用户类型区分 老师抽样率100% 学生抽样率1% */
    var _ga_trackingId = 'UA-38181315-1',_ga_sampleRate = 1;
    ga('create', {
        trackingId: _ga_trackingId,
        cookieDomain: 'auto',
        sampleRate: _ga_sampleRate
    });
    ga('send', 'pageview');
</script>
</body>
</html>
