/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50022
Source Host           : localhost:3306
Source Database       : hiteacher_1

Target Server Type    : MYSQL
Target Server Version : 50022
File Encoding         : 65001

Date: 2017-05-18 22:34:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for homework
-- ----------------------------
DROP TABLE IF EXISTS `homework`;
CREATE TABLE `homework` (
  `id` int(11) NOT NULL auto_increment COMMENT '具体作业记录id',
  `course_id` varchar(64) default NULL COMMENT '课程编号',
  `classnumber` varchar(10) default NULL COMMENT '班号',
  `teacher_id` bigint(20) default NULL COMMENT '教师姓名',
  `stu_id` bigint(20) default NULL COMMENT '学生id',
  `hw_content` varchar(1000) default NULL COMMENT '教师上传的作业内容',
  `hw_picture` varchar(255) default NULL COMMENT '提交作业图片路径',
  `assign_time` datetime default NULL COMMENT '作业布置时间(教师)',
  `commit_time` datetime default NULL COMMENT '最后提交时间(学生)',
  `hw_score` varchar(20) default NULL COMMENT '作业评分',
  `record_id` int(11) default NULL COMMENT '统计作业记录id',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of homework
-- ----------------------------
INSERT INTO `homework` VALUES ('1', '1112', '01', '201', '101', '英语作文两篇,1000字', 'C:\\tomcat\\webapps\\HiTeacher\\homework\\1112\\01\\101\\1495096498565', '2017-05-17 23:11:35', '2017-05-18 16:35:29', null, '1');
INSERT INTO `homework` VALUES ('2', '1112', '01', '201', '102', '英语作文两篇,1000字', 'C:\\tomcat\\webapps\\HiTeacher\\homework\\1112\\01\\102\\1495076669401', '2017-05-17 23:11:35', '2017-05-18 11:04:29', 'A', '1');
INSERT INTO `homework` VALUES ('3', '1112', '01', '201', '103', '英语作文两篇,1000字', 'C:\\tomcat\\webapps\\HiTeacher\\homework\\1112\\01\\103\\1495082029703', '2017-05-17 23:11:35', '2017-05-18 16:32:57', null, '1');
INSERT INTO `homework` VALUES ('4', '1112', '01', '201', '104', '英语作文两篇,1000字', null, '2017-05-17 23:11:35', null, null, '1');
INSERT INTO `homework` VALUES ('5', '1112', '01', '201', '105', '英语作文两篇,1000字', null, '2017-05-17 23:11:35', null, null, '1');
INSERT INTO `homework` VALUES ('6', '1112', '01', '201', '106', '英语作文两篇,1000字', null, '2017-05-17 23:11:35', null, null, '1');
INSERT INTO `homework` VALUES ('7', '1112', '01', '201', '107', '英语作文两篇,1000字', null, '2017-05-17 23:11:35', null, null, '1');
INSERT INTO `homework` VALUES ('8', '1112', '01', '201', '108', '英语作文两篇,1000字', null, '2017-05-17 23:11:35', null, null, '1');
INSERT INTO `homework` VALUES ('9', '1112', '01', '201', '109', '英语作文两篇,1000字', null, '2017-05-17 23:11:35', null, null, '1');
INSERT INTO `homework` VALUES ('10', '1112', '01', '201', '110', '英语作文两篇,1000字', null, '2017-05-17 23:11:35', null, null, '1');
INSERT INTO `homework` VALUES ('21', '1115', '02', '202', '101', '英语作文两篇,1000字', 'C:\\tomcat\\webapps\\HiTeacher\\homework\\1115\\02\\101\\1495096634115', '2017-05-18 10:18:27', '2017-05-18 16:37:14', 'B', '10');
INSERT INTO `homework` VALUES ('22', '1115', '02', '202', '102', '英语作文两篇,1000字', null, '2017-05-18 10:18:27', null, null, '10');
INSERT INTO `homework` VALUES ('23', '1115', '02', '202', '103', '英语作文两篇,1000字', null, '2017-05-18 10:18:27', null, null, '10');
INSERT INTO `homework` VALUES ('24', '1115', '02', '202', '104', '英语作文两篇,1000字', null, '2017-05-18 10:18:27', null, null, '10');
INSERT INTO `homework` VALUES ('25', '1115', '02', '202', '105', '英语作文两篇,1000字', null, '2017-05-18 10:18:27', null, null, '10');
INSERT INTO `homework` VALUES ('26', '1115', '02', '202', '106', '英语作文两篇,1000字', null, '2017-05-18 10:18:27', null, null, '10');
INSERT INTO `homework` VALUES ('27', '1115', '02', '202', '107', '英语作文两篇,1000字', null, '2017-05-18 10:18:27', null, null, '10');
INSERT INTO `homework` VALUES ('28', '1115', '02', '202', '108', '英语作文两篇,1000字', null, '2017-05-18 10:18:27', null, null, '10');
INSERT INTO `homework` VALUES ('29', '1115', '02', '202', '109', '英语作文两篇,1000字', null, '2017-05-18 10:18:27', null, null, '10');
INSERT INTO `homework` VALUES ('30', '1115', '02', '202', '110', '英语作文两篇,1000字', null, '2017-05-18 10:18:27', null, null, '10');
SET FOREIGN_KEY_CHECKS=1;
