/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50022
Source Host           : localhost:3306
Source Database       : hiteacher_1

Target Server Type    : MYSQL
Target Server Version : 50022
File Encoding         : 65001

Date: 2017-05-18 22:35:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `stu_id` bigint(30) NOT NULL,
  `user_id` varchar(32) NOT NULL,
  `stu_password` varchar(32) NOT NULL,
  `stu_name` varchar(64) default NULL,
  `school_id` int(3) default NULL,
  `xueyuan` int(3) default NULL,
  `xueyuan_name` varchar(32) default NULL,
  `nianji` varchar(32) default NULL,
  `class` varchar(32) default NULL,
  `major` varchar(32) default NULL,
  `advisor` varchar(20) default NULL,
  PRIMARY KEY  (`stu_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES ('100', '100', '123456', '花木兰同学', null, null, null, null, null, null, null);
INSERT INTO `student` VALUES ('101', '101', '123456', '荆轲同学', '1', null, null, null, null, null, null);
INSERT INTO `student` VALUES ('102', '102', '123456', '张飞同学', '2', null, null, null, null, null, null);
INSERT INTO `student` VALUES ('103', '103', '123456', '大乔同学', '3', null, null, null, null, null, null);
INSERT INTO `student` VALUES ('104', '104', '123456', '李白同学', '4', null, null, null, null, null, null);
INSERT INTO `student` VALUES ('105', '105', '123456', '韩信同学', '5', null, null, null, null, null, null);
INSERT INTO `student` VALUES ('106', '106', '123456', '东皇太一同学', '6', null, null, null, null, null, null);
INSERT INTO `student` VALUES ('107', '107', '123456', '黄忠同学', '7', null, null, null, null, null, null);
INSERT INTO `student` VALUES ('108', '108', '123456', '盖聂同学', '8', null, null, null, null, null, null);
INSERT INTO `student` VALUES ('109', '109', '123456', '赵云同学', '9', null, null, null, null, null, null);
INSERT INTO `student` VALUES ('110', '110', '123456', '关羽同学', '10', null, null, null, null, null, null);
INSERT INTO `student` VALUES ('14010520029', '01', '032043', ' 江岚', '1', null, ' ', ' 2014', ' 1401052', ' 信息工程', null);
SET FOREIGN_KEY_CHECKS=1;
