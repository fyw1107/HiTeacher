/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50022
Source Host           : localhost:3306
Source Database       : hiteacher_1

Target Server Type    : MYSQL
Target Server Version : 50022
File Encoding         : 65001

Date: 2017-05-18 22:35:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for newscollection
-- ----------------------------
DROP TABLE IF EXISTS `newscollection`;
CREATE TABLE `newscollection` (
  `collectionId` int(11) NOT NULL auto_increment,
  `newsId` int(11) NOT NULL COMMENT '新闻id',
  `stuId` bigint(20) default NULL COMMENT '学生id',
  `teacherId` int(11) default NULL COMMENT '教师id',
  PRIMARY KEY  (`collectionId`),
  UNIQUE KEY `u_newsId_stuId_teacherId` (`newsId`,`stuId`,`teacherId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of newscollection
-- ----------------------------
INSERT INTO `newscollection` VALUES ('8', '1', '103', '-1');
INSERT INTO `newscollection` VALUES ('17', '2', '-1', '105');
INSERT INTO `newscollection` VALUES ('23', '2', '108', '-1');
INSERT INTO `newscollection` VALUES ('15', '3', '-1', '104');
INSERT INTO `newscollection` VALUES ('16', '3', '-1', '105');
INSERT INTO `newscollection` VALUES ('2', '3', '-1', '201');
INSERT INTO `newscollection` VALUES ('1', '3', '102', '-1');
INSERT INTO `newscollection` VALUES ('3', '4', '102', '-1');
INSERT INTO `newscollection` VALUES ('22', '4', '109', '-1');
INSERT INTO `newscollection` VALUES ('18', '5', '-1', '105');
INSERT INTO `newscollection` VALUES ('4', '5', '102', '-1');
INSERT INTO `newscollection` VALUES ('5', '6', '102', '-1');
INSERT INTO `newscollection` VALUES ('19', '7', '-1', '105');
INSERT INTO `newscollection` VALUES ('20', '7', '-1', '107');
INSERT INTO `newscollection` VALUES ('21', '7', '-1', '109');
INSERT INTO `newscollection` VALUES ('6', '7', '102', '-1');
INSERT INTO `newscollection` VALUES ('7', '8', '102', '-1');
INSERT INTO `newscollection` VALUES ('9', '9', '103', '-1');
INSERT INTO `newscollection` VALUES ('10', '9', '104', '-1');
INSERT INTO `newscollection` VALUES ('11', '9', '105', '-1');
INSERT INTO `newscollection` VALUES ('13', '10', '-1', '103');
INSERT INTO `newscollection` VALUES ('14', '10', '-1', '108');
SET FOREIGN_KEY_CHECKS=1;
