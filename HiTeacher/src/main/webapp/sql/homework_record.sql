/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50022
Source Host           : localhost:3306
Source Database       : hiteacher_1

Target Server Type    : MYSQL
Target Server Version : 50022
File Encoding         : 65001

Date: 2017-05-18 22:34:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for homework_record
-- ----------------------------
DROP TABLE IF EXISTS `homework_record`;
CREATE TABLE `homework_record` (
  `id` int(11) NOT NULL auto_increment COMMENT '统计作业记录id',
  `course_id` varchar(64) default NULL COMMENT '课程编号',
  `course_name` varchar(64) default NULL COMMENT '课程名称',
  `classnumber` varchar(10) default NULL COMMENT '班号',
  `teacher_id` bigint(20) default NULL COMMENT '教师姓名',
  `assign_time` datetime default NULL COMMENT '作业布置时间(教师)',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of homework_record
-- ----------------------------
INSERT INTO `homework_record` VALUES ('1', '1112', '英语写作', '01', '201', '2017-05-17 22:45:40');
INSERT INTO `homework_record` VALUES ('2', '1112', '英语写作', '01', '201', '2017-05-17 22:45:54');
INSERT INTO `homework_record` VALUES ('3', '1112', '英语写作', '01', '201', '2017-05-17 22:46:06');
INSERT INTO `homework_record` VALUES ('4', '1112', '英语写作', '01', '201', '2017-05-17 22:47:52');
INSERT INTO `homework_record` VALUES ('5', '1112', '英语写作', '01', '201', '2017-05-17 22:48:08');
INSERT INTO `homework_record` VALUES ('6', '1112', '英语写作', '01', '201', '2017-05-17 22:51:46');
INSERT INTO `homework_record` VALUES ('7', '1112', '英语写作', '01', '201', '2017-05-17 22:52:24');
INSERT INTO `homework_record` VALUES ('8', '1112', '英语写作', '01', '201', '2017-05-17 22:52:53');
INSERT INTO `homework_record` VALUES ('9', '1112', '英语写作', '01', '201', '2017-05-17 23:06:17');
INSERT INTO `homework_record` VALUES ('10', '1115', '商务英语', '02', '202', '2017-05-18 10:17:25');
SET FOREIGN_KEY_CHECKS=1;
