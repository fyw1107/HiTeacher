<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    
    <METAHTTP-EQUIV="Pragma"CONTENT="no-cache">
	<METAHTTP-EQUIV="Cache-Control"CONTENT="no-cache">
	<METAHTTP-EQUIV="Expires"CONTENT="0">
	
    <title>批阅</title>
    <link rel="shortcut icon" href="images/logo.png" type="image/x-icon" />   
    <script type='text/javascript' src='./js/jquery-1.12.0.min.js' ></script>
    <script type="text/javascript" src="./js/sweetalert.min.js"></script>
    <script type='text/javascript' src='./js/draw.js' ></script>
    <link rel='stylesheet' href='css/main.css'>
    <link rel="stylesheet" href="css/sweetalert.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css" />
</head>
<body>
<div id="hideImg">
</div>
<br>
<div  id='show'>
    <canvas id='myCanvas' ></canvas>
</div>
<div >
    <center>
    <button id='clean' type='button' class='btn btn-warning'>清除</button>
    <button id='next' type='button' class='btn btn-info'>下一张</button>
            最终分数：<input type='text' id='score'></input>
    <button id='sub' type='button' class='btn btn-info'>提交</button>   
    </center>
</div>
<script type="text/javascript">
var phone = "<%=session.getAttribute("phone")%>"; 
if (phone == "null"){
	alert("请先登录！");
	window.location.href = "index.jsp";
}
</script>

</body>
</html>

