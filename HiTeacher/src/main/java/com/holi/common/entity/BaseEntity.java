package com.holi.common.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public abstract class BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 唯一标识
	 */
	protected String id;
	protected Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 插入前生成全球唯一ID UUID
	 */
	public void preInsert() {
		setId(UUID.randomUUID().toString().replaceAll("-", ""));
	}
}
