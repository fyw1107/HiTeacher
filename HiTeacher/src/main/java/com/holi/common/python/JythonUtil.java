package com.holi.common.python;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.python.core.Py;
import org.python.core.PySystemState;
import org.python.util.PythonInterpreter;
import org.springframework.transaction.annotation.Transactional;

/**
 * Java调用Python接口
 * 
 * @author IVAN 1.不同的方法之间仅存在文件路径和参数的差别 2.python脚本存在服务器本地
 *         3.jython的jar包不要用maven依赖,直接将自己配置好的jython.jar配置到工程目录下
 */
@Transactional
public class JythonUtil {

	private static Logger LOG = Logger.getLogger(JythonUtil.class);

	/**
	 * 存储本科生学生信息
	 * 
	 * @param stuId
	 * @param stuPassword
	 * @param userId
	 * @param schoolId
	 * @return
	 * @throws Exception
	 */
	public static String saveStudentInfoByPython(String stuId, String stuPassword, String userId, String schoolId)
			throws Exception {

		String s = "python C:/Python27/py/studentInf/all.py" + " " + stuId + " " + stuPassword + " " + userId + " "
				+ schoolId;
		Process process = Runtime.getRuntime().exec(s);

		// 取得命令结果的输出流
		InputStream fis = process.getInputStream();
		// 取得获得错误流。
		InputStream errorInput = process.getErrorStream();

		String fisStr = getPyResult(fis);
		String error = getPyResult(errorInput);

		process.waitFor();

		return error;
	}

	/**
	 * 
	 * @param stuId
	 * @param stuPassword
	 * @param userId
	 * @param schoolId
	 * @return
	 * @throws Exception
	 */
	public static String saveGraduateStudentInfoByPython(String stuId, String stuPassword, String userId,
			String schoolId) throws Exception {

		String s = "python C:/Python27/py/graduateStudentInf/all.py" + " " + stuId + " " + stuPassword + " " + userId
				+ " " + schoolId;
		Process process = Runtime.getRuntime().exec(s);

		// 取得命令结果的输出流
		InputStream fis = process.getInputStream();
		// 取得获得错误流。
		InputStream errorInput = process.getErrorStream();

		String fisStr = getPyResult(fis);
		String error = getPyResult(errorInput);
		System.out.println(error);
		process.waitFor();

		return error;
	}

	private static String getPyResult(InputStream fis) throws IOException {
		// 用一个读输出流类去读
		InputStreamReader isr = new InputStreamReader(fis);
		// 用缓冲器读行
		BufferedReader br = new BufferedReader(isr);
		String line = "";
		String result = "";
		// 直到读完为止
		while ((line = br.readLine()) != null) {
			System.out.println(line);
			result += line;
		}
		return result;
	}

	/**
	 * 存储学生信息
	 *
	 */
	public static void saveStudentInfo(String userId, String stuId, String stuPassword, String schoolId)
			throws Exception {

		// 定义参数
		String[] argv = { stuId, stuPassword, userId, schoolId };
		// 设置参数
		PythonInterpreter.initialize(null, null, argv);

		// 确定将要使用的第三方库文件添加到系统环境变量
		PySystemState sys = Py.getSystemState();
		sys.path.add("C:\\jython-2.7.00\\Lib\\site-packages");
		sys.path.add("C:\\py\\studentInf");
		LOG.info(sys.path.toString());

		PythonInterpreter interpreter = new PythonInterpreter();
		InputStream file;

		file = new FileInputStream("C:\\py\\studentInf\\all.py");
		interpreter.execfile(file);

		file.close();

		interpreter.close();

		LOG.info("game over");
	}

	/**
	 * 存储成绩信息
	 * 
	 * @param sno
	 *            学号
	 * @param password
	 *            密码
	 * @param stu_id
	 *            对应学生id
	 */
	public static void saveScoreInfo(String sno, String password, String stu_id) {

		// 定义参数
		String[] args2 = { sno, password, stu_id };

		// 设置参数
		PythonInterpreter.initialize(null, null, args2);
		PythonInterpreter interpreter = new PythonInterpreter();

		// 确定将要使用的第三方库文件添加到系统环境变量
		PySystemState sys = Py.getSystemState();
		sys.path.add("C:\\Users\\IVAN\\Desktop\\jython-2.7.00\\jython-2.7.00.jar\\Lib\\site-packages");
		System.out.println(sys.path.toString());

		// 执行Python脚本
		interpreter.execfile("C:\\Users\\IVAN\\Desktop\\score.py");
	}

	/**
	 * 存储用于显示课表的信息
	 * 
	 * @param sno
	 * @param password
	 * @param stu_id
	 */
	public static void saveKebiaoInfo(String sno, String password, String stu_id) {

		// 定义参数
		String[] args2 = { sno, password, stu_id };

		// 设置参数
		PythonInterpreter.initialize(null, null, args2);
		PythonInterpreter interpreter = new PythonInterpreter();

		// 确定将要使用的第三方库文件添加到系统环境变量
		PySystemState sys = Py.getSystemState();
		sys.path.add("C:\\Users\\IVAN\\Desktop\\jython-2.7.00\\jython-2.7.00.jar\\Lib\\site-packages");
		System.out.println(sys.path.toString());

		// 执行Python脚本
		interpreter.execfile("C:\\Users\\IVAN\\Desktop\\kebiao.py");
	}

	/**
	 * 用于存储课程信息(上课地点、周次、节次等经过处理)
	 * 
	 * @param sno
	 * @param password
	 * @param stu_id
	 */
	public static void saveCourseInfo(String sno, String password, String stu_id) {

		// 定义参数
		String[] args2 = { sno, password, stu_id };

		// 设置参数
		PythonInterpreter.initialize(null, null, args2);
		PythonInterpreter interpreter = new PythonInterpreter();

		// 确定将要使用的第三方库文件添加到系统环境变量
		PySystemState sys = Py.getSystemState();
		sys.path.add("C:\\Users\\IVAN\\Desktop\\jython-2.7.00\\jython-2.7.00.jar\\Lib\\site-packages");
		System.out.println(sys.path.toString());

		// 执行Python脚本
		interpreter.execfile("C:\\Users\\IVAN\\Desktop\\course.py");
	}

	/**
	 * 存储校园新闻
	 */
	public static void saveXiaoxi() {

		// 确定将要使用的第三方库文件添加到系统环境变量
		PySystemState sys = Py.getSystemState();

		// 本地测试路径
		// sys.path.add("C:\\Users\\IVAN\\Desktop\\jython-2.7.00\\jython-2.7.00.jar\\Lib\\site-packages");
		// sys.path.add("C:\\Users\\IVAN\\Desktop\\xiaoxi");

		/** 服务器部署路径 **/
		// 添加第三方库到系统环境变量
		sys.path.add("C:\\jython-2.7.00\\jython-2.7.00.jar\\Lib\\site-packages");
		// 添加py文件所在文件夹路径到系统环境变量
		sys.path.add("C:\\py\\xiaoxi");

		// System.out.println(sys.path.toString());

		PythonInterpreter interpreter = new PythonInterpreter();
		InputStream file;
		try {
			file = new FileInputStream("C:\\py\\xiaoxi\\all.py");
			interpreter.execfile(file);
			file.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// 为了实现服务器定时调度,此处不能关闭,否则会报
			// "Failed to submit a listener notification task. Event loop shut
			// down?"
			// 的错误
			// interpreter.close();
		}
	}

	/**
	 * 存储校园新闻
	 */
	public static void saveXiaoxiByPython() {

		try {
			System.out.println("hi");
			String s = "python C:/Python27/py/xiaoxi/all.py";
			Process process = Runtime.getRuntime().exec(s);
			process.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("中断异常！");
			e.printStackTrace();
		}
	}
}
