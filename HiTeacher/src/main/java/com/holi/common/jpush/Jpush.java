package com.holi.common.jpush;

import cn.jiguang.common.ClientConfig;
import cn.jpush.api.JPushClient;

/**
 * 极光推送管理类
 * 
 * @author lz
 *
 */
public class Jpush {

	public static JPushClient getJPushClient() {
		ClientConfig clientConfig = ClientConfig.getInstance();
		clientConfig.setTimeToLive(60);
		JPushClient jPushClient = new JPushClient("ab3c06c1404e182cf68135fc", "288ad690020a0de09a268601", null,
				clientConfig);
		return jPushClient;
	}
}
