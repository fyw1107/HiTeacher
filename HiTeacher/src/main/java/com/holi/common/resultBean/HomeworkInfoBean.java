package com.holi.common.resultBean;

import java.util.Date;

public class HomeworkInfoBean {

	private Integer id;
	private Date assignTime;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getAssignTime() {
		return assignTime;
	}
	public void setAssignTime(Date assignTime) {
		this.assignTime = assignTime;
	}
}
