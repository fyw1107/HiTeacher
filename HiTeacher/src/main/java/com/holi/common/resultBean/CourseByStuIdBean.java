package com.holi.common.resultBean;

/**
 * 通过stuId请求课程返回Bean
 * 
 * @author lz
 *
 */
public class CourseByStuIdBean {

	private String courseId;

	private String teacherId;

	private String courseName;

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

}
