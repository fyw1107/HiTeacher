package com.holi.common.resultBean;

/**
 * 封装课程的简要信息(与AllCourse类功能重复！作为工具bean应该删除entity中的AllCourse)
 * @author IVAN
 *
 */
public class CourseInfoBean {

	private String courseId;
	private String courseName;
	private String classnumber;
	
	public String getClassnumber() {
		return classnumber;
	}
	public void setClassnumber(String classnumber) {
		this.classnumber = classnumber;
	}
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
}
