package com.holi.common.resultBean;

/**
 * 上课提醒滚动条反馈信息    
 * @author IVAN
 *
 */
public class CourseBean {

	private String duration;		//上课时间区间
	private String courseName;		//课程名称
	private String coursePlace;		//上课地点
	
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getCoursePlace() {
		return coursePlace;
	}
	public void setCoursePlace(String coursePlace) {
		this.coursePlace = coursePlace;
	}
}
