package com.holi.common.resultBean;

import com.holi.module.entity.Homework;

/**
 * 封装查询的带有学生信息的作业记录
 * @author IVAN
 *
 */
public class AllHomeworkInfoBean extends Homework{

	private static final long serialVersionUID = -4945406933949540371L;
	private String stuName;		//姓名
	private Integer schoolId;	//学院
	
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public String getStuName() {
		return stuName;
	}
	public void setStuName(String stuName) {
		this.stuName = stuName;
	}
}
