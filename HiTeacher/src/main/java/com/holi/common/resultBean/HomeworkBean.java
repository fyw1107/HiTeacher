package com.holi.common.resultBean;

import com.holi.module.entity.HomeworkRecord;

/**
 * 用于返回主页显示作业列表和详细信息的Bean
 * @author IVAN
 *
 */
public class HomeworkBean {
	
	private HomeworkRecord hoRecord;	//教师布置的一次作业记录
	private double uncommitedRate;		//该次作业的未提交率
	private double uncheckedRate;		//该次作业的未批改率
	
	public HomeworkRecord getHoRecord() {
		return hoRecord;
	}
	public void setHoRecord(HomeworkRecord hoRecord) {
		this.hoRecord = hoRecord;
	}
	public double getUncommitedRate() {
		return uncommitedRate;
	}
	public void setUncommitedRate(double uncommitedRate) {
		this.uncommitedRate = uncommitedRate;
	}
	public double getUncheckedRate() {
		return uncheckedRate;
	}
	public void setUncheckedRate(double uncheckedRate) {
		this.uncheckedRate = uncheckedRate;
	}
}
