package com.holi.common.resultBean;

public class oneCradBean {
	private Long stuId;
	private String phone;
	private String name;
	private String xuyuanName;

	public oneCradBean(Long stuId, String phone, String name, String xuyuanName) {
		super();
		this.stuId = stuId;
		this.phone = phone;
		this.name = name;
		this.xuyuanName = xuyuanName;
	}

	public Long getStuId() {
		return stuId;
	}

	public void setStuId(Long stuId) {
		this.stuId = stuId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getXuyuanName() {
		return xuyuanName;
	}

	public void setXuyuanName(String xuyuanName) {
		this.xuyuanName = xuyuanName;
	}

}
