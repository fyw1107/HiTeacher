package com.holi.common.resultBean;

import com.holi.module.entity.XiaoXiNews;

/**
 * 查询我的收藏时用于封装新闻和收藏记录id
 * @author IVAN
 *
 */
public class CollectionBean extends XiaoXiNews{

	private static final long serialVersionUID = -4874124458618671817L;
	private int collectionId;		//收藏记录id

	public int getCollectionId() {
		return collectionId;
	}

	public void setCollectionId(int collectionId) {
		this.collectionId = collectionId;
	}
}
