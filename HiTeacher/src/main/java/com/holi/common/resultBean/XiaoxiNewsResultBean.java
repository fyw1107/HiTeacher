package com.holi.common.resultBean;

import java.util.Date;

public class XiaoxiNewsResultBean {

	private int id;
	private int xueyuan;
	private String name;
	private String content;
	private String link;
	private Date date;
	private String picsrc;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPicsrc() {
		return picsrc;
	}

	public void setPicsrc(String picsrc) {
		this.picsrc = picsrc;
	}

	public int getXueyuan() {
		return xueyuan;
	}

	public void setXueyuan(int xueyuan) {
		this.xueyuan = xueyuan;
	}

}
