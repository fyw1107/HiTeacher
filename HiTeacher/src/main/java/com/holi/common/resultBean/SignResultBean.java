package com.holi.common.resultBean;

public class SignResultBean {
	private String courseName;
	private Long sutId;
	private String stuName;

	public Long getSutId() {
		return sutId;
	}

	public void setSutId(Long sutId) {
		this.sutId = sutId;
	}

	public String getStuName() {
		return stuName;
	}

	public void setStuName(String stuName) {
		this.stuName = stuName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

}
