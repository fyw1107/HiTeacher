package com.holi.common.resultBean;

/**
 * 我的课表(显示整张课表)
 * 
 * @author IVAN `id` int(11) NOT NULL auto_increment, `stu_id` int(64) unsigned
 *         NOT NULL COMMENT '学生id外键', `weekday` tinyint(4) NOT NULL COMMENT
 *         '星期号（0周日，1周一，2周二...）', `one` varchar(100) default NULL COMMENT
 *         '1,2节次', `three` varchar(100) default NULL COMMENT '3,4节次', `five`
 *         varchar(100) default NULL COMMENT '5,6节次', `seven` varchar(100)
 *         default NULL COMMENT '7,8节次', `nine` varchar(100) default NULL
 *         COMMENT '9,10节次',
 */
public class CourseScheduleBean {

	private int id; // 课表记录id
	private int weekday; // 星期几

	private String oneCourseName; // 第1,2节 课程名字
	private String oneCourseId; // 第1,2节 课程Id

	private String threeCourseName; // 第3,4节 课程名字
	private String threeCourseId; // 第3,4节 课程Id

	private String fiveCourseName; // 第5,6节 课程名字
	private String fiveCourseId; // 第5,6节 课程Id

	private String sevenCourseName; // 第7,8节 课程名字
	private String sevenCourseId; // 第7,8节 课程Id

	private String nineCourseName; // 第9,10节 课程名字
	private String nineCourseId; // 第9,10节 课程Id

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWeekday() {
		return weekday;
	}

	public void setWeekday(int weekday) {
		this.weekday = weekday;
	}

	public String getOneCourseName() {
		return oneCourseName;
	}

	public void setOneCourseName(String oneCourseName) {
		this.oneCourseName = oneCourseName;
	}

	public String getOneCourseId() {
		return oneCourseId;
	}

	public void setOneCourseId(String oneCourseId) {
		this.oneCourseId = oneCourseId;
	}

	public String getThreeCourseName() {
		return threeCourseName;
	}

	public void setThreeCourseName(String threeCourseName) {
		this.threeCourseName = threeCourseName;
	}

	public String getThreeCourseId() {
		return threeCourseId;
	}

	public void setThreeCourseId(String threeCourseId) {
		this.threeCourseId = threeCourseId;
	}

	public String getFiveCourseName() {
		return fiveCourseName;
	}

	public void setFiveCourseName(String fiveCourseName) {
		this.fiveCourseName = fiveCourseName;
	}

	public String getFiveCourseId() {
		return fiveCourseId;
	}

	public void setFiveCourseId(String fiveCourseId) {
		this.fiveCourseId = fiveCourseId;
	}

	public String getSevenCourseName() {
		return sevenCourseName;
	}

	public void setSevenCourseName(String sevenCourseName) {
		this.sevenCourseName = sevenCourseName;
	}

	public String getSevenCourseId() {
		return sevenCourseId;
	}

	public void setSevenCourseId(String sevenCourseId) {
		this.sevenCourseId = sevenCourseId;
	}

	public String getNineCourseName() {
		return nineCourseName;
	}

	public void setNineCourseName(String nineCourseName) {
		this.nineCourseName = nineCourseName;
	}

	public String getNineCourseId() {
		return nineCourseId;
	}

	public void setNineCourseId(String nineCourseId) {
		this.nineCourseId = nineCourseId;
	}

}
