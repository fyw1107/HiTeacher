package com.holi.common.jsonBean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dell on 2017/3/24.
 */

public class StudentSignInMsg implements Serializable {

	private static final long serialVersionUID = 1L;

	private String teacherId;
	private Long stuId;
	private ArrayList<WifiItem> wifiList;

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public ArrayList<WifiItem> getWifiList() {
		return wifiList;
	}

	public void setWifiList(ArrayList<WifiItem> wifiList) {
		this.wifiList = wifiList;
	}

	public Long getStuId() {
		return stuId;
	}

	public void setStuId(Long stuId) {
		this.stuId = stuId;
	}

}
