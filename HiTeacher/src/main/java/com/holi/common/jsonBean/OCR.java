package com.holi.common.jsonBean;

import java.util.List;

public class OCR {
	private Long log_id;
	private int direction;
	private int words_result_num;
	private List<Words> words_result;

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public int getWords_result_num() {
		return words_result_num;
	}

	public void setWords_result_num(int words_result_num) {
		this.words_result_num = words_result_num;
	}

	public List<Words> getWords_result() {
		return words_result;
	}

	public void setWords_result(List<Words> words_result) {
		this.words_result = words_result;
	}

	public Long getLog_id() {
		return log_id;
	}

	public void setLog_id(Long log_id) {
		this.log_id = log_id;
	}

}
