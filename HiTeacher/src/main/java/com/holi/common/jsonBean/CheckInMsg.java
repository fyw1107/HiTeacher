package com.holi.common.jsonBean;

import java.util.Set;

/**
 * Created by PangXiaojian on 2017/5/10.
 */
public class CheckInMsg {
	private String courseId;
	private String stuId;
	private String singFlag;
	private String teacherId;
	private String classNumber;
	private Set<String> bluetoothNameList;

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getStuId() {
		return stuId;
	}

	public void setStuId(String stuId) {
		this.stuId = stuId;
	}

	public String getSingFlag() {
		return singFlag;
	}

	public void setSingFlag(String singFlag) {
		this.singFlag = singFlag;
	}

	public Set<String> getBluetoothNameList() {
		return bluetoothNameList;
	}

	public void setBluetoothNameList(Set<String> bluetoothNameList) {
		this.bluetoothNameList = bluetoothNameList;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getClassNumber() {
		return classNumber;
	}

	public void setClassNumber(String classNumber) {
		this.classNumber = classNumber;
	}

}