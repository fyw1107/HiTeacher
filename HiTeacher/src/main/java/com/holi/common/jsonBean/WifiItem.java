package com.holi.common.jsonBean;

import java.io.Serializable;

/**
 * Created by fyk on 17-3-18.
 */

public class WifiItem implements Serializable {

	private static final long serialVersionUID = 1L;

	private String SSID;
	private String BSSID;// MAC addr
	private String capabilities;
	private int frequency;
	private int level;// 信号强度

	public String getSSID() {
		return SSID;
	}

	public void setSSID(String SSID) {
		this.SSID = SSID;
	}

	public String getBSSID() {
		return BSSID;
	}

	public void setBSSID(String BSSID) {
		this.BSSID = BSSID;
	}

	public String getCapabilities() {
		return capabilities;
	}

	public void setCapabilities(String capabilities) {
		this.capabilities = capabilities;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
}
