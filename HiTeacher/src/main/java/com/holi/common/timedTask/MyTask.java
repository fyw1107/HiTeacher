package com.holi.common.timedTask;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.holi.common.jpush.Jpush;
import com.holi.module.dao.StudentDao;
import com.holi.module.entity.Student;

import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;

@Component
public class MyTask {

	@Autowired
	private StudentDao studentDao;

	@Scheduled(cron = "1/5 * * * * ? ") // 间隔5秒执行
	public void taskCycle() {
		System.out.println("你好啊啊啊");
	}

	@Scheduled(cron = "0 00 03 ? * *") // 每天03:00触发
	public void python() {

		// 将数据库的所有学生 账号密码取出
		List<Student> studentList = studentDao.findAllList();

		for (Student student : studentList) {
			// 調用python执行 监控成绩和停换课通知
			// TODO
			// 取出最近更新的数据

			// Jpush推送给到相应的客户端
			try {
				Jpush.getJPushClient().sendIosMessageWithRegistrationID("", "", student.getUserId());
			} catch (APIConnectionException e) {
				e.printStackTrace();
			} catch (APIRequestException e) {
				e.printStackTrace();
			}
		}

	}
}
