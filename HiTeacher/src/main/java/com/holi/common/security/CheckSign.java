package com.holi.common.security;

import java.util.Arrays;

/**
 * 验证签名
 * @author lz
 *
 */
public class CheckSign {
	
	/**
	 * 判断签名的是否符合规则
	 * @param userId
	 * @param timestamp
	 * @param sign
	 * @return
	 */
	/*public static boolean isSuccess(String userId,String timestamp,String sign) {
		
		String userIdSort = sort(userId);
		String md5 = Md5.getMD5(userIdSort + timestamp);
		
		if(md5.equals(sign)) 
			return true;
		return false;
	}*/
	/**
	 * 对字符串进行排序 
	 * @param userId
	 * @return
	 */
	private static String sort(String userId) {
		char[] chars = userId.toCharArray();
		  Arrays.sort(chars);
		  return new String(chars);
	}
}
