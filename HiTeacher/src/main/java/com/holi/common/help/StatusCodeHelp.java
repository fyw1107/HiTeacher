package com.holi.common.help;

/**
 * 存放返回状态码
 * 
 * @author lz
 *
 */
public class StatusCodeHelp {
	/**
	 * 操作成功
	 */
	public static final int success = 200;

	public static final String successMessage = "成功";
	/**
	 * 签名错误
	 */
	public static final int signError = 101;

	public static final String signErrorMessage = "签名错误";
	/**
	 * 参数错误
	 */
	public static final int parameterError = 102;

	public static final String parameterErrorMessage = "参数错误";
	/**
	 * 访问资源不存在
	 */
	public static final int notFoundResource = 103;

	public static final String notFoundResourceMessage = "访问资源不存在";

	/**
	 * 用户不存在
	 */
	public static final int notFoundUser = 104;

	public static final String notFoundUserMeaage = "用户不存在";
	/**
	 * 密码错误
	 */
	public static final int passwordError = 105;

	public static final String passwordErrorMessage = "密码错误";
	/**
	 * 该用户已经存在
	 */
	public static final int userIsExist = 106;

	public static final String userIsExistMessage = "用户已经存在";
	/**
	 * 服务器内部错误(数据库写入失败等)
	 */
	public static final int internalErr = 500;

	public static final String internalErrMessage = "服务器内部错误";
	/**
	 * 访问超时
	 */
	public static final int timeOut = 501;

	public static final String timeOutMessage = "访问超时";
	/**
	 * 谢谢反馈
	 */
	public static final String thkFeedback = "多谢你的反馈";

	/**
	 * session_user
	 */
	public static final String SESSION_USER = "session_user";

	/**
	 * 用户不是老师
	 */
	public static final int notTeacher = 107;

	public static final String notTeacherMessage = "请使用教师账号登陆";

	/**
	 * 校园网链接超时
	 */
	public static final int schoolNetTimeOut = 108;

	public static final String schoolNetTimeOutMessage = "绑定失败,请重新绑定";

	/**
	 * 本次签到失败，请重新签到。
	 */
	public static final int signFail = 109;

	public static final String signFailMessage = "本次签到失败，请重新签到。";

	/**
	 * 邀请码错误
	 */
	public static final int teacherIdError = 110;

	public static final String teacherIdErrorMessage = "邀请码错误";

	/**
	 * 该邀请码已经绑定用户。
	 */
	public static final int teacherIdHaveBindErro = 111;

	public static final String teacherIdHaveBindErroMessage = "该邀请码已经绑定用户";

	/**
	 * 该学生账号已经被绑定
	 */
	public static final int stuIdHaveBindErro = 112;

	public static final String stuIdHaveBindErroMessage = "该学生账号已经被绑定";

	/**
	 * 一卡通识别失败，请重新拍照
	 */
	public static final int OCRoneCradError = 113;

	public static final String OCRoneCradErrorMessage = "一卡通识别失败，请换个姿势再来一次";

	/**
	 * 此一卡通的主人，没有绑定我们的app,无法获取信息喔~
	 */
	public static final int oneCradNotBand = 114;

	public static final String oneCradNotBandMessage = "此一卡通的主人，没有绑定我们的app,无法获取信息喔~";
}
