package com.holi.common.yunxin.request;

import java.io.IOException;

import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OcrRequest {

	private OkHttpClient client;
	private static OcrRequest baiduOCR;

	private static final String grant_type = "client_credentials";
	private static final String client_id = "gttA8PG9NBFoN53MAns6qPjz";
	private static final String client_secret = "iLcheOPjwvGROVr8KzZjTrTQ9UCQ4Qa1";
	private static String access_token = "24.1372a88d54f4f37b6f3b9b0fabaceb0f.2592000.1499137039.282335-9723725";

	private static final String generalOcrURL = "https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic?access_token="
			+ access_token;
	private static final String getAccessTokenURL = "https://aip.baidubce.com/oauth/2.0/token?grant_type=" + grant_type
			+ "&client_id=" + client_id + "&client_secret=" + client_secret;

	private OcrRequest() {
		client = new OkHttpClient();
	}

	public static OcrRequest getInstance() {

		if (baiduOCR == null) {
			synchronized (ClassGroupRequest.class) {
				if (baiduOCR == null) {
					baiduOCR = new OcrRequest();
				}
			}
		}
		return baiduOCR;
	}

	/**
	 * 获取accessToken
	 * 
	 * @return
	 * @throws IOException
	 */
	public String getAccessToken() throws IOException {

		Request request = new Request.Builder().url(getAccessTokenURL).build();

		Response response = client.newCall(request).execute();

		String result = response.body().string();
		JSONObject jsonObject = new JSONObject(result);
		String accessToken = jsonObject.getString("access_token");

		return accessToken;
	}

	/**
	 * 通用文字识别
	 * 
	 * @param image
	 * @return
	 * @throws IOException
	 */
	public Response generalOCR(String image) throws IOException {

		RequestBody formBody = new FormBody.Builder().add("image", image).add("detect_direction", "true").build();

		Request request = new Request.Builder().url(generalOcrURL)
				.addHeader("Content-Type", "application/x-www-form-urlencoded").post(formBody).build();

		Response response = client.newCall(request).execute();

		return response;
	}

}
