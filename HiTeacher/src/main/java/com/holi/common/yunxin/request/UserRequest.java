package com.holi.common.yunxin.request;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.holi.common.help.YunXinHelp;
import com.holi.common.util.DateUtil;
import com.holi.common.util.StringUtil;
import com.holi.common.yunxin.security.CheckSumBuilder;
import com.holi.common.yunxin.security.RandomNum;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UserRequest {

	Logger LOG = Logger.getLogger(this.getClass());

	private static UserRequest user = null;
	private OkHttpClient client;

	private static final String userRegisterURL = "https://api.netease.im/nimserver/user/create.action";
	private static final String userFoundPasswordURL = "https://api.netease.im/nimserver/user/update.action";

	private UserRequest() {
		client = new OkHttpClient();
	}

	public static UserRequest getInstance() {
		// 先判断是否被实例化过，避免了每次加锁同步的性能消耗
		if (user == null) {
			synchronized (UserRequest.class) {
				if (user == null) {
					user = new UserRequest();
				}
			}
		}
		return user;
	}

	public Response userRegister(String accid, String name, String token) throws IOException {

		String curTime = String.valueOf(DateUtil.getSystemTime().getTime() / 1000);
		String Nonce = RandomNum.getRandomNum();
		String CheckSum = CheckSumBuilder.getCheckSum(YunXinHelp.appSecret, Nonce, curTime);
		
		System.out.println(curTime);

		RequestBody formBody = new FormBody.Builder().add("accid", accid).add("name", name).add("token", token).build();

		Request request = new Request.Builder().url(userRegisterURL).addHeader("AppKey", YunXinHelp.appKey)
				.addHeader("Nonce", Nonce).addHeader("CurTime", curTime).addHeader("CheckSum", CheckSum)
				.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8").post(formBody).build();

		Response response = client.newCall(request).execute();

		return response;
	}

	/**
	 * 密码找回
	 * 
	 * @param accid
	 * @param token
	 * @return
	 * @throws IOException
	 */
	public Response updatePassword(String accid, String token) throws IOException {

		String curTime = String.valueOf(DateUtil.getSystemTime().getTime()/1000);
		String Nonce = RandomNum.getRandomNum();
		String CheckSum = CheckSumBuilder.getCheckSum(YunXinHelp.appSecret, Nonce, curTime);

		RequestBody formBody = new FormBody.Builder().add("accid", accid).add("token", token).build();

		Request request = new Request.Builder().url(userFoundPasswordURL).addHeader("AppKey", YunXinHelp.appKey)
				.addHeader("Nonce", Nonce).addHeader("CurTime", curTime).addHeader("CheckSum", CheckSum)
				.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8").post(formBody).build();

		Response response = client.newCall(request).execute();

		return response;
	}
}
