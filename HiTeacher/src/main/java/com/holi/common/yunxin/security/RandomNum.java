package com.holi.common.yunxin.security;

import java.util.Random;

public class RandomNum {

	/**
	 * 生成六位十以内的随机数
	 * @return
	 */
	public static String getRandomNum() {

		String randomNum="";

		Random random = new Random();

		for (int i = 0; i < 6; i++) 
			randomNum += random.nextInt(10);

		return randomNum;
	}
	
	/**
	 * 输出m个n(不包含n)以内的不同的随机数
	 * @param m
	 * @param n
	 * @return
	 */
	public static String getRandomNum(int m,int n) {
		
		String randomNum="";
		
		String temp = "";

		Random random = new Random();

		for (int i = 0; i < m; i++) {
			
			temp =String.valueOf(random.nextInt(n));
		
			while(randomNum.contains(temp)) {
				temp =String.valueOf(random.nextInt(n));
			}

			
			randomNum += temp;
		}

		return randomNum;
	}
	
	public static void main(String[] args) {
		String radom = RandomNum.getRandomNum(5,10);
		System.out.println(radom);
		String[] str = radom.split("");
		for (String string : str) {
			System.out.println(string);
		}
	}
}
