package com.holi.common.yunxin.request;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.holi.common.help.YunXinHelp;
import com.holi.common.util.DateUtil;
import com.holi.common.yunxin.security.CheckSumBuilder;
import com.holi.common.yunxin.security.RandomNum;
import com.holi.module.entity.ClassGroup;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ClassGroupRequest {

	Logger LOG = Logger.getLogger(this.getClass());

	private OkHttpClient client;
	private static ClassGroupRequest classGroupRequest;

	private static final String createClassGroupURL = "https://api.netease.im/nimserver/team/create.action";
	private static final String addClassGroupURL = "https://api.netease.im/nimserver/team/add.action";
	private static final String changeOwnerURL = "https://api.netease.im/nimserver/team/changeOwner.action";
	private static final String queryMsgURL = "https://api.netease.im/nimserver/team/query.action";

	private ClassGroupRequest() {
		client = new OkHttpClient();
	}

	public static ClassGroupRequest getInstance() {

		if (classGroupRequest == null) {
			synchronized (ClassGroupRequest.class) {
				if (classGroupRequest == null) {
					classGroupRequest = new ClassGroupRequest();
				}
			}
		}
		return classGroupRequest;
	}

	/**
	 * 创建班级群
	 * 
	 * @param classGroup
	 * @return
	 * @throws IOException
	 */
	public Response createClassGroup(ClassGroup classGroup) throws IOException {

		String curTime = String.valueOf(DateUtil.getSystemTime().getTime()/1000);
		String Nonce = RandomNum.getRandomNum();
		String CheckSum = CheckSumBuilder.getCheckSum(YunXinHelp.appSecret, Nonce, curTime);

		RequestBody formBody = new FormBody.Builder().add("tname", classGroup.getTname())
				.add("owner", classGroup.getOwner()).add("members", classGroup.getMembers())
				/* .add("intro", classGroup.getIntro()) */.add("msg", classGroup.getMsg())
				.add("magree", String.valueOf(classGroup.getMagree()))
				.add("joinmode",
						String.valueOf(classGroup
								.getJoinmode()))/*
												 * .add("custom",
												 * classGroup.getCustom())
												 */
				.add("beinvitemode", String.valueOf(classGroup.getBeinvitemode()))
				/*
				 * .add("invitemode",
				 * String.valueOf(classGroup.getInvitemode()))
				 * .add("uptinfomode",
				 * String.valueOf(classGroup.getUptinfomode()))
				 * .add("upcustommode",
				 * String.valueOf(classGroup.getUptinfomode()))
				 */.build();

		Request request = new Request.Builder().url(createClassGroupURL).addHeader("AppKey", YunXinHelp.appKey)
				.addHeader("Nonce", Nonce).addHeader("CurTime", curTime).addHeader("CheckSum", CheckSum)
				.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8").post(formBody).build();

		Response response = client.newCall(request).execute();

		return response;
	}

	/**
	 * 加入班级群
	 * 
	 * @param classGroup
	 * @return
	 * @throws IOException
	 */
	public Response addClassGroup(ClassGroup classGroup) throws IOException {

		String curTime = String.valueOf(DateUtil.getSystemTime().getTime()/1000);
		String Nonce = RandomNum.getRandomNum();
		String CheckSum = CheckSumBuilder.getCheckSum(YunXinHelp.appSecret, Nonce, curTime);

		RequestBody formBody = new FormBody.Builder().add("tid", classGroup.getTid())
				.add("owner", classGroup.getOwner()).add("members", classGroup.getMembers())
				.add("msg", classGroup.getMsg()).add("magree", String.valueOf(classGroup.getMagree())).build();

		Request request = new Request.Builder().url(addClassGroupURL).addHeader("AppKey", YunXinHelp.appKey)
				.addHeader("Nonce", Nonce).addHeader("CurTime", curTime).addHeader("CheckSum", CheckSum)
				.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8").post(formBody).build();

		Response response = client.newCall(request).execute();

		return response;
	}

	/**
	 * 移交群主
	 * 
	 * @param classGroup
	 * @return
	 * @throws IOException
	 */
	public Response changeOwner(ClassGroup classGroup, String newowner, String leave) throws IOException {

		String curTime = String.valueOf(DateUtil.getSystemTime().getTime()/1000);
		String Nonce = RandomNum.getRandomNum();
		String CheckSum = CheckSumBuilder.getCheckSum(YunXinHelp.appSecret, Nonce, curTime);

		RequestBody formBody = new FormBody.Builder().add("tid", classGroup.getTid())
				.add("owner", classGroup.getOwner()).add("newowner", newowner).add("leave", leave).build();

		Request request = new Request.Builder().url(changeOwnerURL).addHeader("AppKey", YunXinHelp.appKey)
				.addHeader("Nonce", Nonce).addHeader("CurTime", curTime).addHeader("CheckSum", CheckSum)
				.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8").post(formBody).build();

		Response response = client.newCall(request).execute();

		return response;
	}

	/**
	 * 群信息与成员列表查询
	 * 
	 * @param classGroup
	 * @param tids
	 * @param ope
	 * @return
	 * @throws IOException
	 */
	public Response queryMsg( String tids, String ope) throws IOException {

		String curTime = String.valueOf(DateUtil.getSystemTime().getTime()/1000);
		String Nonce = RandomNum.getRandomNum();
		String CheckSum = CheckSumBuilder.getCheckSum(YunXinHelp.appSecret, Nonce, curTime);

		RequestBody formBody = new FormBody.Builder().add("tids", tids).add("ope", ope).build();

		Request request = new Request.Builder().url(queryMsgURL).addHeader("AppKey", YunXinHelp.appKey)
				.addHeader("Nonce", Nonce).addHeader("CurTime", curTime).addHeader("CheckSum", CheckSum)
				.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8").post(formBody).build();

		Response response = client.newCall(request).execute();

		return response;
	}

}
