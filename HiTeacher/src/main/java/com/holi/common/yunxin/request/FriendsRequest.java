package com.holi.common.yunxin.request;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.holi.common.help.YunXinHelp;
import com.holi.common.util.DateUtil;
import com.holi.common.yunxin.security.CheckSumBuilder;
import com.holi.common.yunxin.security.RandomNum;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FriendsRequest {

	Logger LOG = Logger.getLogger(this.getClass());

	private OkHttpClient client;
	private static FriendsRequest friends;

	private static final String addFriendURL = "https://api.netease.im/nimserver/friend/add.action";
	private static final String updateFriendURL = "https://api.netease.im/nimserver/friend/update.action";

	private FriendsRequest() {
		client = new OkHttpClient();
	}

	public static FriendsRequest getInstance() {

		if (friends == null) {
			synchronized (FriendsRequest.class) {
				if (friends == null) {
					friends = new FriendsRequest();
				}
			}
		}
		return friends;
	}

	public Response addFriend(String accid, String faccid, String type, String msg) throws IOException {

		String curTime = String.valueOf(DateUtil.getSystemTime().getTime()/1000);
		String Nonce = RandomNum.getRandomNum();
		String CheckSum = CheckSumBuilder.getCheckSum(YunXinHelp.appSecret, Nonce, curTime);

		RequestBody formBody = new FormBody.Builder().add("accid", accid).add("faccid", faccid).add("msg", msg)
				.add("type", type).build();

		Request request = new Request.Builder().url(addFriendURL).addHeader("AppKey", YunXinHelp.appKey)
				.addHeader("Nonce", Nonce).addHeader("CurTime", curTime).addHeader("CheckSum", CheckSum)
				.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8").post(formBody).build();

		Response response = client.newCall(request).execute();

		return response;
	}
}
