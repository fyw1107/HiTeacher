package com.holi.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 计算周次类  
 * @author IVAN
 *
 */
public class WeekCalcUtil{

	private static String firstDayString = "2017-02-27";	//第一周的第一天
	private SimpleDateFormat sdf; 
	private Date firstDayDate;
	private Calendar cal; 

	public WeekCalcUtil(){
		this.sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			this.firstDayDate = sdf.parse(firstDayString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.cal = Calendar.getInstance();
	}

	public String getFirstDay() {
		return firstDayString;
	}

	public static void main(String[] args) {

		WeekCalcUtil ww = new WeekCalcUtil();
		int days = ww.daysBetween(new Date());
		System.out.println(days);

		System.out.println(ww.getCurrentWeek(days));
		System.out.println(ww.cal.getWeekYear());
	}

	/**
	 * 计算当前距开学日期的天数  
	 * @param currentDate  当前日期
	 * @return 天数   
	 */
	public int daysBetween(Date currentDate){    
		try {
			currentDate = sdf.parse(sdf.format(currentDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}  
		cal.setTime(firstDayDate);    
		long time1 = cal.getTimeInMillis();                 
		cal.setTime(currentDate);    
		long time2 = cal.getTimeInMillis();         
		long between_days=(time2-time1)/(1000*3600*24);  

		return Integer.parseInt(String.valueOf(between_days))+1;           
	}  

	/**
	 * 根据天数计算当前的周次
	 * @param days
	 * @return
	 */
	public int getCurrentWeek(int days){

		int week = days/7;
		if(days % 7 != 0)
			return week + 1;
		return week;
	}

	/**
	 * 获取今天是星期几
	 * @param date
	 * @return 0周日 1周一 2周二......
	 */
	public int getDayofweek(Date date){  
		cal.setTime(date);
		int w = cal.get(Calendar.DAY_OF_WEEK)-1; 
		return w;
	}
}
