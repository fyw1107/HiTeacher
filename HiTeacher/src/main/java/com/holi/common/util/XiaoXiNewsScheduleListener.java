package com.holi.common.util;

import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Servlet监听器(和Timer类组合完成定时调度任务功能)
 * @author IVAN
 *
 */
public class XiaoXiNewsScheduleListener implements ServletContextListener {

	private Timer timer;  
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		timer = new Timer(true);
		sce.getServletContext().log("定时器已启动");
		timer.schedule(new ScheduleTask(sce.getServletContext()), 5000, 1000*600*60);
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		timer.cancel();  
		sce.getServletContext().log("定时器销毁");
	}
}
