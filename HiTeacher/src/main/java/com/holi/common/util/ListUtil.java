package com.holi.common.util;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.holi.module.entity.Student;

public class ListUtil {

	// 并集（set唯一性）
	public static List<? extends Object> union(List<? extends Object> list1, List<? extends Object> list2) {
		Set<Object> hs = new HashSet<Object>();

		for (Object str : list1) {
			hs.add(str);
		}
		for (Object str : list2) {
			hs.add(str);
		}

		List<Object> reslutList = new LinkedList<>();

		reslutList.addAll(hs);

		return reslutList;
	}

	// 交集
	public static List<? extends Object> intersect(List<? extends Object> list1, List<? extends Object> list2) {
		Set<Object> hs1 = new HashSet<Object>();
		Set<Object> hs2 = new HashSet<Object>();

		for (Object str : list1) {
			hs1.add(str);
		}
		for (Object str : list2) {
			if (hs1.contains(str)) {
				hs2.add(str);
			}
		}

		List<Object> reslutList = new LinkedList<>();

		reslutList.addAll(hs2);

		return reslutList;
	}

	// 求两个数组的差集
	public static String[] substract(String[] arg1, String[] arg2) {

		Set<String> hs = new HashSet<>();
		for (String arg : arg1) {
			hs.add(arg);
		}
		for (String arg : arg2) {
			if (hs.contains(arg)) {
				hs.remove(arg);
			}
		}
		String[] result = {};
		return hs.toArray(result);
	}

	// 求两个lsit的差集
	public static List<Student> substractList(List<Student> list1, List<Student> list2) {

		Set<Student> hs = new HashSet<Student>();

		for (Student student : list1) {
			hs.add(student);
		}

		for (Student student : list2) {
			if (hs.contains(student))
				hs.remove(student);
		}

		List<Student> reslutList = new LinkedList<>();

		reslutList.addAll(hs);

		return reslutList;
	}
}
