package com.holi.common.util;

import java.util.TimerTask;
import javax.servlet.ServletContext;

import com.holi.common.python.JythonUtil;

/**
 * 爬取新闻的任务线程
 * @author IVAN
 *
 */
public class ScheduleTask extends TimerTask{

	private ServletContext context;

	public ScheduleTask(ServletContext context){
		this.context = context;
	}

	@Override
	public void run() {
		context.log("hello");
		//爬取新闻,存入数据库
		//JythonUtil.saveXiaoxi();
		JythonUtil.saveXiaoxiByPython();
	}
}
