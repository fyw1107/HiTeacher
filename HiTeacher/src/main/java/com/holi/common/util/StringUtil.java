package com.holi.common.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**

 * 字符串工具类

 * @author lz
 */
public class StringUtil {

	/**

	 * 时间戳转化为Date类型

	 * 

	 * @param timestamp

	 * @return

	 */
	public static Date StringToDate(String timestamp) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		try {
			Date date = sdf.parse(timestamp);
			return date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
		return null;
	}

	/**

	 * 从头截取字符串从m到n个字符

	 * 

	 * @param str

	 * @return

	 */
	public static String subString(String str, int m, int n) {
		if (str != null)
			str = str.substring(m, n);
		return str;
	}
	
	public static String stringParamDecoder(String str){
		
		String strParam = null;
		try {
			strParam = URLDecoder.decode(str, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return strParam;
	}
}