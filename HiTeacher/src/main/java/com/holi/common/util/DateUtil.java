package com.holi.common.util;

import java.util.Date;

/**
 * 时间工具类
 * @author lz
 *
 */
public class DateUtil {

	/**
	 * 发送链接时间与系统的时间相隔的时间
	 * @param timestamp
	 * @return
	 */ 
	public static int DateApart(String timestamp) {
		long nowDate = new Date().getTime();
		long sendTime = StringUtil.StringToDate(timestamp).getTime();
		
		return (int) (sendTime - nowDate);
	}
	
	/**
	 * 获取系统时间
	 * @return
	 */
	public static Date getSystemTime() {
		
		return new Date();
		
	}
}
