package com.holi.module.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.holi.common.resultBean.CourseScheduleBean;
import com.holi.module.dao.CourseDao;
import com.holi.module.dao.CourseScheduleDao;

/**
 * 我的课表Service
 * 
 * @author IVAN
 *
 */
@Service
public class CourseScheduleService {

	@Autowired
	private CourseScheduleDao courseScheduleDao;

	@Autowired
	private CourseDao courseDao;

	public List<CourseScheduleBean> getMySchedule(Long stuId) {

		int i = 0;

		List<CourseScheduleBean> csList = courseScheduleDao.listCourseSchedule(stuId);

		/*
		 * for (CourseScheduleBean courseSchedule : csList) {
		 * 
		 * handelData(courseSchedule); }
		 */
		return csList;
	}

	@SuppressWarnings("unused")
	private void handelData(CourseScheduleBean courseSchedule) {
		if (!courseSchedule.getOneCourseName().isEmpty()) {
			// 匹配课程名拿到课程ID
			String courseId = courseDao.getCourseIdBycourseName(handleCourseName(courseSchedule.getOneCourseName()));
			courseSchedule.setOneCourseId(courseId);
		}

		if (!courseSchedule.getThreeCourseName().isEmpty()) {
			// 匹配课程名拿到课程ID
			String courseId = courseDao.getCourseIdBycourseName(handleCourseName(courseSchedule.getOneCourseName()));
			courseSchedule.setThreeCourseId(courseId);
		}

		if (!courseSchedule.getFiveCourseName().isEmpty()) {
			// 匹配课程名拿到课程ID
			String courseId = courseDao.getCourseIdBycourseName(handleCourseName(courseSchedule.getOneCourseName()));
			courseSchedule.setFiveCourseId(courseId);
		}

		if (!courseSchedule.getSevenCourseName().isEmpty()) {
			// 匹配课程名拿到课程ID
			String courseId = courseDao.getCourseIdBycourseName(handleCourseName(courseSchedule.getOneCourseName()));
			courseSchedule.setSevenCourseId(courseId);
		}

		if (!courseSchedule.getNineCourseName().isEmpty()) {
			// 匹配课程名拿到课程ID
			String courseId = courseDao.getCourseIdBycourseName(handleCourseName(courseSchedule.getOneCourseName()));
			courseSchedule.setNineCourseId(courseId);
		}
	}

	private String handleCourseName(String courseName) {
		String[] s = courseName.split("_");
		return s[0];
	}
}
