package com.holi.module.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.holi.common.help.StatusCodeHelp;
import com.holi.common.resultBean.CourseBean;
import com.holi.common.resultBean.CourseByStuIdBean;
import com.holi.common.resultBean.ResultBean;
import com.holi.common.util.WeekCalcUtil;
import com.holi.module.dao.CourseDao;
import com.holi.module.dao.TeacherDao;
import com.holi.module.entity.Course;

/**
 * 课程提醒业务逻辑
 * 
 * @author IVAN
 *
 */
@Service
public class CourseService {

	@Autowired
	private CourseDao courseDao;

	@Autowired
	private ResultBean resultBean;

	@Autowired
	private TeacherDao teacherDao;

	public List<CourseBean> listCourseByStuAndWeek(int stuId) {

		// 用于存储返回Course的list

		List<CourseBean> courseList = new ArrayList<>();
		String duration = null;

		/************** 计算时令和学期 **************/
		char season = 0; // 冬令时

		int term = 0; // 秋学期

		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH) + 1; // 当前月份

		if (month >= 5 && month <= 10)
			season = 1; // 夏令时

		if (month >= 3 && month <= 7)
			term = 1; // 春学期

		/************** 计算星期和周次 **************/
		WeekCalcUtil wCal = new WeekCalcUtil();
		Date date = new Date();
		int weekday = wCal.getDayofweek(date);

		// 获取今天是第几周
		int weekth = wCal.getCurrentWeek(wCal.daysBetween(date));

		/************** 封装查询需要的参数 **************/
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		map.put("stuId", stuId);
		map.put("weekday", weekday);
		map.put("term", term);

		/************** 查询并返回符合条件的课程 **************/
		List<Course> list = courseDao.listCourses(map);
		// 将符合上课周次的课程返回

		for (Course c : list) {
			String[] weeks = c.getCourseWeek().split(","); // 形如1,2,3,5,7,9的字符串,切割成字符串数组

			for (String w : weeks) {
				if (Integer.parseInt(w) == weekth) {
					if (season == 1) {
						switch (Integer.parseInt(c.getCourseTime())) {
						case 1:
							duration = "8:00 ~ 9:45";
							break;
						case 3:
							duration = "10:15 ~ 12:00";
							break;
						case 5:
							duration = "14:30 ~ 16:15";
							break;
						case 7:
							duration = "16:45 ~ 18:30";
							break;
						case 9:
							duration = "19:30 ~ 21:45";
							break;
						}
					} else {
						switch (Integer.parseInt(c.getCourseTime())) {
						case 1:
							duration = "8:00 ~ 9:45";
							break;
						case 3:
							duration = "10:15 ~ 12:00";
							break;
						case 5:
							duration = "14:00 ~ 15:45";
							break;
						case 7:
							duration = "16:15 ~ 18:00";
							break;
						case 9:
							duration = "19:00 ~ 21:15";
							break;
						}
					}
					// 构建需要返回前台的滚动信息Bean

					CourseBean cb = new CourseBean();
					cb.setCourseName(c.getCourseName());
					cb.setCoursePlace(c.getClassroom());
					cb.setDuration(duration);
					courseList.add(cb);
				}
			}
		}
		return courseList;
	}

	/**
	 * 根据stuId获取本学生所上的课程
	 * 
	 * @param stuId
	 * @return
	 */
	public ResultBean stuCourse(Long stuId) {

		List<Course> coursesList = courseDao.findCourseByStuId(stuId);

		for (Course course : coursesList) {
			String teacherId = teacherDao.getTeacherByCourseId(course.getCourseId());
			course.setTeacherId(teacherId);
		}

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, coursesList);
		return resultBean;
	}

	/**
	 * 根据teacherId获取改老师所上的所有课程
	 * 
	 * @param teacherId
	 * @return
	 */
	public ResultBean teacherCourse(String teacherId) {

		List<Course> coursesList = courseDao.findCourseByTeacherId(teacherId);
		
		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, coursesList);
		return resultBean;
	}

}
