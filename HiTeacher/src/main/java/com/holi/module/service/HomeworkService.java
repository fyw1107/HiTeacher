package com.holi.module.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.holi.common.resultBean.AllHomeworkInfoBean;
import com.holi.common.resultBean.CourseInfoBean;
import com.holi.common.resultBean.HomeworkInfoBean;
import com.holi.module.dao.CourseDao;
import com.holi.module.dao.HomeworkDao;
import com.holi.module.entity.Homework;
import com.holi.module.entity.HomeworkRecord;

import sun.misc.BASE64Decoder;

/**
 * 作业业务层
 * 
 * @author IVAN
 *
 */
@Service
public class HomeworkService {

	@Autowired
	private HomeworkDao homeworkDao;

	@Autowired
	private CourseDao courseDao;

	private Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 布置作业(教师端) 同时更新具体作业表和统计作业表
	 * 
	 * @param courseId
	 * @param courseName
	 * @param classnumber
	 * @param teacherId
	 * @param hwContent
	 * @return
	 */
	public int assignHomework(String courseId, String courseName, String classnumber, String teacherId,
			String hwContent) {

		logger.info("###########courseId###########:" + courseId);
		logger.info("###########courseName###########:" + courseName);
		logger.info("###########classnumber###########:" + classnumber);
		logger.info("###########teacherId###########:" + teacherId);
		logger.info("###########hwContent###########:" + hwContent);

		int count = 0;
		int recordId = 0;
		Date date = new Date();
		Map<String, Object> map = new LinkedHashMap<>();

		/* 1.在作业统计记录表中插入一条记录 */
		HomeworkRecord hoRecord = new HomeworkRecord(courseId, courseName, classnumber, teacherId, date);
		homeworkDao.insertHomeworkRecord(hoRecord);
		// 返回当前插入记录的主键
		recordId = hoRecord.getId();

		/* 2.查询该课该班的所有学生 */
		List<Long> stuIdList = courseDao.getStudentByCourseIdAndClassnumber(courseId, classnumber);
		// 封装要批量插入的List
		List<Homework> batchList = new ArrayList<>();
		if (stuIdList.size() == 0) {
			logger.error("没有查到要布置作业的学生对象！");
		}
		for (Long stuId : stuIdList) {
			batchList.add(new Homework(courseId, courseName, classnumber, teacherId, stuId, hwContent, date, recordId));
		}
		map.clear();
		map.put("homework", batchList);
		/* 3.批量插入详细作业记录 */
		count = homeworkDao.insertByBatch(map);
		return count;
	}

	/**
	 * 显示未提交作业(学生端)
	 * 
	 * @param stuId
	 * @return
	 */
	public List<Homework> listUncommitedHomework(long stuId) {

		List<Homework> hwList = homeworkDao.getUncommitedHomeworkByStuId(stuId);
		return hwList;
	}

	/**
	 * 显示已提交作业(包括已批阅和未批阅)(学生端)
	 * 
	 * @param stuId
	 * @return
	 */
	public List<Homework> listAllCommitedHomework(long stuId) {

		List<Homework> hwList = homeworkDao.listAllCommitedHomework(stuId);
		return hwList;
	}

	/**
	 * 学生端查询所有作业
	 * 
	 * @param stuId
	 * @return
	 */
	public Map<String, Object> listAllStudentHomework(long stuId) {

		Map<String, Object> map = new HashMap<>();

		List<Homework> uncommitedList = homeworkDao.getUncommitedHomeworkByStuId(stuId);
		List<Homework> commitedList = homeworkDao.listAllCommitedHomework(stuId);

		map.put("uncommitedList", uncommitedList);
		map.put("commitedList", commitedList);

		return map;
	}

	/**
	 * 提交(更新)作业 (安卓学生端)——2017.5.17
	 * 
	 * @param homeworkId
	 * @param picPath
	 * @param homeworkImages
	 * @return
	 */
	public String commitHomework(Integer homeworkId, String picPath, MultipartFile[] homeworkImages) {

		Map<String, Object> map = new LinkedHashMap<>();
		/* 判断即将提交的作业记录是否已提交过 */
		Homework homework = homeworkDao.getHomeworkById(homeworkId);
		/* (1)还未提交,则上传作业 */
		if (homework.getHwPicture() == null && homework.getCommitTime() == null) {
			// 多文件上传
			MultipartFileUpload_1(homeworkImages, picPath);
			// 只存储该次作业上层文件夹的路径,获取作业时可通过该路径获取所有的文件
			map.put("homeworkId", homeworkId);
			map.put("picPath", picPath);
			map.put("commitDate", new Date());
			int count = homeworkDao.updateByStudent(map);
			if (count > 0) {
				return "作业提交成功！";
			} else {
				return "作业提交失败,请刷新后重新提交！";
			}
		} else {
			/* (2)已提交过,判断是否已被批阅 */
			if (homework.getHwScore() != null) {
				/* (2)-1 已被教师批阅,无法更新提交作业 */
				return "该次作业已被教师批阅,无法再次提交,请刷新查看成绩！";
			} else {
				/* (2)-2 尚未批阅,可更新提交作业 */
				// 获取服务器本地存储作业图片的url
				String picPath1 = homework.getHwPicture();
				// 将新的作业图片更新到原路径中
				MultipartFileUpload_1(homeworkImages, picPath1);
				// 更新数据库
				map.put("homeworkId", homeworkId);
				map.put("picPath", picPath1);
				map.put("commitDate", new Date());
				int count = homeworkDao.updateByStudent(map);
				if (count > 0) {
					return "作业更新成功！";
				} else {
					return "作业更新失败,请刷新后重新提交！";
				}
			}
		}
	}

	/**
	 * 提交(更新)作业(Web学生端)——method1_2017.5.17
	 * 方法一：CommonsMultipartResolver——MultipartHttpServletRequest——MultipartFile[]
	 * 
	 * @param homeworkId
	 * @param picPath
	 * @param request
	 * @return
	 */
	public String commitHomework_1(Integer homeworkId, String picPath, HttpServletRequest request) {

		Map<String, Object> map = new LinkedHashMap<>();
		/* 判断即将提交的作业记录是否已提交过 */
		Homework homework = homeworkDao.getHomeworkById(homeworkId);
		/* (1)还未提交,则上传作业 */
		if (homework.getHwPicture() == null && homework.getCommitTime() == null) {
			// 多文件上传
			MultipartFileUpload(request, picPath);
			// 只存储该次作业上层文件夹的路径,获取作业时可通过该路径获取所有的文件
			map.put("homeworkId", homeworkId);
			map.put("picPath", picPath);
			map.put("commitDate", new Date());
			int count = homeworkDao.updateByStudent(map);
			if (count > 0) {
				return "作业提交成功！";
			} else {
				return "作业提交失败,请刷新后重新提交！";
			}
		} else {
			logger.info("已提交,进行更新！");
			/* (2)已提交过,判断是否已被批阅 */
			if (homework.getHwScore() != null) {
				/* (2)-1 已被教师批阅,无法更新提交作业 */
				return "该次作业已被教师批阅,无法再次提交,请刷新查看成绩！";
			} else {
				/* (2)-2 尚未批阅,可更新提交作业 */
				// 获取服务器本地存储作业图片的url
				String picPath1 = homework.getHwPicture();
				// 将新的作业图片更新到原路径中
				MultipartFileUpload(request, picPath1);
				// 更新数据库
				map.put("homeworkId", homeworkId);
				map.put("picPath", picPath1);
				map.put("commitDate", new Date());
				int count = homeworkDao.updateByStudent(map);
				if (count > 0) {
					return "作业更新成功！";
				} else {
					return "作业更新失败,请刷新后重新提交！";
				}
			}
		}
	}

	/**
	 * 提交(更新)作业 (Web学生端)——2017.5.19 方法二：使用FileUpload工具类实现文件上传
	 * 
	 * @param homeworkId
	 * @param picPath
	 * @param request
	 * @return
	 */
	public String commitHomework_2(Integer homeworkId, String picPath, HttpServletRequest request) {

		Map<String, Object> map = new LinkedHashMap<>();
		/* 判断即将提交的作业记录是否已提交过 */
		Homework homework = homeworkDao.getHomeworkById(homeworkId);
		/* (1)还未提交,则上传作业 */
		if (homework.getHwPicture() == null && homework.getCommitTime() == null) {
			// 多文件上传
			MultipartFileUpload_2(request, picPath);
			// 只存储该次作业上层文件夹的路径,获取作业时可通过该路径获取所有的文件
			map.put("homeworkId", homeworkId);
			map.put("picPath", picPath);
			map.put("commitDate", new Date());
			int count = homeworkDao.updateByStudent(map);
			if (count > 0) {
				return "作业提交成功！";
			} else {
				return "作业提交失败,请刷新后重新提交！";
			}
		} else {
			/* (2)已提交过,判断是否已被批阅 */
			if (homework.getHwScore() != null) {
				/* (2)-1 已被教师批阅,无法更新提交作业 */
				return "该次作业已被教师批阅,无法再次提交,请刷新查看成绩！";
			} else {
				/* (2)-2 尚未批阅,可更新提交作业 */
				// 获取服务器本地存储作业图片的url
				String picPath1 = homework.getHwPicture();
				// 将新的作业图片更新到原路径中
				MultipartFileUpload_2(request, picPath1);
				// 更新数据库
				map.put("homeworkId", homeworkId);
				map.put("picPath", picPath1);
				map.put("commitDate", new Date());
				int count = homeworkDao.updateByStudent(map);
				if (count > 0) {
					return "作业更新成功！";
				} else {
					return "作业更新失败,请刷新后重新提交！";
				}
			}
		}
	}

	/**
	 * 查询返回教师的所有课程的简略信息(课程编号+名称) (安卓端)
	 * 
	 * @param teacherId
	 * @return
	 */
	public List<CourseInfoBean> listCourseInfo(String teacherId) {

		List<CourseInfoBean> list = homeworkDao.listCourseInfo(teacherId);
		return list;
	}

	/**
	 * 查询返回教师的所有课程的简略信息(课程编号+名称)——放在网页端登录代码中 网页端(1)
	 * 
	 * @param teacherId
	 * @return
	 */
	public List<CourseInfoBean> listTeacherCourseInfo(String teacherId) {

		List<CourseInfoBean> list = homeworkDao.listTeacherCourseInfo(teacherId);
		return list;
	}

	/**
	 * 根据教师id和课程id获取已布置作业的班级(班号)集合 网页端(2)
	 * 
	 * @param teacherId
	 * @param courseId
	 * @return
	 */
	public List<String> getClassnumbers(String teacherId, String courseId) {

		List<String> list = null;
		Map<String, Object> map = new HashMap<>();

		map.put("teacherId", teacherId);
		map.put("courseId", courseId);

		list = homeworkDao.getClassnumbersByTeacherAndCourse(map);
		return list;
	}

	/**
	 * 根据课程id和班号获取作业的编次(布置时间+作业编号(主键)) 网页端(3)
	 * 
	 * @param teacherId
	 * @param courseId
	 * @param classnumber
	 * @return
	 */
	public List<HomeworkInfoBean> getHomeworkNumber(String teacherId, String courseId, String classnumber) {

		List<HomeworkInfoBean> list = null;
		Map<String, Object> map = new HashMap<>();

		map.put("teacherId", teacherId);
		map.put("courseId", courseId);
		map.put("classnumber", classnumber);

		list = homeworkDao.getHomeworkNumberByCourseAndClassnumber(map);

		return list;
	}

	/**
	 * 查询所选作业编次对应的详细记录(包括提交与批阅情况) 网页端(4)
	 * 
	 * @param homeworkRecordId
	 * @return
	 */
	public Map<String, Object> listAllHomeworkInfo(Integer homeworkRecordId) {

		Map<String, Object> map = new HashMap<>();
		List<AllHomeworkInfoBean> totalList = null;
		List<AllHomeworkInfoBean> uncommitedList = new ArrayList<>(); // 未提交,包括学生信息
		List<AllHomeworkInfoBean> commitedList = new ArrayList<>(); // 已提交,包括已批阅和未批阅
		List<AllHomeworkInfoBean> uncheckedList = new ArrayList<>(); // 未批阅(提供批阅接口)
		List<AllHomeworkInfoBean> checkedList = new ArrayList<>(); // 已批阅

		// 获取总的记录数
		totalList = homeworkDao.listTotalHomework(homeworkRecordId);
		for (AllHomeworkInfoBean homework : totalList) {
			// 未提交的作业
			if (homework.getHwPicture() == null) {
				uncommitedList.add(homework);
			} else {
				// 已提交的作业
				commitedList.add(homework);
				// 已提交但未批阅的作业
				if (homework.getHwScore() == null) {
					String picUrl = "";
					File file = new File(homework.getHwPicture());
					if (file.exists() && file.isDirectory()) {
						File[] files = file.listFiles();
						for (int i = 0; i < files.length; i++) {
							picUrl += (i == (files.length - 1) ? (files[i].getAbsolutePath())
									: (files[i].getAbsolutePath() + ","));
						}
					}
					// 设置作业图片的完整路径地址
					homework.setHwPicture(picUrl);
					uncheckedList.add(homework);
				} else {
					// 已批阅
					checkedList.add(homework);
				}
			}
		}
		// 列表
		map.put("totalList", totalList);
		map.put("uncommitedList", uncommitedList);
		map.put("commitedList", commitedList);
		map.put("uncheckedList", uncheckedList);
		map.put("checkedList", checkedList);
		// 列表大小
		map.put("totalListSize", totalList.size());
		map.put("uncommitedListSize", uncommitedList.size());
		map.put("commitedListSize", commitedList.size());
		map.put("uncheckedListSize", uncheckedList.size());
		map.put("checkedListSize", checkedList.size());
		return map;
	}

	/**
	 * 批改作业(网页)_更新批改后的图片+评分等级 网页端(6)
	 * 
	 * @param homeworkId
	 * @param homeworkGrade
	 * @param imgBase64————————————————————————————————————————注意：这里应该是个上传图片的数组
	 * @param imgUrl(图片在服务器本地的完整路径,由客户端获取)
	 */
	public int updateHomeworkScore(Integer homeworkId, String homeworkGrade) {

		Map<String, Object> map = new LinkedHashMap<>();
		map.put("homeworkId", homeworkId);
		map.put("homeworkGrade", homeworkGrade);
		// 更新作业评分
		int count = homeworkDao.updateHomeworkScore(map);
		return count;
	}

	/**
	 * 查询返回某次作业未批阅列表
	 * 
	 * @param homeworkRecordId
	 */
	public List<Homework> listUncheckedHomework(Integer homeworkRecordId) {
		List<Homework> uncheckedList = homeworkDao.listUncheckedHomework(homeworkRecordId);
		return uncheckedList;
	}

	/**
	 * 查询返回某次作业未提交学生信息列表
	 * 
	 * @param homeworkRecordId
	 * @return
	 */
	public List<AllHomeworkInfoBean> listUncommitedStudentInfo(Integer homeworkRecordId) {
		List<AllHomeworkInfoBean> uncommitededList = homeworkDao.listUncommitedStudentInfo(homeworkRecordId);
		return uncommitededList;
	}

	/***********************************************************************************************************************************************
	 ***********************************************************************************************************************************************
	 ******************************************************** Service方法与辅助计算方法分割线****************************************************************
	 ***********************************************************************************************************************************************
	 ***********************************************************************************************************************************************
	 */

	/**
	 * 将经过编码的图片文件解码成图片
	 * 
	 * @param base64ImgData
	 * @param filePath
	 * @throws IOException
	 */
	public void convertBase64DataToImage(String base64ImgData, String filePath) throws IOException {

		// 构造BASE64解码器
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] bs = decoder.decodeBuffer(base64ImgData);
		OutputStream os = null;
		os = new FileOutputStream(new File(filePath));
		os.write(bs);
		os.close();
	}

	/**
	 * 文件路径处理
	 * 
	 * @param picPath
	 */
	public void resolveFilePath(String picPath) {

		File file = new File(picPath);
		if (!file.exists()) {
			file.mkdirs();
		} else {
			// 清空作业路径下的所有作业,防止更新作业时使用了与之前不同的文件名
			File[] files = file.listFiles();
			for (File file1 : files) {
				file1.delete();
			}
		}
	}

	/**
	 * 多文件上传器(方法一使用)——CommonsMultipartResolver——MultipartHttpServletRequest——MultipartFile[]
	 * 底层使用的是Commons FileUpload解析multipart请求！
	 * 
	 * @param request
	 * @param picPath
	 */
	public void MultipartFileUpload(HttpServletRequest request, String picPath) {

		// 构造多部分文件上传解析器
		CommonsMultipartResolver coMultipartResolver = new CommonsMultipartResolver(
				request.getSession().getServletContext());
		// 判断是否多部分上传请求
		if (coMultipartResolver.isMultipart(request)) {
			// 转换request
			MultipartHttpServletRequest muHttpServletRequest = (MultipartHttpServletRequest) request;
			Iterator<String> iter = muHttpServletRequest.getFileNames();
			System.out.println("iter.toString():" + iter.toString());
			// 文件路径处理
			resolveFilePath(picPath);
			// 遍历上传文件
			while (iter.hasNext()) {
				System.out.println("iter.next().toString():" + iter.next().toString());
				MultipartFile multipartFile = muHttpServletRequest.getFile(iter.next().toString());
				if (multipartFile != null) {
					// 通过获取文件名判断上传的file流是否为空
					if (!multipartFile.getOriginalFilename().trim().equals("")) {
						String path = picPath + "\\" + multipartFile.getOriginalFilename();
						System.out.println("存放路径：" + path);
						File picFile = new File(path);
						if (!picFile.exists() || !picFile.isDirectory()) {
							picFile.mkdirs();
						}
						try {
							// 上传文件到指定路径
							multipartFile.transferTo(picFile);
						} catch (IllegalStateException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					} else {
						// 在前端控制"不能上传空文件"
					}
				}
			}
		}
	}

	/**
	 * 多文件上传器(方法二使用)——MultipartFile[]
	 * 
	 * @param homeworkImages
	 * @param picPath
	 */
	public void MultipartFileUpload_1(MultipartFile[] homeworkImages, String picPath) {

		// 文件路径处理
		resolveFilePath(picPath);
		// 多文件上传
		for (MultipartFile multipartFile : homeworkImages) {
			// 判断是否为空文件
			if (!multipartFile.getOriginalFilename().trim().equals("")) {
				String path = picPath + "\\" + multipartFile.getOriginalFilename();
				System.out.println("存放路径：" + path);
				File picFile = new File(path);
				// 先创建文件
				if (!picFile.exists()) {
					picFile.mkdirs();
				}
				try {
					// 上传文件到指定路径
					multipartFile.transferTo(picFile);
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				// 在前端控制"不能上传空文件"
			}
		}
	}

	/**
	 * 多文件上传器(方法三使用)——ServletFileUpload
	 * 
	 * @param request
	 * @param picPath
	 */
	public void MultipartFileUpload_2(HttpServletRequest request, String picPath) {

		// 判断表单的提交方式是否是multipart/form-data类型
		if (ServletFileUpload.isMultipartContent(request)) {
			DiskFileItemFactory dFactory = new DiskFileItemFactory(1024 * 1024 * 10,
					new File(request.getServletContext().getRealPath("/upload")));
			ServletFileUpload sFileUpload = new ServletFileUpload(dFactory);
			sFileUpload.setFileSizeMax(1024 * 1024 * 20); // 设置上传文件最大20M
			sFileUpload.setHeaderEncoding("utf-8");
			try {
				List<FileItem> list = sFileUpload.parseRequest(request);
				// 文件路径处理
				resolveFilePath(picPath);
				for (FileItem fileItem : list) {
					if (fileItem.isFormField()) {
						// 上传普通项
					} else {
						// 上传文件项
						String fileName = fileItem.getName();
						if (!fileName.trim().equals("")) {
							InputStream in = fileItem.getInputStream();
							FileOutputStream fos = new FileOutputStream(picPath + "\\" + fileName);
							byte[] buf = new byte[1024];
							int readBuf = 0;
							while ((readBuf = in.read(buf)) != -1) {
								fos.write(buf, 0, readBuf);
								fos.flush();
							}
							fos.close();
							in.close();
						}
					}
				}
			} catch (FileUploadException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}