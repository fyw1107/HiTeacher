package com.holi.module.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.holi.common.help.StatusCodeHelp;
import com.holi.common.resultBean.ResultBean;
import com.holi.common.util.StringUtil;
import com.holi.module.dao.XiaoXiNewsDao;
import com.holi.module.entity.XiaoXiNews;

/**
 * 校园新闻业务逻辑
 * 
 * @author IVAN
 *
 */
@Service
public class XiaoXiNewsService {

	@Autowired
	private XiaoXiNewsDao xiaoXiNewsDao;

	@Autowired
	private ResultBean resultBean;

	/**
	 * 1.下拉刷新
	 * 
	 * @param stuId
	 * @param teacherId
	 * @return
	 */
	public ResultBean pullDown(String stuId, String teacherId) {

		// 计算所属学院
		int schoolId = calcSchoolId(stuId, teacherId);

		List<XiaoXiNews> newsList = xiaoXiNewsDao.pullDown(schoolId, 0);
		if (newsList == null)
			return null;

		return getReturnNews(newsList);
	}

	/**
	 * 2.上拉加载
	 * 
	 * @param stuId
	 * @param datetime
	 * @param teacherId
	 * @return
	 */
	public ResultBean pullUp(String stuId, String teacherId, String datetime) {

		// 计算所属学院
		int schoolId = calcSchoolId(stuId, teacherId);

		List<XiaoXiNews> newsList = xiaoXiNewsDao.pullUp(schoolId, 0, datetime);
		if (newsList == null)
			return null;

		return getReturnNews(newsList);
	}

	/**
	 * 根据学生和教师id计算学院id
	 * 
	 * @param stuId
	 * @param teacherId
	 * @return
	 */
	private int calcSchoolId(String stuId, String teacherId) {
		int schoolId = 0;
		if (stuId == null) {
			// 截取最后两位(测试用)
			schoolId = Integer.valueOf(StringUtil.subString(teacherId, teacherId.length() - 2, teacherId.length()));
		} else {
			if (stuId.length() > 5) {
				// 真实学号用
				schoolId = Integer.valueOf(StringUtil.subString(stuId, 2, 4));
			} else {
				// 截取最后两位(测试用)
				schoolId = Integer.valueOf(StringUtil.subString(stuId, stuId.length() - 2, stuId.length()));
			}
		}
		return schoolId;
	}

	/**
	 * 对查询到的新闻List进行处理,返回合适数量的新闻
	 * 
	 * @param newsList
	 * @return
	 */
	public ResultBean getReturnNews(List<XiaoXiNews> newsList) {
		// 用于记录返回的当次查询结果
		List<XiaoXiNews> resultList = null;

		// 不超过10条，则全部返回
		if (newsList.size() <= 10) {
			// 处理新闻内容,用于前台显示
			resultList = HandleData(newsList);
			resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, resultList);
		} else {
			// 选择返回某一日期之前的所有新闻(注意：同一日期的新闻不能分多次返回)
			List<XiaoXiNews> tempList = null;
			tempList = handleList(newsList);
			// 处理新闻内容,用于前台显示
			resultList = HandleData(tempList);
			resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, resultList);
		}
		return resultBean;
	}

	/**
	 * 对List进行截取
	 * 
	 * @param newsList
	 * @return
	 */
	public List<XiaoXiNews> handleList(List<XiaoXiNews> newsList) {

		int bound = 0; // 用于记录返回的最后一条新闻在list中的下标
		// 获取第10条记录的日期
		Date date = newsList.get(9).getDate();
		int i = 0;
		for (i = 10; i < newsList.size(); i++) {
			if (newsList.get(i).getDate().getTime() < date.getTime())
				break;
		}
		bound = i;
		// 以bound值为边界截取新闻list的0~bound部分
		return newsList.subList(0, bound); // include 0 , exclude bound
	}

	/**
	 * 处理从数据库查出的数据
	 * 
	 * @param newsList
	 * @return
	 */
	private ArrayList<XiaoXiNews> HandleData(List<XiaoXiNews> newsList) {

		ArrayList<XiaoXiNews> reslutList = new ArrayList<XiaoXiNews>();

		for (XiaoXiNews xiaoxiNews : newsList) {
			// 用于列表页面缩略显示新闻内容
			xiaoxiNews.setContent(
					StringUtil.subString(xiaoxiNews.getContent(), 0, (int) (xiaoxiNews.getContent().length() * 0.1)));
			xiaoxiNews.setPicsrc(subPicsrc(xiaoxiNews.getPicsrc()));
			reslutList.add(xiaoxiNews);
		}
		return reslutList;
	}

	/**
	 * 处理图片路径,只取第一个,用于列表页面新闻图片显示
	 * 
	 * @param str
	 * @return
	 */
	private String subPicsrc(String str) {

		if (str != null) {
			str = str.replace("[", "").replace("]", "").replaceAll("u'", "").replaceAll("'", "").replaceAll(" ", "");
			String[] list = str.split(",");
			str = list[0];
		}
		return str;
	}
}
