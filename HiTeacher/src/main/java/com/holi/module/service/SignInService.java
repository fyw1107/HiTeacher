package com.holi.module.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.holi.common.help.StatusCodeHelp;
import com.holi.common.jpush.Jpush;
import com.holi.common.jsonBean.CheckInMsg;
import com.holi.common.jsonBean.StudentSignInMsg;
import com.holi.common.jsonBean.WifiItem;
import com.holi.common.resultBean.ResultBean;
import com.holi.common.resultBean.SignDataBean;
import com.holi.common.resultBean.SignResultBean;
import com.holi.common.util.ListUtil;
import com.holi.common.yunxin.security.RandomNum;
import com.holi.module.dao.ClassGroupDao;
import com.holi.module.dao.CourseDao;
import com.holi.module.dao.SignCacheDao;
import com.holi.module.dao.SignInDao;
import com.holi.module.dao.StudentDao;
import com.holi.module.dao.UserDao;
import com.holi.module.entity.ClassGroup;
import com.holi.module.entity.Course;
import com.holi.module.entity.SignCache;
import com.holi.module.entity.SignIn;
import com.holi.module.entity.Student;

import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;

/**
 * 签到逻辑层
 * 
 * @author lz
 *
 */
@Transactional
@Service
public class SignInService {

	@Autowired
	private SignInDao signInDao;

	@Autowired
	private SignCacheDao signInfDao;

	@Autowired
	private SignCacheDao signCacheDao;

	@Autowired
	private ResultBean resultBean;

	@Autowired
	private CourseDao courseDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private StudentDao studentDao;

	@Autowired
	private ClassGroupDao classGroupDao;

	/**
	 * 开启签到业务逻辑
	 * 
	 * @param teacherId
	 */
	public ResultBean startSignIn(String teacherId) {

		// 通过时间和teacherId 获取当前上课的学生
		// TODO
		// 获取老师上的课程，根据课程取出上课的学生
		List<Course> coursesList = courseDao.findCourseByTeacherId(teacherId);
		int size = coursesList.size();

		// 生成随机数组
		String randomNumStr = RandomNum.getRandomNum((int) (size * 0.7), size);
		// 随机的学生开启热点
		String[] hotSpotStudent = randomNumStr.split("");

		// 上课总人数
		String[] allStudent = new String[size];
		for (int i = 0; i < size; i++) {
			allStudent[i] = String.valueOf(i);
		}

		// 开启wifi搜索热点的学生
		String[] wifiStudent = (String[]) ListUtil.substract(allStudent, hotSpotStudent);

		// 随机取出若干学生 随机分配热点名
		// Jpush对随机的学生发送开启热点指令
		JPushClient jPushClient = Jpush.getJPushClient();

		for (int i = 0; i < hotSpotStudent.length; i++) {
			Long stuId = coursesList.get(Integer.valueOf(hotSpotStudent[i])).getStuId();

			String jpushId = userDao.getjpushIdByStuId(stuId);

			if (jpushId == null)
				continue;

			try {
				Map<String, String> extras = new HashMap<String, String>();
				extras.put("msgType", "1004");
				extras.put("signType", "1");
				extras.put("teacherId", teacherId);
				extras.put("hotSpotName", String.valueOf(stuId));
				PushResult result = jPushClient.sendAndroidNotificationWithRegistrationID("SignIn", "statrtSignIn",
						extras, jpushId);

				// 储存学生与热点的关系
				SignCache signCache = new SignCache();

				signCache.setTeacherId(teacherId);
				signCache.setStuId(stuId);
				signCache.setWifiName(String.valueOf(stuId));
				signCache.setIsWifiCreater(1);

				signCacheDao.insert(signCache);

			} catch (APIConnectionException e) {
				e.printStackTrace();
			} catch (APIRequestException e) {
				e.printStackTrace();
			}

		}

		// 对其他同学发送搜索热点 指令
		for (int i = 0; i < wifiStudent.length; i++) {

			Long stuId = coursesList.get(Integer.valueOf(wifiStudent[i])).getStuId();

			String jpushId = userDao.getjpushIdByStuId(stuId);

			if (jpushId == null)
				continue;

			try {
				Map<String, String> extras = new HashMap<String, String>();
				extras.put("msgType", "1004");
				extras.put("signType", "0");
				extras.put("teacherId", teacherId);
				extras.put("hotSpotName", "");
				PushResult result = jPushClient.sendAndroidNotificationWithRegistrationID("SignIn", "statrtSignIn",
						extras, jpushId);
			} catch (APIConnectionException e) {
				e.printStackTrace();
			} catch (APIRequestException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, null);
		return resultBean;
	}

	/**
	 * 获取签到信息
	 * 
	 * @param studentId
	 * @param teacherId
	 */
	public ResultBean sendSignInMessage(StudentSignInMsg sigMsg) {

		Long stuId = sigMsg.getStuId();
		String teacherId = sigMsg.getTeacherId();
		List<WifiItem> wifiList = sigMsg.getWifiList();

		if (wifiList.isEmpty()) {
			SignIn signIn = new SignIn();

			signIn.setTeacherId(teacherId);
			signIn.setStuId(stuId);
			signIn.setSignFlag(0);
			signIn.setCreateTime(new Date());

			signInDao.insert(signIn);

			resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, null);
			return resultBean;
		}

		// 将数据存入signCache
		for (WifiItem wifiItem : wifiList) {
			SignCache sc = new SignCache();

			sc.setWifiName(wifiItem.getSSID());
			sc.setTeacherId(teacherId);
			sc.setStuId(stuId);
			sc.setCreateTime(new Date());

			signInfDao.insert(sc);
		}

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, null);
		return resultBean;

	}

	/**
	 * 处理签到数据
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ResultBean handleData(String teacherId) {

		// wifi名字与同连上此wifi名字的SignCache
		HashMap<String, List<SignCache>> wifiNameStuIdMap = new HashMap<String, List<SignCache>>();

		// 热点名与开启者MAP
		HashMap<String, SignCache> wifiCacheMap = new HashMap<String, SignCache>();

		// 用于排序的TreeMap,降序排序。
		Map<Integer, String> treeMap = new TreeMap<Integer, String>(new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o2 - o1;
			}
		});

		// 未签到的StuId
		// Set<SignCache> notsignList = new HashSet<SignCache>();

		// 已经签到的StuId
		List<SignCache> signList = new ArrayList<SignCache>();

		// 根据老师ID获取开启的热点
		List<SignCache> wifiNameList = signCacheDao.findAllCreateWifi(teacherId, 1);

		if (wifiNameList.isEmpty()) {
			resultBean.set(StatusCodeHelp.internalErr, StatusCodeHelp.internalErrMessage, null);
			return resultBean;
		}

		for (SignCache signCache : wifiNameList) {

			String wifiName = signCache.getWifiName();
			List<SignCache> signCacheList = signInfDao.findStuIdBywifiName(wifiName, teacherId);

			wifiNameStuIdMap.put(wifiName, signCacheList);
			treeMap.put(signCacheList.size(), wifiName);
			wifiCacheMap.put(wifiName, signCache);

		}

		// 如果开启的热点没人连上
		if (wifiNameStuIdMap.isEmpty()) {
			resultBean.set(StatusCodeHelp.internalErr, "本次签到失败，请重新签到", null);
			return resultBean;
		}

		// 判断那个wifi链接人数最多，可能出现多个wifi人数相同但是都是最多的。
		// 连接最多人数的所有wifi开启者算到，
		// 依次交集，有交集取并集，无交集，没来。
		Set<Integer> keySet = treeMap.keySet();
		Object[] keyList = keySet.toArray();
		String wifiName0 = treeMap.get(keyList[0]);

		signList = wifiNameStuIdMap.get(wifiName0);
		signList.add(wifiCacheMap.get(wifiName0));

		for (int i = 1; i < keyList.length; i++) {

			List<SignCache> list1 = wifiNameStuIdMap.get(treeMap.get(keyList[i]));
			// 取交集
			List<SignCache> unionList = (List<SignCache>) ListUtil.intersect(signList, list1);

			if (!unionList.isEmpty()) {
				// 取并集
				signList = (List<SignCache>) ListUtil.union(signList, list1);
			} else {
				break;
			}
		}

		// 返回的Data
		HashMap<String, Object> resultMap = new HashMap<String, Object>();

		// 所有上这节课的学生
		List<Student> allStudentlist = new ArrayList<Student>();
		// 未签到的学生信息（stuId,stuName）
		List<Student> notsignMsg = new ArrayList<Student>();
		// 签到的学生信息（stuId,stuName）
		List<Student> signMsg = new ArrayList<Student>();

		// 签到的设为“1”
		for (SignCache signCache : signList) {
			SignIn signIn = new SignIn();
			Student student = signCache.getStudent();

			signIn.setTeacherId(teacherId);
			signIn.setStuId(student.getStuId());
			signIn.setSignFlag(1);
			signIn.setCreateTime(new Date());

			signInDao.insert(signIn);

			signMsg.add(student);
		}

		// TODO
		signCacheDao.deleteAll();

		resultMap.put("sign", signMsg);
		resultMap.put("notSign", notsignMsg);

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, resultMap);
		return resultBean;
	}

	/**
	 * 获取某个学生上的所有课程的签到数据
	 * 
	 * @param stuId
	 * @return
	 */
	public ResultBean getStuSignInData(Long stuId) {

		List<Course> coursesList = courseDao.findCourseByStuId(stuId);

		for (Course course : coursesList) {

			String courseId = course.getCourseId();

			List<SignIn> signDataList = signInDao.findSignDataByStuIdAndCourserId(stuId, courseId);

			double signRate = signRate(signDataList);

			SignDataBean signDataBean = new SignDataBean();
			course.setSignRate(signRate);
		}

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, coursesList);
		return resultBean;
	}

	/**
	 * 获取某个老师上的所有课程的签到数据
	 * 
	 * @param teacherId
	 * @param courseId
	 * @return
	 */
	public ResultBean getTeacherSignInData(String teacherId) {

		List<Course> coursesList = courseDao.findCourseByTeacherId(teacherId);

		for (Course course : coursesList) {
			String courseId = course.getCourseId();

			List<SignIn> signDataList = signInDao.findSignDataByCourserId(courseId);

			double signRate = signRate(signDataList);

			ClassGroup classgroup = classGroupDao.getClassGroupByCourseId(courseId);

			course.setSignRate(signRate);
			course.setClassGroupId(classgroup.getTid());
		}

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, coursesList);
		return resultBean;
	}

	/**
	 * 获取某个课程下所有学生的签到数据
	 * 
	 * @param courseId
	 * @return
	 */
	public ResultBean getCourseSignInData(String courseId) {
		List<Student> stuList = studentDao.findStuByCourseId(courseId);
		for (Student student : stuList) {
			Long stuId = student.getStuId();

			List<SignIn> signDataList = signInDao.findSignDataByStuIdAndCourserId(stuId, courseId);

			double signRate = signRate(signDataList);

			student.setSignRate(signRate);
		}

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, stuList);
		return resultBean;
	}

	/**
	 * 计算签到率
	 * 
	 * @param signDataList
	 * @return
	 */
	private double signRate(List<SignIn> signDataList) {

		if (signDataList.isEmpty())
			return 0;

		int signNum = 0;
		// int notSignNum = 0;

		for (SignIn signIn : signDataList) {
			if (signIn.getSignFlag() == 1)
				signNum++;
		}

		double signRate = (double) signNum / (signDataList.size());
		return signRate;
	}

	/**
	 * 学生端上传签到信息（v2.0）
	 * 
	 * @param sigMsg
	 * @return
	 */
	public ResultBean uploadSignMsg(CheckInMsg sigMsg) {

		String stuId = sigMsg.getStuId();
		String courseId = sigMsg.getCourseId();
		String teacherId = sigMsg.getTeacherId();

		String classNumber = sigMsg.getClassNumber();
		String signFlag = sigMsg.getSingFlag();

		System.out.println(">>>>>>>>>>>>>>>>>>");
		System.out.println(teacherId + " " + stuId + " " + courseId + " " + classNumber + " " + " " + signFlag);
		System.out.println(">>>>>>>>>>>>>>>>>>");

		SignCache signCache = new SignCache();

		signCache.setStuId(Long.parseLong(stuId));
		signCache.setTeacherId(teacherId);
		signCache.setCourseId(courseId);
		signCache.setClassNumber(classNumber);
		signCache.setSignFlag(signFlag.equals("T") ? 1 : 0);
		signCache.setCreateTime(new Date());

		if (signCache.getTeacherId() != null && signCache.getSignFlag() == 1) {
			signCacheDao.insert(signCache);
		}

		Set<String> sginMsgList = sigMsg.getBluetoothNameList();

		for (String str : sginMsgList) {
			SignCache signCacheTemp = new SignCache();
			System.out.println("++++++++++++++++++++");
			System.out.println(str);
			System.out.println("++++++++++++++++++++");
			try {
				String[] s = str.split("\\|");
				/*
				 * System.out.println("rrrrrrrrrrrrrrrrrr");
				 * System.out.println(s[0]); System.out.println(s[1]);
				 * System.out.println(s[2]);
				 * System.out.println("rrrrrrrrrrrrrrrrr");
				 */
				signCacheTemp.setCourseId(courseId);
				signCacheTemp.setClassNumber(classNumber);
				signCacheTemp.setTeacherId(teacherId);
				signCacheTemp.setStuId(Long.parseLong(s[1], 16));
				signCacheTemp.setSignFlag(s[2].equals("T") ? 1 : 0);
				signCacheTemp.setCreateTime(new Date());
			} catch (Exception e) {
				e.printStackTrace();
				resultBean.set(StatusCodeHelp.parameterError, StatusCodeHelp.parameterErrorMessage, null);
				return resultBean;
			}

			// 只插入签到成功的学生
			if (signCacheTemp.getSignFlag() == 1 && signCacheTemp.getTeacherId() != null) {
				signCacheDao.insert(signCacheTemp);
			}
		}

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, null);
		return resultBean;
	}

	/**
	 * 教师端获取签到结果
	 * 
	 * @param teacherId
	 * @param courseId
	 * @return
	 */
	public ResultBean getSignInResult(String teacherId, String isSignEnd) {
		// 通过techerId获取最新的一条数据
		SignCache signCache = signCacheDao.getRecentDataByTeacherId(teacherId);

		if (signCache == null) {
			resultBean.set(StatusCodeHelp.signFail, StatusCodeHelp.signFailMessage, null);
			return resultBean;
		}

		String courseId = signCache.getCourseId();
		String classNumber = signCache.getClassNumber();

		Course course = courseDao.getCourseBycourseIdAndClassNumber(courseId, " " + classNumber);// TODO
		String courseName = course.getCourseName();

		// 将缓存表signInCache中的签到数据
		List<SignCache> signCacheList = signCacheDao.findSignDataByCourserId(courseId, classNumber);
		List<SignIn> signInList = new ArrayList<>();
		List<SignIn> notSignInList = new ArrayList<>();

		// 所有上这节课的学生
		List<Student> allStudentlist = new ArrayList<Student>();
		// 未签到的学生
		List<Student> notsignMsg = new ArrayList<Student>();
		// 签到的学生
		List<Student> signMsg = new ArrayList<Student>();

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		// 返回未签到的学生信息（stuId,stuName）
		List<SignResultBean> notsignStudentMsg = new ArrayList<SignResultBean>();
		// 返回签到的学生信息（stuId,stuName）
		List<SignResultBean> signStudentMsg = new ArrayList<SignResultBean>();

		if (signCacheList.isEmpty()) {
			resultBean.set(StatusCodeHelp.signFail, StatusCodeHelp.signFailMessage, null);
			return resultBean;
		}

		for (SignCache signCacheTemp : signCacheList) {
			SignIn signIn = new SignIn();

			signIn.setCourseId(signCacheTemp.getCourseId());
			signIn.setStuId(signCacheTemp.getStudent().getStuId());
			signIn.setSignFlag(signCacheTemp.getSignFlag());
			signIn.setTeacherId(teacherId);
			signIn.setClassNumber(classNumber);
			signIn.setCreateTime(new Date());

			signInList.add(signIn);

			// 获取签到成功的学生
			if (signCacheTemp.getSignFlag() == 1) {
				Student student = studentDao.getByStuId(signCacheTemp.getStudent().getStuId());
				signMsg.add(student);
			}
		}

		if (isSignEnd.equals("true")) {
			signInDao.insertSignData(signInList);
			int i = signCacheDao.deleteByTeacherId(teacherId);
		}

		// 根据课程ID取出上课的学生
		List<Course> coursesList = courseDao.findByCourseId(courseId);

		for (Course courseTemp : coursesList) {
			Student student = studentDao.getByStuId(courseTemp.getStuId());
			allStudentlist.add(student);
		}

		notsignMsg = ListUtil.substractList(allStudentlist, signMsg);

		for (Student student : signMsg) {
			SignResultBean signResultBean = new SignResultBean();
			signResultBean.setSutId(student.getStuId());
			signResultBean.setStuName(student.getStuName());
			signResultBean.setCourseName(courseName);

			signStudentMsg.add(signResultBean);
		}

		for (Student student : notsignMsg) {
			SignIn signIn = new SignIn();

			signIn.setCourseId(courseId);
			signIn.setStuId(student.getStuId());
			signIn.setSignFlag(0);
			signIn.setCreateTime(new Date());
			signIn.setTeacherId(teacherId);
			signIn.setClassNumber(classNumber);

			notSignInList.add(signIn);

			SignResultBean signResultBean = new SignResultBean();
			signResultBean.setSutId(student.getStuId());
			signResultBean.setStuName(student.getStuName());
			signResultBean.setCourseName(courseName);

			notsignStudentMsg.add(signResultBean);
		}

		if (isSignEnd.equals("true")) {
			signInDao.insertSignData(notSignInList);
		}

		resultMap.put("sign", signStudentMsg);
		resultMap.put("notSign", notsignStudentMsg);

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, resultMap);
		return resultBean;
	}

}
