package com.holi.module.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.holi.common.yunxin.request.ClassGroupRequest;
import com.holi.module.dao.AllCourseDao;
import com.holi.module.dao.ClassGroupDao;
import com.holi.module.dao.CourseDao;
import com.holi.module.entity.AllCourse;
import com.holi.module.entity.ClassGroup;
import com.holi.module.entity.Course;

import okhttp3.Response;

/**
 * 班級群业务类
 * 
 * @author lz
 *
 */
@Service
@Transactional
public class ClassGroupService {

	@Autowired
	private CourseDao courseDao;

	@Autowired
	private ClassGroupDao classGroupDao;

	@Autowired
	private AllCourseDao allCourseDao;

	/**
	 * 根据学生ID管理班级群
	 * 
	 * @param stuId
	 */
	public void manageClassGroupByStuId(String stuId, String userId) {

		// 取出该学生上的所有课程
		System.out.println(Long.valueOf(stuId));
		List<Course> courserList = courseDao.findCourseByStuId(Long.valueOf(stuId));

		for (Course course : courserList) {
			// 根据课程获取本课程的班级群
			ClassGroup classGroupTemp = classGroupDao.getClassGroupByCourseId(course.getCourseId());
			// 判断该课程是否已有班级群
			// 没有班级群，创建班级群
			if (classGroupTemp == null) {
				ClassGroup classGroup = new ClassGroup();

				String courseName = sensitiveWord(course.getCourseName());

				classGroup.setTname(courseName);
				classGroup.setOwner(userId);
				classGroup.setMembers("[\"" + userId + "\"]");
				classGroup.setMsg(course.getCourseName());
				classGroup.setMagree(0);
				classGroup.setJoinmode(2);
				classGroup.setBeinvitemode(1);

				try {
					Response response = ClassGroupRequest.getInstance().createClassGroup(classGroup);
					// 处理返回结果
					String str = response.body().string();

					System.out.println(str);

					JSONObject jsonObject = new JSONObject(str);
					int code = jsonObject.getInt("code");

					if (code == 200) {
						String tid = jsonObject.getString("tid");

						classGroup.setTid(tid);
						classGroupDao.insert(classGroup);
						courseDao.insertCourseAndClassGroupRelation(course.getCourseId(), tid);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {
				// 有，加入该群
				try {
					classGroupTemp.setMembers("[\"" + userId + "\"]");
					Response response = ClassGroupRequest.getInstance().addClassGroup(classGroupTemp);

					System.out.println("havae calssgroup" + response.body().string());

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}

	/**
	 * 根据老师ID管理班级群
	 * 
	 * @param teacherId
	 */
	public void manageClassGroupByteacherId(String teacherId, String userId) {
		// 获取老师上的所有课程
		List<AllCourse> allCourseList = allCourseDao.findCourseByTeacherId(teacherId);

		for (AllCourse allCourse : allCourseList) {
			// 根据课程获取本课程的班级群
			ClassGroup classGroupTemp = classGroupDao.getClassGroupByCourseId(allCourse.getCourseId());
			// 判断该课程是否已有班级群
			// 没有班级群，创建班级群
			if (classGroupTemp == null) {
				ClassGroup classGroup = new ClassGroup();

				String courseName = allCourse.getCourseName();
				courseName = sensitiveWord(courseName);

				classGroup.setTname(courseName);
				classGroup.setOwner(userId);
				classGroup.setMembers("[\"" + userId + "\"]");
				classGroup.setMsg(allCourse.getCourseName());
				classGroup.setMagree(0);
				classGroup.setJoinmode(2);
				classGroup.setBeinvitemode(1);

				try {
					Response response = ClassGroupRequest.getInstance().createClassGroup(classGroup);
					// 处理返回结果
					String str = response.body().string();
					JSONObject jsonObject = new JSONObject(str);
					String tid = jsonObject.getString("tid");

					courseDao.insertCourseAndClassGroupRelation(allCourse.getCourseId(), tid);
					classGroup.setTid(tid);
					classGroup.setCreateTime(new Date());
					classGroupDao.insert(classGroup);
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {

				try {
					// 有，加入该群
					classGroupTemp.setMembers("[\"" + userId + "\"]");
					Response response0 = ClassGroupRequest.getInstance().addClassGroup(classGroupTemp);

					// 移交群主
					String newowner = userId;
					String leave = "2";
					Response response1 = ClassGroupRequest.getInstance().changeOwner(classGroupTemp, newowner, leave);

					String str = response1.body().string();
					JSONObject jsonObject = new JSONObject(str);
					int code = jsonObject.getInt("code");

					if (code == 200)
						// 更新数据（owner）
						classGroupDao.updataOwner(classGroupTemp.getTid(), newowner);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}

	private String sensitiveWord(String courseName) {

		// 正则表达式规则
		String regEx = "毛泽东";
		// 编译正则表达式
		Pattern pattern = Pattern.compile(regEx);

		// 忽略大小写的写法
		Matcher matcher = pattern.matcher(courseName);

		// 查找字符串中是否有匹配正则表达式的字符/字符串
		if (matcher.find()) {
			courseName = "毛概";
		}

		return courseName;
	}
}
