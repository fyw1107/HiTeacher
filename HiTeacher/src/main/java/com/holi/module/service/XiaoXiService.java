package com.holi.module.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.holi.common.help.StatusCodeHelp;
import com.holi.common.resultBean.ResultBean;
import com.holi.common.resultBean.XiaoxiNewsResultBean;
import com.holi.common.util.StringUtil;
import com.holi.module.dao.XiaoXiNewsDao;
import com.holi.module.entity.XiaoXiNews;

/**
 * 校息业务逻辑
 * 
 * @author lz
 *
 */
@Service
public class XiaoXiService {

	@Autowired
	private XiaoXiNewsDao xiaoXiNewsDao;
	@Autowired
	private ResultBean resultBean;

	/**
	 * 下拉刷新
	 *//*
	public ResultBean pullDown(String stuId, String teacherId) {

		int temp = handlePara(stuId, teacherId);

		List<XiaoxiNewsResultBean> newsList = xiaoXiNewsDao.pullDown(temp);
		if (newsList == null)
			return null;

		List<XiaoxiNewsResultBean> resultList = HandleData(newsList);

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, resultList);

		return resultBean;
	}

	private int handlePara(String stuId, String teacherId) {
		int temp = 0;
		if (teacherId == null)
			temp = Integer.valueOf(StringUtil.subString(stuId, stuId.length() - 1, stuId.length()));
		else
			temp = Integer.valueOf(StringUtil.subString(teacherId, teacherId.length() - 1, teacherId.length()));
		return temp;
	}

	*//**
	 * 上拉加载
	 * 
	 * @return
	 *//*
	public ResultBean pullUp(String stuId, int lastId, String teacherId) {

		int temp = handlePara(stuId, teacherId);

		List<XiaoxiNewsResultBean> newsList = xiaoXiNewsDao.pullUp(temp, lastId);
		if (newsList == null)
			return null;

		List<XiaoxiNewsResultBean> resultList = HandleData(newsList);

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, resultList);

		return resultBean;
	}*/

	/**
	 * 处理图片路径，只取第一个
	 * 
	 * @param str
	 * @return
	 */
	private String subPicsrc(String str) {

		if (str != null) {
			str = str.replace("[", "").replace("]", "").replaceAll("'", "").replaceAll(" ", "");
			String[] list = str.split(",");
			str = list[0];
		}
		return str;
	}

	/**
	 * 处理从数据库查出的数据
	 * 
	 * @param newsList
	 * @return
	 */
	private ArrayList<XiaoxiNewsResultBean> HandleData(List<XiaoxiNewsResultBean> newsList) {

		ArrayList<XiaoxiNewsResultBean> reslutList = new ArrayList<XiaoxiNewsResultBean>();

		for (XiaoxiNewsResultBean xiaoxiNews : newsList) {
			xiaoxiNews.setContent(
					StringUtil.subString(xiaoxiNews.getContent(), 0, (int) (xiaoxiNews.getContent().length() * 0.1)));
			xiaoxiNews.setPicsrc(subPicsrc(xiaoxiNews.getPicsrc()));
			reslutList.add(xiaoxiNews);
		}

		return reslutList;
	}

}
