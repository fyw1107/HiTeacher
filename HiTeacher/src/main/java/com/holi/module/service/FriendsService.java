package com.holi.module.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.holi.common.help.StatusCodeHelp;
import com.holi.common.resultBean.ResultBean;
import com.holi.module.dao.FriendsDao;
import com.holi.module.entity.Friends;

/**
 * 好友关系管理逻辑层
 * 
 * @author lz
 *
 */
@Service
public class FriendsService {

	@Autowired
	private FriendsDao friendsDao;

	@Autowired
	private ResultBean resultBean;

	public Object addFriend(String accid, String faccid) throws IOException {

		Friends friends = new Friends();

		friends.preInsert();
		friends.setAccid(accid);
		friends.setFaccid(faccid);

		friendsDao.insert(friends);

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, null);

		return resultBean;
	}

	/**
	 * 刪除好友
	 * 
	 * @param accid
	 * @param faccid
	 * @return
	 */
	public Object removeFriend(String accid, String faccid) {
		
		friendsDao.deleteFriend(accid, faccid);
		
		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, null);

		return resultBean; 
	}

}