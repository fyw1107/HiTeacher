package com.holi.module.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.holi.common.help.StatusCodeHelp;
import com.holi.common.jsonBean.OCR;
import com.holi.common.jsonBean.Words;
import com.holi.common.resultBean.ResultBean;
import com.holi.common.resultBean.oneCradBean;
import com.holi.common.yunxin.request.OcrRequest;
import com.holi.module.dao.StudentDao;
import com.holi.module.entity.Student;

import okhttp3.Response;
import sun.misc.BASE64Encoder;

/**
 * OCR业务层
 * 
 * @author lz
 *
 */
@SuppressWarnings("restriction")
@Service
public class OcrService {

	@Autowired
	private ResultBean resultBean;

	@Autowired
	private StudentDao studentDao;

	/**
	 * 一卡通识别 业务
	 * 
	 * @param oneCardImages
	 * @return
	 * @throws IOException
	 */
	public ResultBean oneCard(MultipartFile[] oneCardImages) throws Exception {

		OCR ocr = recognitionImageData(oneCardImages);

		List<Words> wordLsit = ocr.getWords_result();

		String stuId = null;
		for (Words words : wordLsit) {

			String reg = "^.*\\d{9}.*$";
			String word = words.getWords();

			if (word.matches(reg)) {
				String regEx = "[^0-9]";
				Pattern p = Pattern.compile(regEx);
				Matcher m = p.matcher(word);
				stuId = m.replaceAll("").trim();
				System.out.println(stuId);
			}
		}

		if (stuId == null) {
			resultBean.set(StatusCodeHelp.OCRoneCradError, StatusCodeHelp.OCRoneCradErrorMessage, null);
			return resultBean;
		}

		Student student = studentDao.getByStuId(Long.parseLong(stuId));

		if (student == null) {
			resultBean.set(StatusCodeHelp.oneCradNotBand, StatusCodeHelp.oneCradNotBandMessage, null);
			return resultBean;
		} else {
			oneCradBean oneCradBean = new oneCradBean(student.getStuId(), student.getUserId(), student.getStuName(),
					student.getXueyuanName());
			resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, oneCradBean);
			return resultBean;
		}

	}

	/**
	 * 普通图片识别
	 * 
	 * @param generalImage
	 * @return
	 * @throws Exception
	 */
	public ResultBean generalImage(MultipartFile[] generalImage) throws Exception {
		OCR ocr = recognitionImageData(generalImage);
		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, ocr);
		return resultBean;
	}

	/**
	 * 识别图片文字
	 * 
	 * @param oneCardImages
	 * @return
	 * @throws IOException
	 */
	private OCR recognitionImageData(MultipartFile[] oneCardImages) throws IOException {
		String base64ImageStr = GetBase64ImageStr(oneCardImages[0].getInputStream());

		Response response = OcrRequest.getInstance().generalOCR(base64ImageStr);
		String OCRStr = response.body().string();

		System.out.println(OCRStr);

		Gson gson = new Gson();
		OCR ocr = gson.fromJson(OCRStr, OCR.class);
		return ocr;
	}

	/**
	 * 将图片base64编码
	 * 
	 * @param in
	 * @return
	 */
	private static String GetBase64ImageStr(InputStream in) {
		// 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
		byte[] data = null;

		// 读取图片字节数组
		try {
			data = new byte[in.available()];
			in.read(data);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// 对字节数组Base64编码
		BASE64Encoder encoder = new BASE64Encoder();
		return encoder.encode(data).replace("\r\n", "");// 返回Base64编码过的字节数组字符串
	}

}
