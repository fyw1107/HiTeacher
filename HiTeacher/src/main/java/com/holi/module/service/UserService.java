package com.holi.module.service;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.holi.common.help.StatusCodeHelp;
import com.holi.common.python.JythonUtil;
import com.holi.common.resultBean.ResultBean;
import com.holi.common.security.MD5;
import com.holi.common.yunxin.request.UserRequest;
import com.holi.module.dao.AllCourseDao;
import com.holi.module.dao.CourseDao;
import com.holi.module.dao.CourseScheduleDao;
import com.holi.module.dao.FeedbackDao;
import com.holi.module.dao.StudentDao;
import com.holi.module.dao.TeacherDao;
import com.holi.module.dao.UserDao;
import com.holi.module.entity.AllCourse;
import com.holi.module.entity.Feedback;
import com.holi.module.entity.Student;
import com.holi.module.entity.Teacher;
import com.holi.module.entity.User;

import okhttp3.Response;

/*
 * user业务逻辑
 * @author lz
 */
@Service
public class UserService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private ResultBean resultBean;

	@Autowired
	private FeedbackDao feedbackDao;

	@Autowired
	private ClassGroupService classGroupService;

	@Autowired
	private StudentDao studentDao;

	@Autowired
	private TeacherDao teacherDao;

	@Autowired
	private AllCourseDao allCourseDao;

	@Autowired
	private CourseDao courseDao;

	@Autowired
	private CourseScheduleDao courseScheduleDao;

	/**
	 * 新用户注册业务
	 * 
	 * @param phone
	 * @param password
	 * @throws IOException
	 */
	public String register(String accid, String name, String token) throws IOException {
		// TODO
		Response response = UserRequest.getInstance().userRegister(accid, name, token);

		String str = response.body().string();
		JSONObject jsonObject = new JSONObject(str);
		int code = jsonObject.getInt("code");

		if (code == 200) {
			User user = new User();

			user.setUserId(accid);
			user.setUserPassword(token);
			user.setNickName(name);
			user.setUserType(-1);
			user.setCreateTime(new Date());

			userDao.insert(user);
		}
		return str;

	}

	/**
	 * 用户登陆 业务层
	 * 
	 * @param phone
	 * @param password
	 */
	public ResultBean login(String phone, String password) {

		User user = userDao.getByPhone(phone);

		if (user == null) {
			resultBean.set(StatusCodeHelp.notFoundUser, StatusCodeHelp.notFoundUserMeaage, null);
			return resultBean;
		}

		if (user.getUserType() == 0) {
			resultBean.set(StatusCodeHelp.notTeacher, StatusCodeHelp.notTeacherMessage, null);
			return resultBean;
		}

		password = MD5.getStringMD5(password);

		if (!password.equals(user.getUserPassword())) {
			resultBean.set(StatusCodeHelp.passwordError, StatusCodeHelp.passwordErrorMessage, null);
			return resultBean;
		}

		Map<String, Object> resultMap = new HashMap<>();

		List<AllCourse> courseList = allCourseDao.findCourseByTeacherId(phone);
		Teacher teacher = teacherDao.getTeacherByUserId(phone);

		resultMap.put("teacher", teacher);
		resultMap.put("courseList", courseList);

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, resultMap);
		return resultBean;
	}

	/**
	 * 密码找回 逻辑层
	 * 
	 * @param phone
	 * @param newpassword
	 * @return
	 * @throws IOException
	 */
	public Object updatePassword(String accid, String token) throws IOException {

		Response response = UserRequest.getInstance().updatePassword(accid, token);

		String str = response.body().string();
		JSONObject jsonObject = new JSONObject(str);
		int code = jsonObject.getInt("code");

		if (code == 200) {

			User user = userDao.getByPhone(accid);
			user.setUserPassword(token);

			userDao.update(user);
		}

		return str;
	}

	/**
	 * 用戶反饋
	 * 
	 * @param content
	 * @param phone
	 * @return
	 */
	public Object feedback(String userId, String content, String phone, String type) {

		Feedback feedback = new Feedback();

		feedback.setAccid(userId);
		feedback.setContent(content);
		feedback.setPhone(phone);
		feedback.setType(type);
		feedback.setCreateTime(new Date());

		feedbackDao.insert(feedback);

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.thkFeedback, null);
		return resultBean;
	}

	/**
	 * 绑定学生
	 * 
	 * @param userId
	 * @param stuId
	 * @param stuPassword
	 * @return sutdent的基本信息
	 */
	public ResultBean bindStudent(String userId, String stuId, String stuPassword) {

		Student student = studentDao.getByStuId(Long.parseLong(stuId));

		/*
		 * if (student != null) {
		 * resultBean.set(StatusCodeHelp.stuIdHaveBindErro,
		 * StatusCodeHelp.stuIdHaveBindErroMessage, null); return resultBean; }
		 */
		// 调用爬虫将数据爬出来
		// if (stuId.length() >= 3) {

		try {
			/*
			 * String result = "";
			 * 
			 * if (stuId.length() == 11) { result =
			 * JythonUtil.saveStudentInfoByPython(stuId, stuPassword, userId,
			 * "1"); } else if (stuId.length() == 10) {
			 * JythonUtil.saveGraduateStudentInfoByPython(stuId, stuPassword,
			 * userId, "1"); } else {
			 * resultBean.set(StatusCodeHelp.schoolNetTimeOut,
			 * StatusCodeHelp.schoolNetTimeOutMessage, null); return resultBean;
			 * }
			 * 
			 * if (!result.isEmpty()) { // 运行python异常回滚 return huiGun(stuId,
			 * userId); }
			 */

			// 更新用户类型
			updateUserType(userId, 0);

			// 自动建群
			classGroupService.manageClassGroupByStuId(stuId, userId);

		} catch (Exception e) {
			e.printStackTrace();
			// java 异常 回滚
			// return huiGun(stuId, userId);
		}
		// }

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, null);
		return resultBean;
	}

	@Transactional
	public void updateUserType(String userId, int userType) {
		// TODO
		// 更新userType
		User user = new User();

		user.setUserId(userId);
		user.setUserType(userType);

		userDao.updateUserType(user);
	}

	/**
	 * 发现错误回滚
	 * 
	 * @param stuId
	 * @return
	 */
	private ResultBean huiGun(String stuId, String userId) {
		System.out.println("studentInf python exception");
		// 发现异常回滚数据
		updateUserType(userId, -1);
		studentDao.delete(stuId);
		courseDao.deleteByStuId(Long.valueOf(stuId));
		courseScheduleDao.deleteByStuId(Long.valueOf(stuId));
		studentDao.delete(stuId);
		resultBean.set(StatusCodeHelp.schoolNetTimeOut, StatusCodeHelp.schoolNetTimeOutMessage, null);
		return resultBean;
	}

	/**
	 * 绑定老师
	 * 
	 * @param userId
	 * @param teacherId
	 * @return
	 */
	@Transactional
	public ResultBean bindTeacher(String userId, String teacherId) {
		Teacher teacher = teacherDao.getByTeacherId(teacherId);

		if (teacher == null) {
			resultBean.set(StatusCodeHelp.teacherIdError, StatusCodeHelp.teacherIdErrorMessage, null);
			return resultBean;
		} else if (teacher.getUserId() != null) {
			resultBean.set(StatusCodeHelp.teacherIdHaveBindErro, StatusCodeHelp.teacherIdHaveBindErroMessage, null);
			return resultBean;
		}

		teacher.setUserId(userId);
		teacher.setCreateTime(new Date());

		// 更新teacher
		teacherDao.update(teacher);

		// 更新userType
		User user = new User();

		user.setUserId(userId);
		user.setUserType(1);

		userDao.updateUserType(user);

		// 自动建群
		classGroupService.manageClassGroupByteacherId(teacherId, userId);

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, null);
		return resultBean;
	}

	/**
	 * 判断用户类型业务
	 * 
	 * @param accid
	 * @return
	 */
	public ResultBean userInfo(String accid) {

		HashMap<String, String> hs = new HashMap<String, String>();

		User user = userDao.getUserType(accid);

		if (user == null) {
			resultBean.set(StatusCodeHelp.notFoundUser, StatusCodeHelp.notFoundUserMeaage, null);
			return resultBean;
		}

		int userType = user.getUserType();

		if (userType == 0) {
			Long stuId = studentDao.getStuIdByUserId(accid);

			hs.put("userType", String.valueOf(userType));
			hs.put("schoolId", String.valueOf(stuId));

			resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, hs);
			return resultBean;
		}

		if (userType == 1) {
			String teacherId = teacherDao.getTeacherIdByUserId(accid);

			hs.put("userType", String.valueOf(userType));
			hs.put("schoolId", teacherId);

			resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, hs);
			return resultBean;
		}

		hs.put("userType", String.valueOf(userType));
		hs.put("schoolId", null);

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, hs);
		return resultBean;
	}

	/**
	 * 绑定极光Id
	 * 
	 * @param userId
	 * @param jpushId
	 * @return
	 */
	public ResultBean bingJpushId(String userId, String jpushId) {
		userDao.updateJpushId(userId, jpushId);

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, null);
		return resultBean;
	}

	/**
	 * 解除绑定极光Id
	 * 
	 * @param userId
	 * @param jpushId
	 * @return
	 */
	public ResultBean removeBindJpush(String userId) {
		userDao.updateJpushId(userId, null);

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, null);
		return resultBean;
	}

}
