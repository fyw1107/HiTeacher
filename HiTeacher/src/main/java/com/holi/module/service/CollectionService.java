package com.holi.module.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.holi.common.help.StatusCodeHelp;
import com.holi.common.resultBean.CollectionBean;
import com.holi.common.resultBean.ResultBean;
import com.holi.common.util.StringUtil;
import com.holi.module.dao.CollectionDao;

/**
 * 新闻收藏Service
 * 
 * @author IVAN
 *
 */
@Service
public class CollectionService {

	@Autowired
	private CollectionDao collectionDao;

	@Autowired
	private ResultBean resultBean;

	/**
	 * 添加收藏
	 * 
	 * @param newsId
	 * @param stuId
	 * @param teacherId
	 * @return
	 */
	public ResultBean addCollection(int newsId, long stuId, int teacherId) {
		int count = collectionDao.insert(newsId, stuId, teacherId);
		if (count > 0)
			resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, null);
		else
			resultBean.set(StatusCodeHelp.notFoundResource, StatusCodeHelp.notFoundResourceMessage, null);
		return resultBean;
	}

	/**
	 * 删除收藏
	 * 
	 * @param collectionId
	 * @return
	 */
	public ResultBean deleteCollection(int collectionId) {
		int count = collectionDao.delete(collectionId);
		if (count > 0)
			resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, null);
		else
			resultBean.set(StatusCodeHelp.notFoundResource, StatusCodeHelp.notFoundResourceMessage, null);
		return resultBean;
	}

	/**
	 * 查看我的收藏
	 * 
	 * @param stuId
	 * @param teacherId
	 * @return
	 */
	public ResultBean listMyCollection(long stuId, int teacherId) {

		List<CollectionBean> list = collectionDao.listMyCollection(stuId, teacherId);
		List<CollectionBean> newList = HandleData(list);
		if (list.size() > 0)
			resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, newList);
		else
			resultBean.set(StatusCodeHelp.notFoundResource, StatusCodeHelp.notFoundResourceMessage, newList);
		return resultBean;
	}

	/**
	 * 处理从数据库查出的数据
	 * 
	 * @param newsList
	 * @return
	 */
	private List<CollectionBean> HandleData(List<CollectionBean> collectionList) {

		List<CollectionBean> reslutList = new ArrayList<>();

		for (CollectionBean collectionBean : collectionList) {
			// 用于列表页面缩略显示新闻内容
			collectionBean.setContent(StringUtil.subString(collectionBean.getContent(), 0,
					(int) (collectionBean.getContent().length() * 0.1)));
			collectionBean.setPicsrc(subPicsrc(collectionBean.getPicsrc()));
			reslutList.add(collectionBean);
		}
		return reslutList;
	}

	/**
	 * 处理图片路径,只取第一个,用于列表页面新闻图片显示
	 * 
	 * @param str
	 * @return
	 */
	private String subPicsrc(String str) {

		if (str != null) {
			str = str.replace("[", "").replace("]", "").replaceAll("u'", "").replaceAll("'", "").replaceAll(" ", "");
			String[] list = str.split(",");
			str = list[0];
		}
		return str;
	}

}
