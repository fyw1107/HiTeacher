package com.holi.module.entity;

import com.holi.common.entity.BaseEntity;

public class MyMessage extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private String stuId;
	private String messageType;
	private String content;

	public String getStuId() {
		return stuId;
	}

	public void setStuId(String stuId) {
		this.stuId = stuId;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
