/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.holi.module.entity;

import java.io.Serializable;

/**
 * 学生Entity
 * 
 * @author lz
 * @version 2016-12-16
 */
public class Student implements Serializable {

	private static final long serialVersionUID = 1L;

	private String userId; // 用戶ID
	private Long stuId; // 学号
	private String stuPassword; // 教务处密码
	private String stuName; // 真实名字
	private int schoolId; // 学校
	private int xueyuan; // 学院
	private String xueyuanName; // 学院名字
	private String major; // 导师
	private String advisor; // 专业
	private double signRate;// 签到率

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getStuPassword() {
		return stuPassword;
	}

	public void setStuPassword(String stuPassword) {
		this.stuPassword = stuPassword;
	}

	public String getStuName() {
		return stuName;
	}

	public void setStuName(String stuName) {
		this.stuName = stuName;
	}

	public int getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}

	public int getXueyuan() {
		return xueyuan;
	}

	public void setXueyuan(int xueyuan) {
		this.xueyuan = xueyuan;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getAdvisor() {
		return advisor;
	}

	public void setAdvisor(String advisor) {
		this.advisor = advisor;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof Student) {
			Student anotherSignCache = (Student) obj;
			if (this.getStuId() == anotherSignCache.getStuId())
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		// TODO
		String s = this.getStuId().toString();
		if (s.length() > 10) {
			s = s.substring(s.length() - 9, s.length());
			return Integer.valueOf(s);
		} else {
			return Integer.valueOf(s);
		}
	}

	public double getSignRate() {
		return signRate;
	}

	public void setSignRate(double signRate) {
		this.signRate = signRate;
	}

	public Long getStuId() {
		return stuId;
	}

	public void setStuId(Long stuId) {
		this.stuId = stuId;
	}

	public String getXueyuanName() {
		return xueyuanName;
	}

	public void setXueyuanName(String xueyuanName) {
		this.xueyuanName = xueyuanName;
	}

}