/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.holi.module.entity;


import java.util.Date;
import com.holi.common.entity.BaseEntity;

/**
 * 评论Entity
 * @author lz
 * @version 2016-12-16
 */
public class Comment extends BaseEntity {
	
	private static final long serialVersionUID = 1L;
	private String topicId;		// 话题id
	private String fromUid;		// 评论用户ID
	private String toUid;		// to_uid
	private String content;		// 评论内容
	private String voice;		// 语音回复
	private Date time;		// time
	
	public Comment() { 
		super();
	}

	public String getTopicId() {
		return topicId;
	}

	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}

	public String getFromUid() {
		return fromUid;
	}

	public void setFromUid(String fromUid) {
		this.fromUid = fromUid;
	}

	public String getToUid() {
		return toUid;
	}

	public void setToUid(String toUid) {
		this.toUid = toUid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getVoice() {
		return voice;
	}

	public void setVoice(String voice) {
		this.voice = voice;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

}