package com.holi.module.entity;

import com.holi.common.entity.BaseEntity;

/**
 * 用来存储从学生端传来的数据
 * 
 * @author lz
 *
 */
public class SignCache extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private Long stuId; // 学生ID
	private String courseId; // 课程ID
	private String classNumber;// 分班号
	private int signFlag; // 签到标志位
	private String teacherId; // 教师ID
	private String wifiName; // 热点名称
	private int isWifiCreater; // 默认为"0"不是热点的开启者，"1"代表热点的开启者

	private Student student;

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getWifiName() {
		return wifiName;
	}

	public void setWifiName(String wifiName) {
		this.wifiName = wifiName;
	}

	public int getIsWifiCreater() {
		return isWifiCreater;
	}

	public void setIsWifiCreater(int isWifiCreater) {
		this.isWifiCreater = isWifiCreater;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof SignCache) {
			SignCache anotherSignCache = (SignCache) obj;
			if (this.student.getStuId() == anotherSignCache.student.getStuId())
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		// TODO
		String s = this.student.getStuId().toString();
		if (s.length() > 10) {
			s = s.substring(s.length() - 9, s.length());
			return Integer.valueOf(s);
		} else {
			return Integer.valueOf(s);
		}
	}

	public Long getStuId() {
		return stuId;
	}

	public void setStuId(Long stuId) {
		this.stuId = stuId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public int getSignFlag() {
		return signFlag;
	}

	public void setSignFlag(int signFlag) {
		this.signFlag = signFlag;
	}

	public String getClassNumber() {
		return classNumber;
	}

	public void setClassNumber(String classNumber) {
		this.classNumber = classNumber;
	}

}
