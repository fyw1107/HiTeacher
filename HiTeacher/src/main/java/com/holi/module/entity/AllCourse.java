package com.holi.module.entity;

import java.io.Serializable;

public class AllCourse implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String courseId;
	private String courseName;
	
	public String getCourseId() {
		return courseId;
	}
	
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	
	public String getCourseName() {
		return courseName;
	}
	
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
