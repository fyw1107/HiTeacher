package com.holi.module.entity;

import com.holi.common.entity.BaseEntity;

public class Friends extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private String accid; // 发起者accid
	private String faccid; // 要修改朋友的accid
	private String alias; // 给好友增加备注名，限制长度128
	private String ex; // 修改ex字段，限制长度256
	private int type; // 1直接加好友，2请求加好友，3同意加好友，4拒绝加好友
	private String msg; // 加好友对应的请求消息，第三方组装，最长256字符

	public String getAccid() {
		return accid;
	}

	public void setAccid(String accid) {
		this.accid = accid;
	}

	public String getFaccid() {
		return faccid;
	}

	public void setFaccid(String faccid) {
		this.faccid = faccid;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getEx() {
		return ex;
	}

	public void setEx(String ex) {
		this.ex = ex;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
