package com.holi.module.entity;

import java.io.Serializable;
import java.util.Date;

import com.holi.common.entity.BaseEntity;

public class ClassGroup implements Serializable{

	private static final long serialVersionUID = 1L;

	private String tid; // 群id  
	private String tname; // 群名称
	private String owner; // 群主用户帐号
	private String members; // 群成员
	private String announcement; // 群公告
	private String intro; // 群描述
	private String msg; // 邀请发的文字
	/*
	 * 管理后台建群时， 0不需要被邀请人同意加入群， 1需要被邀请人同意才可以加入群。
	 */
	private int magree;
	/*
	 * 群建好后，sdk操作时，0不用验证，1需要验证, 2不允许任何人加入
	 */
	private int joinmode;
	/*
	 * 自定义高级群扩展属性， 第三方可以跟据此属性自定义扩展自己的群属性。 （建议为json）
	 */
	private String custom;
	private String icon; // 头像路径
	private int beinvitemode; // 被邀请人同意方式，0-需要同意(默认),1-不需要同意
	private int invitemode; // 谁可以邀请他人入群，0-管理员(默认),1-所有人。
	private int uptinfomode; // 谁可以修改群资料，0-管理员(默认),1-所有人
	private int upcustommode; // 谁可以更新群自定义属性，0-管理员(默认),1-所有人
	private Date createTime; // 创建时间

	public String getTname() {
		return tname;
	}

	public void setTname(String tname) {
		this.tname = tname;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getMembers() {
		return members;
	}

	public void setMembers(String members) {
		this.members = members;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getMagree() {
		return magree;
	}

	public void setMagree(int magree) {
		this.magree = magree;
	}

	public int getJoinmode() {
		return joinmode;
	}

	public void setJoinmode(int joinmode) {
		this.joinmode = joinmode;
	}

	public String getCustom() {
		return custom;
	}

	public void setCustom(String custom) {
		this.custom = custom;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getBeinvitemode() {
		return beinvitemode;
	}

	public void setBeinvitemode(int beinvitemode) {
		this.beinvitemode = beinvitemode;
	}

	public int getInvitemode() {
		return invitemode;
	}

	public void setInvitemode(int invitemode) {
		this.invitemode = invitemode;
	}

	public int getUptinfomode() {
		return uptinfomode;
	}

	public void setUptinfomode(int uptinfomode) {
		this.uptinfomode = uptinfomode;
	}

	public int getUpcustommode() {
		return upcustommode;
	}

	public void setUpcustommode(int upcustommode) {
		this.upcustommode = upcustommode;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getAnnouncement() {
		return announcement;
	}

	public void setAnnouncement(String announcement) {
		this.announcement = announcement;
	}

}
