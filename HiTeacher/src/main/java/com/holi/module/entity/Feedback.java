package com.holi.module.entity;

import com.holi.common.entity.BaseEntity;

public class Feedback extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private String accid; // 用户ID
	private String content; // 反馈的内容
	private String phone; // 联系方式
	private String type; // 反馈类型

	public String getAccid() {
		return accid;
	}

	public void setAccid(String accid) {
		this.accid = accid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
