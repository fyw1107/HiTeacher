package com.holi.module.entity;

import com.holi.common.entity.BaseEntity;

/**
 * 签到实体类
 * 
 * @author lz
 *
 */
public class SignIn extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private Long stuId; // 学生ID
	private String teacherId; // 教师ID
	private String courseId; // 课程ID
	private String classNumber;// 分班号
	private int signFlag; // 签到标志位，"1"已到,“0”未到

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public Long getStuId() {
		return stuId;
	}

	public void setStuId(Long stuId) {
		this.stuId = stuId;
	}

	public int getSignFlag() {
		return signFlag;
	}

	public void setSignFlag(int signFlag) {
		this.signFlag = signFlag;
	}

	public String getClassNumber() {
		return classNumber;
	}

	public void setClassNumber(String classNumber) {
		this.classNumber = classNumber;
	}

}
