/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.holi.module.entity;

import java.util.Date;

import com.holi.common.entity.BaseEntity;

/**
 * 话题Entity
 * @author lz
 * @version 2016-12-16   
 */
public class Topic extends BaseEntity {
	
	private static final long serialVersionUID = 1L;
	private User userId;		// 发帖人id
	private Date dateline;		// 发帖时间
	private String message;		// message
	private String image;		// 图片路径
	private Long invisible;		// 是否通过审核 0正常，1未通过审核，-1回收站，-2 等待审核 -3 忽略不显示/草稿 -5 回收站回帖
	private Integer anonymous;		// 是否匿名贴子 1 匿名 0 正常
	private String tags;		// 标签
	private Date createTime;		// 发帖时间
	
	public Topic() {
		super();
	}

	public User getUserId() {
		return userId;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}

	public Date getDateline() {
		return dateline;
	}

	public void setDateline(Date dateline) {
		this.dateline = dateline;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Long getInvisible() {
		return invisible;
	}

	public void setInvisible(Long invisible) {
		this.invisible = invisible;
	}

	public Integer getAnonymous() {
		return anonymous;
	}

	public void setAnonymous(Integer anonymous) {
		this.anonymous = anonymous;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}