package com.holi.module.entity;

import java.io.Serializable;
import java.util.Date;

public class Teacher implements Serializable {

	private static final long serialVersionUID = 1L;

	private String teacherId;
	private String userId;
	private String teacherName;
	private Date createTime;

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
