package com.holi.module.entity;

import java.util.Date;

/**
 * 作业统计记录实体
 * 
 * @author IVAN
 *
 */
public class HomeworkRecord {

	private static final long serialVersionUID = -7269885661678273626L;

	private Integer id;
	private String courseId;
	private String courseName;
	private String classnumber;
	private String teacherId;
	private Date assignTime;

	public HomeworkRecord(String courseId, String courseName, String classnumber, String teacherId2, Date assignTime) {
		super();
		this.courseId = courseId;
		this.courseName = courseName;
		this.classnumber = classnumber;
		this.teacherId = teacherId2;
		this.assignTime = assignTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getClassnumber() {
		return classnumber;
	}

	public void setClassnumber(String classnumber) {
		this.classnumber = classnumber;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public Date getAssignTime() {
		return assignTime;
	}

	public void setAssignTime(Date assignTime) {
		this.assignTime = assignTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
