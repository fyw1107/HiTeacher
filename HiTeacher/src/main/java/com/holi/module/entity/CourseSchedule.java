package com.holi.module.entity;

/**
 * 我的课表(显示整张课表)
 * @author IVAN
 * `id` int(11) NOT NULL auto_increment,
  `stu_id` int(64) unsigned NOT NULL COMMENT '学生id外键',
  `weekday` tinyint(4) NOT NULL COMMENT '星期号（0周日，1周一，2周二...）',
  `one` varchar(100) default NULL COMMENT '1,2节次',
  `three` varchar(100) default NULL COMMENT '3,4节次',
  `five` varchar(100) default NULL COMMENT '5,6节次',
  `seven` varchar(100) default NULL COMMENT '7,8节次',
  `nine` varchar(100) default NULL COMMENT '9,10节次',
 */
public class CourseSchedule {

	private int id;				//课表记录id
	private int weekday;		//星期
	private String one;			//第1,2节
	private String three;		//第3,4节
	private String five;		//第5,6节
	private String seven;		//第7,8节
	private String nine;		//第9,10节
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getWeekday() {
		return weekday;
	}
	public void setWeekday(int weekday) {
		this.weekday = weekday;
	}
	public String getOne() {
		return one;
	}
	public void setOne(String one) {
		this.one = one;
	}
	public String getThree() {
		return three;
	}
	public void setThree(String three) {
		this.three = three;
	}
	public String getFive() {
		return five;
	}
	public void setFive(String five) {
		this.five = five;
	}
	public String getSeven() {
		return seven;
	}
	public void setSeven(String seven) {
		this.seven = seven;
	}
	public String getNine() {
		return nine;
	}
	public void setNine(String nine) {
		this.nine = nine;
	}
}
