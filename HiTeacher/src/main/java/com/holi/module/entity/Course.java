package com.holi.module.entity;

import com.holi.common.entity.BaseEntity;

/**
 * 课程Entity
 * 
 * @author lz
 * @version 2016-12-16
 */
public class Course extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private Long stuId; // 学号
	private String courseId; // 课程编号
	private String courseName; // 课程名字
	private String credit; // 学分
	private Integer degree; // 1 表示是学位课 0 表示非学位课
	private Integer term; // 上课学期 如 20160 最后一个数字0表示上学期 1 表示下学期
	private String teacherName; // 上课老师
	private String campus; // 上课校区
	private String classroom; // 上课地点
	private String courseDay; // 星期
	private String courseTime; // 节次
	private String courseWeek; // 周次
	private String classnumber; // 分班号
	private String teacherId; // 老师Id

	private double signRate; // 用于签到统计
	private String classGroupId; // 对应的云信群ID

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public Integer getDegree() {
		return degree;
	}

	public void setDegree(Integer degree) {
		this.degree = degree;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getCampus() {
		return campus;
	}

	public void setCampus(String campus) {
		this.campus = campus;
	}

	public String getClassroom() {
		return classroom;
	}

	public void setClassroom(String classroom) {
		this.classroom = classroom;
	}

	public String getCourseDay() {
		return courseDay;
	}

	public void setCourseDay(String courseDay) {
		this.courseDay = courseDay;
	}

	public String getCourseTime() {
		return courseTime;
	}

	public void setCourseTime(String courseTime) {
		this.courseTime = courseTime;
	}

	public String getCourseWeek() {
		return courseWeek;
	}

	public void setCourseWeek(String courseWeek) {
		this.courseWeek = courseWeek;
	}

	public String getClassnumber() {
		return classnumber;
	}

	public void setClassnumber(String classnumber) {
		this.classnumber = classnumber;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public double getSignRate() {
		return signRate;
	}

	public void setSignRate(double signRate) {
		this.signRate = signRate;
	}

	public String getClassGroupId() {
		return classGroupId;
	}

	public void setClassGroupId(String classGroupId) {
		this.classGroupId = classGroupId;
	}

	public Long getStuId() {
		return stuId;
	}

	public void setStuId(Long stuId) {
		this.stuId = stuId;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

}
