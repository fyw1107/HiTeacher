package com.holi.module.entity;

import com.holi.common.entity.BaseEntity;

public class User extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private String userId; // 登陆电话
	private String jpushId; //极光推送ID
	private String userPassword; // 登陆密码
	private String nickName; // 昵称
	private String picture; // 头像图片路劲
	private String sign; // 参数teacherId或者studentId进行从小到大排序加上时间戳再进行MD5（32bit）加密（由客户端完成）
	private int userType; //0 为学生  1 为老师 -1 身份不明
	// TODO

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public String getJpushId() {
		return jpushId;
	}

	public void setJpushId(String jpushId) {
		this.jpushId = jpushId;
	}
	
}
