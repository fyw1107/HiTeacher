package com.holi.module.entity;

import java.util.Date;

import com.holi.common.entity.BaseEntity;

/**
 * 作业详细记录实体类
 * 
 * @author IVAN
 *
 */
public class Homework {

	private static final long serialVersionUID = -7269885661678273626L;

	private Integer id;
	private String courseId;
	private String courseName;
	private String classnumber;
	private String teacherId;
	private Long stuId;
	private String hwContent; // 教师布置的作业内容
	private String hwPicture; // 学生提交的作业图片url
	private Date assignTime;
	private Date commitTime;
	private String hwScore;
	private Integer recordId;

	public Homework() {
	}

	public Homework(String courseId, String courseName, String classnumber, String teacherId, Long stuId,
			String hwContent, Date assignTime, Integer recordId) {
		super();
		this.courseId = courseId;
		this.courseName = courseName;
		this.classnumber = classnumber;
		this.teacherId = teacherId;
		this.stuId = stuId;
		this.hwContent = hwContent;
		this.assignTime = assignTime;
		this.recordId = recordId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getClassnumber() {
		return classnumber;
	}

	public void setClassnumber(String classnumber) {
		this.classnumber = classnumber;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public Long getStuId() {
		return stuId;
	}

	public void setStuId(Long stuId) {
		this.stuId = stuId;
	}

	public String getHwContent() {
		return hwContent;
	}

	public void setHwContent(String hwContent) {
		this.hwContent = hwContent;
	}

	public String getHwPicture() {
		return hwPicture;
	}

	public void setHwPicture(String hwPicture) {
		this.hwPicture = hwPicture;
	}

	public Date getAssignTime() {
		return assignTime;
	}

	public void setAssignTime(Date assignTime) {
		this.assignTime = assignTime;
	}

	public Date getCommitTime() {
		return commitTime;
	}

	public void setCommitTime(Date commitTime) {
		this.commitTime = commitTime;
	}

	public String getHwScore() {
		return hwScore;
	}

	public void setHwScore(String hwScore) {
		this.hwScore = hwScore;
	}

	public Integer getRecordId() {
		return recordId;
	}

	public void setRecordId(Integer recordId) {
		this.recordId = recordId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Homework [id=" + id + ", courseId=" + courseId + ", courseName=" + courseName + ", classnumber="
				+ classnumber + ", teacherId=" + teacherId + ", stuId=" + stuId + ", hwContent=" + hwContent
				+ ", hwPicture=" + hwPicture + ", assignTime=" + assignTime + ", commitTime=" + commitTime
				+ ", hwScore=" + hwScore + ", recordId=" + recordId + "]";
	}
}
