package com.holi.module.dao;

import java.util.List;
import java.util.Map;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.common.resultBean.CourseInfoBean;
import com.holi.common.resultBean.HomeworkInfoBean;
import com.holi.common.resultBean.AllHomeworkInfoBean;
import com.holi.module.entity.Homework;
import com.holi.module.entity.HomeworkRecord;

/**
 * 作业DAO
 * @author IVAN
 *
 */
@Dao
public interface HomeworkDao extends BaseDao<Homework> {

	/**
	 * 批量插入详细作业记录
	 * @param map
	 * @return
	 */
	int insertByBatch(Map<String, Object> map);

	/**
	 * 学生获取未提交的作业
	 * @param stuId
	 * @return
	 */
	List<Homework> getUncommitedHomeworkByStuId(long stuId);
	
	/**
	 * 学生获取已提交的作业
	 * @param stuId
	 * @return
	 */
	List<Homework> listAllCommitedHomework(long stuId);
	
	/**
	 * 学生提交作业,更新作业记录
	 * @param map
	 * @return
	 */
	int updateByStudent(Map<String, Object> map);

	/**
	 * 教师批阅作业,更新评分
	 * @param map
	 * @return
	 */
	int updateHomeworkScore(Map<String, Object> map);

	/**
	 * 插入作业统计记录
	 * @param map
	 * @return
	 */
	int insertHomeworkRecord(HomeworkRecord hoRecord);

	/**
	 * 根据教师id获取布置的作业记录
	 * @param teacherId
	 * @return
	 */
	List<HomeworkRecord> listHomeworkByTeacherId(String teacherId);
	
	/**
	 * 根据作业统计记录id获取未批阅的作业记录
	 * @param id
	 * @return
	 */
	List<Homework> listUncheckedHomework(int homeworkRecordId);
	
	/**
	 * 根据作业统计记录id获取未提交作业的学生信息(教师端)
	 * @param homeworkRecordId
	 * @return
	 */
	List<AllHomeworkInfoBean> listUncommitedStudentInfo(int homeworkRecordId);

	/**
	 * 根据作业统计记录id获取本次作业的应提交人数
	 * @param homeworkRecordId
	 * @return
	 */
	List<AllHomeworkInfoBean> listTotalHomework(int homeworkRecordId);

	/**
	 * 查询一次作业记录(学生端)
	 * @param map
	 * @return
	 */
	Homework getHomeworkByStudent(Map<String, Object> map);
	
	/**
	 * 通过作业id查询作业
	 * @param homeworkId
	 * @return
	 */
	Homework getHomeworkById(Integer homeworkId);

	/**
	 * 查询教师所有课程的简略信息(网页端)
	 * @param teacherId
	 * @return
	 */
	List<CourseInfoBean> listTeacherCourseInfo(String teacherId);
	
	/**
	 * 查询教师所有课程的简略信息(安卓端)
	 * @param teacherId
	 * @return
	 */
	List<CourseInfoBean> listCourseInfo(String teacherId);

	/**
	 * 根据教师id和课程id获取已布置作业的班级(班号)集合
	 * @param map
	 * @return
	 */
	List<String> getClassnumbersByTeacherAndCourse(Map<String, Object> map);

	/**
	 * 根据课程id和班号查询该课程该班布置的所有作业
	 * @param map
	 * @return
	 */
	List<HomeworkInfoBean> getHomeworkNumberByCourseAndClassnumber(
			Map<String, Object> map);

}
