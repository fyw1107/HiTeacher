package com.holi.module.dao;

import java.util.List;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.module.entity.AllCourse;

@Dao
public interface AllCourseDao extends BaseDao<AllCourse> {

	List<AllCourse> findCourseByTeacherId(String teacherId);
	
}
