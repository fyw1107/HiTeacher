package com.holi.module.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.module.entity.SignIn;

/**
 * 签到Dao接口
 * 
 * @author lz
 *
 */
@Dao
public interface SignInDao extends BaseDao<SignIn> {

	/**
	 * 得到某个学生在某个课程的签到数据
	 * 
	 * @param integer
	 * @param courseId
	 * @return
	 */
	List<SignIn> findSignDataByStuIdAndCourserId(@Param("stuId") Long stuId, @Param("courseId") String courseId);

	/**
	 * 得到某个课程的签到数据
	 * 
	 * @param teacherId
	 * @param courseId
	 * @return
	 */
	List<SignIn> findSignDataByCourserId(@Param("courseId") String courseId);

	/**
	 * 批量插入签到数据
	 * 
	 * @param signInList
	 */
	void insertSignData(@Param("signInList") List<SignIn> signInList);

}
