package com.holi.module.dao;

import org.apache.ibatis.annotations.Param;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.module.entity.User;

@Dao
public interface UserDao extends BaseDao<User> {

	public User getByPhone(String userId);

	public User getUserType(String userId);

	public void updateUserType(User user);

	public void updateJpushId(@Param("userId") String userId, @Param("jpushId") String jpushId);

	/**
	 * 通过用户ID获取与其绑定的极光ID
	 * 
	 * @param accid
	 * @return
	 */
	public String getjpushIdByStuId(Long stuId);
}
