package com.holi.module.dao;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.module.entity.Feedback;

@Dao
public interface FeedbackDao extends BaseDao<Feedback>{
	
}
