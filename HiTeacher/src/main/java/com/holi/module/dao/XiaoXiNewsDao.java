package com.holi.module.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.module.entity.XiaoXiNews;

/**
 * 校息Dao接口类

 * @author lz
 */
@Dao
public interface XiaoXiNewsDao extends BaseDao<XiaoXiNews> {

	public List<XiaoXiNews> pullDown(@Param("schoolId")int schoolId, @Param("collegeId")int collegeId);

	public List<XiaoXiNews> pullUp(@Param("schoolId") int schoolId, @Param("collegeId")int collegeId, @Param("datetime") String datetime);
}