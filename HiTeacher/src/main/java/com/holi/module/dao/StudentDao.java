/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.holi.module.dao;

import java.util.List;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.module.entity.Student;

/**
 * 学生DAO接口
 * 
 * @author lz
 * @version 2016-12-16
 */
@Dao
public interface StudentDao extends BaseDao<Student> {

	/**
	 * 通过教师ID和系统时间查出所有上课的学生
	 */
	public List<Student> getStuByTcherId(String teacherId, long nowTime);

	/**
	 * 通过学生ID获取与其绑定的用户ID
	 * 
	 * @param sutId
	 * @return
	 */
	public String getUserIdByStuId(Long sutId);

	/**
	 * 通过用户ID获取与其绑定过的学生ID
	 * 
	 * @param accid
	 * @return
	 */
	public Long getStuIdByUserId(String userId);

	/**
	 * 根据courseId获取上该节课的所有的学生
	 * 
	 * @param courseId
	 * @return
	 */
	public List<Student> findStuByCourseId(String courseId);

	public Student getByStuId(Long stuId);

	public void deleteByStuId(String stuId);

}