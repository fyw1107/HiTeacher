package com.holi.module.dao;

import com.holi.common.dao.BaseDao;
import com.holi.module.entity.MyMessage;

public interface MyMessageDao extends BaseDao<MyMessage> {

}
