
package com.holi.module.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.common.resultBean.CourseByStuIdBean;
import com.holi.module.entity.Course;

/**
 * 课程DAO接口
 * 
 * @author lz
 * @version 2016-12-16
 */
@Dao
public interface CourseDao extends BaseDao<Course> {

	public List<Course> getAllCourse();

	public List<Course> listCourses(Map<String, Object> map);

	public List<Course> findCourseByStuId(@Param("stuId") Long stuId);

	public int insertCourseAndClassGroupRelation(@Param("courseId") String courseId, @Param("tid") String tid);

	public List<Course> findCourseByTeacherId(String teacherId);

	public void deleteByStuId(Long valueOf);

	public String getCourseIdBycourseName(String courseName);

	public List<Course> findByCourseId(String courseId);

	public Course getCourseBycourseIdAndClassNumber(@Param("courseId") String courseId,
			@Param("classNumber") String classNumber);

	public List<Long> getStudentByCourseIdAndClassnumber(@Param("courseId") String courseId,
			@Param("classnumber") String classnumber);

}