package com.holi.module.dao;

import java.util.List;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.module.entity.Teacher;

@Dao
public interface TeacherDao extends BaseDao<Teacher> {

	/**
	 * 获取上某节课的老师信息
	 * @param courseId
	 * @return
	 */
	public List<Teacher> findTeacherByCourseId(String courseId);

	public String getTeacherIdByUserId(String userId);

	public Teacher getTeacherByUserId(String userId);

	public String getTeacherByCourseId(String courseId);

	public Teacher getByTeacherId(String teacherId);
	
}
