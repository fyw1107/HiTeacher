package com.holi.module.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.common.resultBean.CourseScheduleBean;
import com.holi.module.entity.CourseSchedule;

/**
 * 我的课表Dao
 * 
 * @author IVAN
 *
 */
@Dao
public interface CourseScheduleDao extends BaseDao<CourseSchedule> {

	/**
	 * 根据学生id查询该学生的课程表信息
	 * 
	 * @param stuid
	 * @return
	 */
	public List<CourseScheduleBean> listCourseSchedule(@Param("stuId") Long stuId);

	public void deleteByStuId(Long valueOf);
}
