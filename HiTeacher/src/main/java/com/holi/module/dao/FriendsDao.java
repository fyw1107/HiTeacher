package com.holi.module.dao;

import org.apache.ibatis.annotations.Param;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.module.entity.Friends;

/**
 * 校息Dao接口类
 * 
 * @author lz
 */
@Dao
public interface FriendsDao extends BaseDao<Friends> {
	public void deleteFriend(@Param("accid") String accid, @Param("faccid") String faccid);
}
