package com.holi.module.dao;


import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.module.entity.Topic;

/**
 * 话题DAO接口
 * @author lz
 * @version 2016-12-16
 */
@Dao
public interface TopicDao extends BaseDao<Topic>{
	
}