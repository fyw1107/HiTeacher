package com.holi.module.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.module.entity.SignCache;

@Dao
public interface SignCacheDao extends BaseDao<SignCache> {
	/**
	 * 取出创建的热点
	 * 
	 * @param teacherId
	 * @param wifiCreater
	 * @return
	 */
	public List<SignCache> findAllCreateWifi(@Param("teacherId") String teacherId,
			@Param("isWifiCreater") int isWifiCreater);

	/**
	 * 根据wifi名取出，连接此wifi名的学生ID
	 * 
	 * @param wifiName
	 * @param teacherId
	 * @return
	 */
	public List<SignCache> findStuIdBywifiName(@Param("wifiName") String wifiName,
			@Param("teacherId") String teacherId);

	/**
	 * 清空表
	 */
	public void deleteAll();

	/**
	 * 找出某个课程的数据
	 * 
	 * @param courseId
	 * @return
	 */
	public List<SignCache> findSignDataByCourserId(@Param("courseId") String courseId,
			@Param("classNumber") String classNumber);

	/**
	 * 通过techerId获取最新的一条数据
	 * 
	 * @param teacherId
	 * @return
	 */
	public SignCache getRecentDataByTeacherId(String teacherId);

	/**
	 * 签到结束清除缓存
	 * 
	 * @param courseId
	 * @param classNumber
	 * @return
	 */
	public int deleteByTeacherId(String teacherId);
}
