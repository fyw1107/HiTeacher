package com.holi.module.dao;

import org.apache.ibatis.annotations.Param;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.module.entity.ClassGroup;

@Dao
public interface ClassGroupDao extends BaseDao<ClassGroup> {

	/**
	 * 通过课程ID获取班级群信息
	 * 
	 * @param courseId
	 * @return
	 */
	public ClassGroup getClassGroupByCourseId(String courseId);

	/**
	 * 更新群主
	 * 
	 * @param newowner
	 */
	public void updataOwner(@Param("tid") String tid, @Param("newOwner") String newowner);

}
