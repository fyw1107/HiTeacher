package com.holi.module.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.common.resultBean.CollectionBean;
import com.holi.module.entity.NewsCollection;

/**
 * 新闻收藏DAO
 * @author IVAN
 *
 */
@Dao
public interface CollectionDao extends BaseDao<NewsCollection> {

	/**
	 * 添加收藏
	 * @param newsId
	 * @param stuId
	 * @param teacherId
	 * @return
	 */
	public int insert(@Param("newsId")int newsId, @Param("stuId")long stuId, @Param("teacherId")int teacherId);
	
	/**
	 * 删除收藏(收藏记录的id通过显示我的收藏时随新闻内容一起返回客户端)
	 * @param id
	 * @return
	 */
	public int delete(@Param("collectionId")int collectionId);
	
	/**
	 * 显示我的收藏
	 * @param stuId
	 * @param teacherId
	 * @return
	 */
	public List<CollectionBean> listMyCollection(@Param("stuId")long stuId, @Param("teacherId")int teacherId);
}
