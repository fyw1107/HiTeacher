/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.holi.module.dao;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.module.entity.Comment;

/**
 * 评论DAO接口
 * @author lz
 * @version 2016-12-16
 */
@Dao
public interface CommentDao extends BaseDao<Comment> {
	
}