package com.holi.module.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 学生控制层
 * 
 * @author lz
 *
 */
@Controller
@RequestMapping(value = "student")
public class StudentController {
	
}
