package com.holi.module.web;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.holi.common.help.StatusCodeHelp;
import com.holi.common.resultBean.ResultBean;
import com.holi.module.service.OcrService;

/**
 * OCR控制层
 * 
 * @author lz
 *
 */
@Controller
@RequestMapping(value = "/ocr")
public class OcrCollection {

	@Autowired
	private OcrService ocrService;

	@Autowired
	private ResultBean resultBean;

	/**
	 * 一卡通识别
	 * 
	 * @param oneCardImages
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/oneCard")
	public ResultBean oneCard(@RequestParam("oneCardImages") MultipartFile[] oneCardImages, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		// 判断是否有图片传来
		if (oneCardImages == null || oneCardImages.length == 0) {
			resultBean.set(StatusCodeHelp.parameterError, StatusCodeHelp.parameterErrorMessage, null);
			return resultBean;
		}

		try {
			return ocrService.oneCard(oneCardImages);
		} catch (Exception e) {
			e.printStackTrace();
			resultBean.set(StatusCodeHelp.OCRoneCradError, StatusCodeHelp.OCRoneCradErrorMessage, null);
			return resultBean;
		}

	}
}
