package com.holi.module.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.holi.common.help.StatusCodeHelp;
import com.holi.common.jsonBean.CheckInMsg;
import com.holi.common.jsonBean.StudentSignInMsg;
import com.holi.common.resultBean.ResultBean;
import com.holi.module.service.SignInService;

/**
 * 签到控制层
 * 
 * @author lz
 *
 */
@Controller
@RequestMapping(value = "/signIn")
public class SignInController {

	@Autowired
	private SignInService signInService;

	@Autowired
	private ResultBean resultBean;

	/**
	 * 学生端上传签到数据
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/uploadSignMsg")
	public ResultBean uploadSignMsg(HttpServletRequest request, HttpServletResponse response) {
		String checkInMsg = request.getParameter("checkInMsg");
		System.out.println(checkInMsg);
		if (checkInMsg == null || checkInMsg.equals("")) {
			resultBean.set(StatusCodeHelp.parameterError, StatusCodeHelp.parameterErrorMessage, null);
			return resultBean;
		}

		Gson gson = new Gson();
		CheckInMsg sigMsg = gson.fromJson(checkInMsg, CheckInMsg.class);

		return signInService.uploadSignMsg(sigMsg);
	}

	/**
	 * 签到结束教师端获取签到数据
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getSignInResult")
	public ResultBean getSignInResult(HttpServletRequest request, HttpServletResponse response) {
		String teacherId = request.getParameter("teacherId");
		String isSignEnd = request.getParameter("isSignEnd");
		// String courseId = request.getParameter("courseId");

		if (teacherId == null) {
			resultBean.set(StatusCodeHelp.parameterError, StatusCodeHelp.parameterErrorMessage, null);
			return resultBean;
		}
		return signInService.getSignInResult(teacherId, isSignEnd);
	}

	/**
	 * 开启签到功能
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/startSignIn")
	public ResultBean startSignIn(HttpServletRequest request, HttpServletResponse response) {

		String teacherId = request.getParameter("teacherId");
		String userId = request.getParameter("userId");

		ResultBean resultBean = signInService.startSignIn(teacherId);

		return resultBean;
	}

	/**
	 * 获取从安卓端发来的签到信息
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/SendSignInMessage")
	public ResultBean SendSignInMessage(HttpServletRequest request, HttpServletResponse response) {

		String studentSignInMsgStr = request.getParameter("studentSignInMsg");

		Gson gson = new Gson();
		StudentSignInMsg sigMsg = gson.fromJson(studentSignInMsgStr, StudentSignInMsg.class);

		if (sigMsg == null) {
			resultBean.set(StatusCodeHelp.parameterError, StatusCodeHelp.passwordErrorMessage, null);
			return resultBean;
		}

		return signInService.sendSignInMessage(sigMsg);
	}

	/**
	 * 处理签到数据
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/endSignIn")
	public ResultBean handleData(HttpServletRequest request, HttpServletResponse response) {
		String teacherId = request.getParameter("teacherId");
		if (teacherId == null) {
			resultBean.set(StatusCodeHelp.parameterError, StatusCodeHelp.passwordErrorMessage, null);
			return resultBean;
		}
		return signInService.handleData(teacherId);
	}

	/**
	 * 我的签到 模块
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/signInData")
	public ResultBean signInData(HttpServletRequest request, HttpServletResponse response) {
		String stuId = request.getParameter("stuId");
		String teacherId = request.getParameter("teacherId");
		String courseId = request.getParameter("courseId");

		if (courseId != null) {
			resultBean = signInService.getCourseSignInData(courseId);
		} else if (stuId != null) {
			resultBean = signInService.getStuSignInData(Long.valueOf(stuId));
		} else if (teacherId != null) {
			resultBean = signInService.getTeacherSignInData(teacherId);
		} else {
			resultBean.set(StatusCodeHelp.parameterError, StatusCodeHelp.passwordErrorMessage, null);
			return resultBean;
		}

		return resultBean;
	}

}
