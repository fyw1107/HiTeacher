package com.holi.module.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.holi.common.help.StatusCodeHelp;
import com.holi.common.resultBean.CourseBean;
import com.holi.common.resultBean.CourseScheduleBean;
import com.holi.common.resultBean.ResultBean;
import com.holi.module.entity.CourseSchedule;
import com.holi.module.service.CourseScheduleService;
import com.holi.module.service.CourseService;

/**
 * 课程显示控制器
 * 
 * @author IVAN
 *
 */
@Controller
@RequestMapping(value = "/course")
public class CourseController {

	@Autowired
	private CourseService courseService;

	@Autowired
	private CourseScheduleService courseScheduleService;

	@Autowired
	private ResultBean resultBean;

	/**
	 * 查询要滚动显示的上课信息
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/scrollCourse")
	@ResponseBody
	public ResultBean listCourses(HttpServletRequest request, HttpServletResponse response) {

		int stuId = Integer.parseInt(request.getParameter("stuId"));

		List<CourseBean> list = courseService.listCourseByStuAndWeek(stuId);

		if (list.size() == 0) {
			CourseBean courseBean = new CourseBean();
			courseBean.setCourseName("今天没有课喔，一起玩耍吧");
			list.add(0, courseBean);

			resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, list);
			return resultBean;
		}

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, list);
		return resultBean;
	}

	/**
	 * 查询我的课程表
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/showMySchedule")
	@ResponseBody
	public ResultBean showMySchedule(HttpServletRequest request, HttpServletResponse response) {

		Long stuId = Long.parseLong(request.getParameter("stuId"));

		List<CourseScheduleBean> list = courseScheduleService.getMySchedule(stuId);

		resultBean.set(StatusCodeHelp.success, StatusCodeHelp.successMessage, list);
		return resultBean;
	}

	/**
	 * 根据stuId获取本学生所上的课程
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/stuCourse")
	@ResponseBody
	public ResultBean stuCourse(HttpServletRequest request, HttpServletResponse response) {

		Long stuId = Long.valueOf(request.getParameter("stuId"));

		ResultBean resultBean = courseService.stuCourse(stuId);

		return resultBean;
	}

	/**
	 * 根据teacherId获取改老师所上的所有课程
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/teacherCourse")
	@ResponseBody
	public ResultBean teacherCourse(HttpServletRequest request, HttpServletResponse response) {

		String teacherId = request.getParameter("teacherId");

		ResultBean resultBean = courseService.teacherCourse(teacherId);

		return resultBean;
	}
}
