package com.holi.module.web;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.holi.module.service.FriendsService;

@Controller
@RequestMapping(value = "/friend")
public class FriendsController {

	@Autowired
	private FriendsService friendsService;

	/**
	 * 同意添加好友
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/addFriend")
	public Object addFriend(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String accid = request.getParameter("accid");
		String faccid = request.getParameter("faccid");

		return friendsService.addFriend(accid, faccid);
	}
	
	@ResponseBody
	@RequestMapping(value = "/removeFriend")
	public Object removeFriend(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String accid = request.getParameter("accid");
		String faccid = request.getParameter("faccid");

		return friendsService.removeFriend(accid, faccid);
	}
}
