package com.holi.module.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.holi.common.resultBean.ResultBean;
import com.holi.module.service.CollectionService;

/**
 * 我的收藏控制层
 * 
 * @author IVAN
 *
 */
@Controller
@RequestMapping(value = "/collection")
public class CollectionController {

	@Autowired
	private CollectionService collectionService;

	/**
	 * 添加收藏
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public ResultBean addCollection(HttpServletRequest request, HttpServletResponse response) {

		if (request.getParameter("newsId") == null) {
			return null;
		} else {
			int newsId = Integer.parseInt(request.getParameter("newsId"));
			long stuId = Long.parseLong(request.getParameter("stuId"));
			int teacherId = Integer.parseInt(request.getParameter("teacherId"));
			return collectionService.addCollection(newsId, stuId, teacherId);
		}
	}

	/**
	 * 删除收藏
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public ResultBean deleteCollection(HttpServletRequest request, HttpServletResponse response) {

		if (request.getParameter("collectionId") == null) {
			return null;
		} else {
			int collectionId = Integer.parseInt(request.getParameter("collectionId"));
			return collectionService.deleteCollection(collectionId);
		}
	}

	/**
	 * 查看收藏
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/show")
	@ResponseBody
	public ResultBean showCollection(HttpServletRequest request, HttpServletResponse response) {

		if (request.getParameter("stuId") == null || request.getParameter("teacherId") == null) {
			return null;
		}
		long stuId = Long.parseLong(request.getParameter("stuId"));
		int teacherId = Integer.parseInt(request.getParameter("teacherId"));

		return collectionService.listMyCollection(stuId, teacherId);
	}
}
