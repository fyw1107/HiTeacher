package com.holi.module.web;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.holi.common.resultBean.ResultBean;
import com.holi.module.service.XiaoXiNewsService;

/**
 * 校息控制层
 * 
 * @author lz
 *
 */
@Controller
@RequestMapping(value = "/xiaoxi")
public class XiaoXiNewsController {

	@Autowired
	private XiaoXiNewsService xiaoXiService;

	/**
	 * 加载新闻
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/pullDown")
	@ResponseBody
	public ResultBean pullDown(HttpServletRequest request, HttpServletResponse response) {
		
		String stuId = request.getParameter("stuId");
		String teacherId = request.getParameter("teacherId");
		String datetime = request.getParameter("datetime");
		
		if (datetime == null) {
			//未指定查询日期,即下拉刷新
			return xiaoXiService.pullDown(stuId, teacherId);
		} else {
			//指定查询日期，为上拉加载
			return xiaoXiService.pullUp(stuId, teacherId, datetime);
		}

	}
}
