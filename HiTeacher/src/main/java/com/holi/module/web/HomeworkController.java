package com.holi.module.web;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.holi.common.resultBean.AllHomeworkInfoBean;
import com.holi.common.resultBean.CourseInfoBean;
import com.holi.common.resultBean.HomeworkInfoBean;
import com.holi.common.resultBean.ResultBean;
import com.holi.common.util.StringUtil;
import com.holi.module.entity.Homework;
import com.holi.module.service.HomeworkService;

/**
 * 作业控制层
 * 
 * @author IVAN
 *
 */
@Controller
@RequestMapping(value = "/homework")
public class HomeworkController {

	@Autowired
	private HomeworkService homeworkService;

	/**
	 * 显示教师的所有课程的简略信息(课程编号+名称)——放在网页端登录代码中 网页端(1)
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/listTC")
	@ResponseBody
	public ResultBean listTeacherCourses(String teacherId) {

		ResultBean resultBean = new ResultBean();

		List<CourseInfoBean> courseInfoList = homeworkService.listTeacherCourseInfo(teacherId);

		resultBean.set(200, "查询成功", courseInfoList);

		return resultBean;
	}

	/**
	 * 根据教师id和课程id获取所有布置了作业的班级(班号)集合 网页端(2)
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/listClassnumber")
	@ResponseBody
	public ResultBean obtainClassnumbers(String teacherId, String courseId) {

		ResultBean resultBean = new ResultBean();

		List<String> list = homeworkService.getClassnumbers(teacherId, courseId);

		resultBean.set(200, "查询成功！", list);

		return resultBean;
	}

	/**
	 * 根据课程id和班号获取作业的编次(布置时间+作业编号(主键)) 网页端(3)
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/listHomeworkNumber")
	@ResponseBody
	public ResultBean obtainHomeworkNumber(String teacherId, String courseId, String classnumber) {

		ResultBean resultBean = new ResultBean();

		List<HomeworkInfoBean> list = homeworkService.getHomeworkNumber(teacherId, courseId, classnumber);

		resultBean.set(200, "查询成功", list);

		return resultBean;
	}

	/**
	 * 显示作业列表(根据选择的作业编次) 网页端(4)
	 * 
	 * @param request
	 * @param response
	 * @return map 包括未提交、已提交和未批阅的list
	 */
	@RequestMapping(value = "/listAll")
	@ResponseBody
	public ResultBean listHomeworkPage(int homeworkRecordId) {

		Map<String, Object> map = null;
		ResultBean resultBean = new ResultBean();

		map = homeworkService.listAllHomeworkInfo(homeworkRecordId);

		resultBean.set(200, "查询成功", map);
		return resultBean;
	}

	/**
	 * 批改作业(网页)_更新批改后的图片 网页端(5-1)
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/checkPicture")
	@ResponseBody
	public String checkHomeworkPicture(HttpServletRequest request, HttpServletResponse response) {

		// 获取要更新的作业图片和原作业图片的url
		String imgBase64 = request.getParameter("imgBase64");
		String picUrl = request.getParameter("url");

		System.out.println("/checkPicture:picUrl:" + picUrl);
		System.out.println("imgBase64:" + imgBase64);

		imgBase64 = imgBase64.replace("data:image/png;base64,", "");
		try {
			// 解码图片并覆盖到原图片
			homeworkService.convertBase64DataToImage(imgBase64, picUrl);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		return "批改图片上传成功！";
	}

	/**
	 * 批改作业(网页)_更新批改后的评分等级(打分~) 网页端(5-2)
	 * 
	 * @param homeworkId
	 *            单次作业在数据库中的主键
	 * @param homeworkGrade
	 * @param homeworkRecordId
	 *            单次作业的recordId,对应homework_record表中的统计记录
	 * @return
	 */
	@RequestMapping(value = "/checkScore", produces = "application/json;charset=utf-8")
	@ResponseBody
	public String checkHomeworkScore(int homeworkId,
			String score/* , int homeworkRecordId */) {

		homeworkService.updateHomeworkScore(homeworkId, score);
		// 批改完一个作业重定向到listAll页面显示更新后的数据
		// return "redirect:/homework/listAll.action?homeworkRecordId=" +
		// homeworkRecordId;
		return "批改成功！";
	}

	/*************************************************************************************************************************************************
	 *************************************************************************************************************************************************
	 ************************************************************ 网页端与安卓端分割线************************************************************************
	 *************************************************************************************************************************************************
	 *************************************************************************************************************************************************
	 */

	/**
	 * 点击作业按钮,返回所有可以布置作业的课程 (安卓教师端)
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/listCourses", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResultBean listTeacherHomeworkPage(HttpServletRequest request, HttpServletResponse response) {

		ResultBean resultBean = new ResultBean();

		String teacherId = request.getParameter("teacherId");
		List<CourseInfoBean> courseInfoList = homeworkService.listCourseInfo(teacherId);

		resultBean.set(200, "查询成功", courseInfoList);

		return resultBean;
	}

	/**
	 * 布置作业 (安卓教师端) 布置作业完成后重定向至列表显示页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/assign", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResultBean assignHomework(HttpServletRequest request, HttpServletResponse response) {

		ResultBean resultBean = new ResultBean();
		int count = 0;
		String courseId = request.getParameter("courseId"); // 课程编号
		String courseName = StringUtil.stringParamDecoder(request.getParameter("courseName")); // 课程名称
		String classnumber = StringUtil.stringParamDecoder(request.getParameter("classnumber")); // 班级
		String teacherId = request.getParameter("teacherId"); // 教师id
		String hwContent = StringUtil.stringParamDecoder(request.getParameter("hwContent")); // 作业内容

		count = homeworkService.assignHomework(courseId, courseName, classnumber, teacherId, hwContent);
		if (count > 0) {
			resultBean.set(200, "布置成功！", null);
		} else {
			resultBean.set(300, "布置失败！", null);
		}
		return resultBean;
	}

	/**
	 * 布置作业界面点击某课某班跳转至选择作业编次界面 (安卓教师端)
	 * 
	 * @param request
	 * @param response
	 * @return 该课该班布置过的所有作业的编次
	 */
	@RequestMapping(value = "/listAllHomeworkNumber", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResultBean listAllHomeworkNumber(HttpServletRequest request, HttpServletResponse response) {

		ResultBean resultBean = new ResultBean();

		String teacherId = request.getParameter("teacherId");
		String courseId = request.getParameter("courseId");
		String classnumber = request.getParameter("classnumber");

		// 返回该教师布置的所有作业(作业记录详情 + 提交/批改率)
		resultBean = this.obtainHomeworkNumber(teacherId, courseId, classnumber);

		return resultBean;
	}

	/**
	 * 显示作业详情列表(根据选择的作业编次) (安卓教师端)
	 * 
	 * @param request
	 * @param response
	 * @return map 包括未提交、已提交和未批阅的list
	 */
	@RequestMapping(value = "/listAllHomework")
	@ResponseBody
	public ResultBean listAllHomeworkPage(HttpServletRequest request, HttpServletResponse response) {

		ResultBean resultBean = new ResultBean();

		Integer homeworkRecordId = Integer.parseInt(request.getParameter("homeworkRecordId"));

		resultBean = this.listHomeworkPage(homeworkRecordId);

		return resultBean;
	}

	/**
	 * 查询返回某次作业未批阅列表 (安卓教师端)
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/listUnchecked")
	@ResponseBody
	public ResultBean listUncheckedHomework(HttpServletRequest request, HttpServletResponse response) {

		ResultBean resultBean = new ResultBean();

		// 由前端显示的HomewordRecord对象获取并传递
		Integer homeworkRecordId = Integer.parseInt(request.getParameter("homeworkRecordId"));

		List<Homework> uncheckedList = homeworkService.listUncheckedHomework(homeworkRecordId);

		if (uncheckedList.size() == 0)
			resultBean.set(300, "没有要批阅的作业哦~", null);
		else
			resultBean.set(200, "查询成功！", uncheckedList);

		return resultBean;
	}

	/**
	 * 返回未提交作业的学生列表(学号+姓名) (安卓教师端)
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/listUncommited")
	@ResponseBody
	public ResultBean listUncommitedStudentInfo(HttpServletRequest request, HttpServletResponse response) {

		ResultBean resultBean = new ResultBean();

		// 由前端显示的HomewordRecord对象获取并传递
		Integer homeworkRecordId = Integer.parseInt(request.getParameter("homeworkRecordId"));

		List<AllHomeworkInfoBean> uncommitededList = homeworkService.listUncommitedStudentInfo(homeworkRecordId);

		if (uncommitededList.size() == 0)
			resultBean.set(300, "所有学生都已提交了哦~", null);
		else
			resultBean.set(200, "查询成功！", uncommitededList);

		return resultBean;
	}

	/**
	 * 查询所有未提交的作业(主页显示) (安卓学生端) 具体分类显示交由前端处理
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/listAllStudentHomework", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResultBean listStudentHomeworkPage(HttpServletRequest request, HttpServletResponse response) {

		ResultBean resultBean = new ResultBean();

		// 通过学生id查找所有还未提交的作业
		long stuId = Long.parseLong(request.getParameter("stuId"));
		Map<String, Object> map = homeworkService.listAllStudentHomework(stuId);

		// TODO 具体作业的分类 (前端还是后台处理？？)
		// TODO 先查询所有的课程分类显示在前端(GROUP BY course_name),再根据点击的具体课程号查询相关的作业记录

		resultBean.set(200, "查询成功！", map);

		return resultBean;
	}

	/**
	 * 提交(更新)作业——method2_2017.5.17 (安卓学生端)
	 * 
	 * @param homeworkImages
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/commit", produces = "application/json;charset=utf-8")
	@ResponseBody
	public String commitHomework(@RequestParam("homeworkImages") MultipartFile[] homeworkImages,
			HttpServletRequest request, HttpServletResponse response) {

		Integer homeworkId = Integer.parseInt(request.getParameter("homeworkId"));
		String courseId = request.getParameter("courseId");
		String classnumber = request.getParameter("classnumber");
		Long stuId = Long.parseLong(request.getParameter("stuId"));

		// 构造图片存储路径(用系统当前时间确定每次提交的作业存放在不同的路径下)
		String picPath = request.getServletContext().getRealPath("/homework") + "\\" + courseId + "\\" + classnumber
				+ "\\" + stuId + "\\" + System.currentTimeMillis();

		String backInfo = "";
		// 判断是否有文件上传
		if (homeworkImages != null && homeworkImages.length > 0)
			backInfo = homeworkService.commitHomework(homeworkId, picPath, homeworkImages);
		else
			return "未选择上传文件！";

		return backInfo;
	}

	/**
	 * 提交(更新)作业——method1_2017.5.17 (Web学生端)
	 * 方法一：CommonsMultipartResolver——MultipartHttpServletRequest——MultipartFile[]
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/commit_1", produces = "application/json;charset=utf-8")
	@ResponseBody
	public String commitHomework_1(HttpServletRequest request, HttpServletResponse response) {

		Integer homeworkId = Integer.parseInt(request.getParameter("homeworkId"));
		String courseId = request.getParameter("courseId");
		String classnumber = request.getParameter("classnumber");
		Long stuId = Long.parseLong(request.getParameter("stuId"));

		// 构造图片存储路径(用系统当前时间确定每次提交的作业存放在不同的路径下)
		String picPath = request.getServletContext().getRealPath("/homework") + "\\" + courseId + "\\" + classnumber
				+ "\\" + stuId + "\\" + System.currentTimeMillis();

		String backInfo = homeworkService.commitHomework_1(homeworkId, picPath, request);

		return backInfo;
	}

	/**
	 * 提交(更新)作业——method3_2017.5.19 (Web学生端) 方法二：使用FileUpload工具类实现文件上传
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "commit_2", produces = "application/json;charset=utf-8")
	@ResponseBody
	public String commitHomework_2(HttpServletRequest request, HttpServletResponse response) {

		Integer homeworkId = Integer.parseInt(request.getParameter("homeworkId"));
		String courseId = request.getParameter("courseId");
		String classnumber = request.getParameter("classnumber");
		Long stuId = Long.parseLong(request.getParameter("stuId"));

		// 构造图片存储路径(用系统当前时间确定每次提交的作业存放在不同的路径下)
		String picPath = request.getServletContext().getRealPath("/homework") + "\\" + courseId + "\\" + classnumber
				+ "\\" + stuId + "\\" + System.currentTimeMillis();

		String backInfo = homeworkService.commitHomework_2(homeworkId, picPath, request);

		return backInfo;
	}

}
