package com.holi.module.web;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.holi.common.help.StatusCodeHelp;
import com.holi.common.resultBean.ResultBean;
import com.holi.module.entity.User;
import com.holi.module.service.UserService;

/**
 * user controller类
 * 
 * @author lz
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private ResultBean resultBean;

	/**
	 * 新用户注册
	 * 
	 * @return json
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/register")
	public String register(HttpServletRequest request, HttpServletResponse response) throws IOException {

		// 设置编码，中文统一使用utf-8
		request.setCharacterEncoding("utf-8");
		String accid = request.getParameter("accid");
		String name = request.getParameter("name");
		String token = request.getParameter("token");
		/*String version = request.getHeader("version");
		System.out.println(version);*/
		/*
		 * String sign = request.getParameter("sign"); String timestamp =
		 * request.getParameter("timestamp");
		 */

		/*
		 * //判断时间戳与系统时间相隔时间大于5min,则判断为恶意访问 if(DateUtil.DateApart(timestamp) >
		 * -300 && DateUtil.DateApart(timestamp) < 300) {
		 * 
		 * //判断签名是否符合定义的规则 if(CheckSign.isSuccess(phone, timestamp, sign)) {
		 */
		return userService.register(accid, name, token);
		/*
		 * } else { reslutMap.put("reslut",StatusCodeHelp.signError); return
		 * reslutMap; }
		 * 
		 * } else { reslutMap.put("reslut",StatusCodeHelp.timeOut); return
		 * reslutMap; }
		 */
	}

	/**
	 * 获取用户信息
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/userInfo")
	public ResultBean userInfo(HttpServletRequest request, HttpServletResponse response) throws IOException {

		// 设置编码，中文统一使用utf-8
		request.setCharacterEncoding("utf-8");
		String accid = request.getParameter("accid");

		return userService.userInfo(accid);
	}

	/**
	 * 用户注册登录 控制层
	 * 
	 * @return json
	 * 
	 */
	@ResponseBody
	@RequestMapping(value = "/login")
	public ResultBean login(HttpServletRequest request, HttpServletResponse response, User user,
			HttpSession httpSession) throws UnsupportedEncodingException {

		// 设置编码，中文统一使用utf-8
		request.setCharacterEncoding("utf-8");
		String phone = request.getParameter("phone");
		String password = request.getParameter("password");

		ResultBean resultBean = userService.login(phone, password);
		if (resultBean.getStatus() == 200) {
			httpSession.setAttribute("phone", phone);
		}

		return resultBean;

	}

	/**
	 * 密码找回 控制层
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/updatePassword")
	public Object updatePassword(HttpServletRequest request, HttpServletResponse response) throws IOException {

		// 设置编码，中文统一使用utf-8
		request.setCharacterEncoding("utf-8");
		String accid = request.getParameter("accid");
		String token = request.getParameter("token");
		/*
		 * String sign = request.getParameter("sign"); String timestamp =
		 * request.getParameter("timestamp");
		 */

		/*
		 * //判断时间戳与系统时间相隔时间大于5min,则判断为恶意访问 if(DateUtil.DateApart(timestamp) >
		 * -300 && DateUtil.DateApart(timestamp) < 300) {
		 * 
		 * //判断签名是否符合定义的规则 if(CheckSign.isSuccess(phone, timestamp, sign)) {
		 */
		return userService.updatePassword(accid, token);
		/*
		 * } else { reslutMap.put("reslut",StatusCodeHelp.signError); return
		 * reslutMap; }
		 * 
		 * } else { reslutMap.put("reslut",StatusCodeHelp.timeOut); return
		 * reslutMap; }
		 */
	}

	/**
	 * 用戶反饋
	 * 
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@ResponseBody
	@RequestMapping(value = "/feedback")
	public Object feedback(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {
		// TODO
		request.setCharacterEncoding("utf-8");
		String userId = request.getParameter("userId");
		String content = request.getParameter("feedbackInfo");
		String phone = request.getParameter("feedbackContact");
		String type = request.getParameter("feedbacktype");
		return userService.feedback(userId,content, phone, type);
	}

	/**
	 * 用户绑定信息
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@ResponseBody
	@RequestMapping(value = "/bind")
	public ResultBean bind(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String userType = request.getParameter("userType");
		String userId = request.getParameter("userId");
		String stuId = request.getParameter("stuId");
		String teacherId = request.getParameter("teacherId");
		String stuPassword = request.getParameter("stuPassword");
		
//		System.out.println(userType + "  " + userId + "  " + stuId+"  "+teacherId+" "+stuPassword);

		if (Integer.valueOf(userType) == 0) {
			resultBean = userService.bindStudent(userId, stuId, stuPassword);
		} else if (Integer.valueOf(userType) == 1) {
			resultBean = userService.bindTeacher(userId, teacherId);
		} else {
			resultBean.set(StatusCodeHelp.parameterError, StatusCodeHelp.parameterErrorMessage, null);
		}

		return resultBean;

	}

	/**
	 * 用户绑定极光ID
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@ResponseBody
	@RequestMapping(value = "/bindJpush")
	public ResultBean bindJpush(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String userId = request.getParameter("userId");
		String jpushId = request.getParameter("jpushId");

		return userService.bingJpushId(userId, jpushId);
	}

	/**
	 * 用户绑定极光ID
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@ResponseBody
	@RequestMapping(value = "/removeBindJpush")
	public ResultBean removeBindJpush(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String userId = request.getParameter("userId");

		return userService.removeBindJpush(userId);
	}

	@ResponseBody
	@RequestMapping(value = "/test")
	public ResultBean test(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException {

		String picPath = request.getSession().getServletContext().getRealPath("/homework");

		return userService.removeBindJpush("");
	}
}