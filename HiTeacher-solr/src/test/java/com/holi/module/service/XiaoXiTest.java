package com.holi.module.service;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 校息测试
 * 
 * @author lz
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring-service.xml", "classpath:spring-mybatis.xml", "classpath:spring-solr.xml" })
public class XiaoXiTest {

	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private XiaoXiService xiaoXiService;
	@Autowired
	private SolrServer solrServer;

	/**
	 * 数据插入solr索引库
	 * 
	 * @throws IOException
	 * @throws SolrServerException
	 */
	@org.junit.Test
	public void insertSolr() throws SolrServerException, IOException {
		
		// 创建一个文档对象
		SolrInputDocument document = new SolrInputDocument();
		document.addField("id", "123482943289");
		document.addField("xiaoxi_news_tittle", "测试商品2");

		// 把文档对象写入索引库
		solrServer.add(document);
		// 提交
		solrServer.commit();

	}

}
