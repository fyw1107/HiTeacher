package com.holi.module.service;

import java.util.List;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.holi.module.dao.XiaoXiDao;
import com.holi.module.entity.XiaoXi;

/**
 * 校息搜索业务逻辑
 * 
 * @author lz
 *
 */
@Service
public class XiaoXiService {

	@Autowired
	private XiaoXiDao xiaoXiDao;
	@Autowired
	private SolrServer solrService;

	/**
	 * 数据插入solr索引库
	 */
	public void indexToSolr() {

		List<XiaoXi> allNewsList = xiaoXiDao.findAllList();

		for (XiaoXi xiaoXi : allNewsList) {
			SolrInputDocument inputDocument = new SolrInputDocument();
			inputDocument.setField("id", xiaoXi.getId());
		}
		
	}
}
