package com.holi.module.dao;

import com.holi.common.annotation.Dao;
import com.holi.common.dao.BaseDao;
import com.holi.module.entity.XiaoXi;

/**
 * 校息Dao接口类
 * 
 * @author lz
 */
@Dao
public interface XiaoXiDao extends BaseDao<XiaoXi> {

}
