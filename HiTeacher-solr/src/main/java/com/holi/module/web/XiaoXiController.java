package com.holi.module.web;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.holi.common.entity.ResponseBean;
import com.holi.module.service.XiaoXiService;

/**
 * 校息控制层
 * 
 * @author lz
 *
 */
@Controller
@RequestMapping(value = "/xiaoxi")
public class XiaoXiController {

	@Autowired
	private XiaoXiService xiaoXiService;

	/**
	 * 增量导入solr
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/deltaImportSolr")
	public void deltaImportSolr(HttpServletRequest request, HttpServletResponse response) throws IOException {

		response.sendRedirect("http://localhost:8080/solr/dataimport?command=delta-import");
	}

	/**
	 * 消息新闻搜素
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/reacherNews")
	public ResponseBean reacherNews(HttpServletRequest request, HttpServletResponse response) {
		
		return null;

	}

}
