package com.holi.module.entity;

import org.apache.solr.client.solrj.beans.Field;

import com.holi.common.entity.BaseEntity;

/**
 * 校息实体类
 * 
 * @author lz
 *
 */
public class XiaoXi extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Field
	private String name;
	@Field
	private String content;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
