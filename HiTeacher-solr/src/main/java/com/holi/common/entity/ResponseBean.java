package com.holi.common.entity;

/**
 * 返回前端的实体类
 * 
 * @author lz
 *
 */
public class ResponseBean {
	/**
	 * 返回的状态码
	 */
	private int status;
	/**
	 * 错误提示信息
	 */
	private String message;
	/**
	 * 返回的数据信息
	 */
	private Object data;
	
	public void set(int status,String message,Object data) {
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
