package com.holi.common.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import org.apache.solr.client.solrj.beans.Field;

public abstract class BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 唯一标识
	 */
	@Field
	protected String id;
	/**
	 * 更新时间
	 */
	@Field
	protected Timestamp updateTime;
	/**
	 * 逻辑删除标志位 1表示未删除 0表示删除
	 */
	@Field
	protected int isDeleted;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

}
